<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinServiceUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('tblbinserviceupdates', function (Blueprint $table) {
			$table->increments('idBinServiceUpdates');
			$table->integer('idBinService');
			$table->float('price');
			$table->float('discPrice');
			$table->integer('stock');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
