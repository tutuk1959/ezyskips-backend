<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use App\tblorderservice;
use App\tblbinservicestok;
use Validator;
use Redirect;
use Illuminate\Support\Facades\DB;

class stockupdate extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'stockupdate:day';

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Stock checking for collection date and then update to default stock';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	public function __construct(){
		parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle(){
		$bnorder = $this->get_bin_order();
		$dateNow = date('Y-m-d', strtotime('now'));
		
		foreach ($bnorder as $k=>$v){
			if($v->collectionDate <= $dateNow){
				$auto_updated_row = $this->get_bin_order_date($v->idBinService, $v->paymentUniqueCode, $v->collectionDate);
				if(!is_null($auto_updated_row)){
					if ($auto_updated_row->stock < $auto_updated_row->default_stock){
						$tblbinservice = new tblbinservice();
						$tblbinservice::where([
							'idBinService' => $auto_updated_row->idBinService
						])
						->update([
							'stock' => $auto_updated_row->stock+1
						]);
						
						$bindisc = $this->getbinservicediscprice($auto_updated_row->idBinService);
						if(!is_null($bindisc)){
							$tblbinserviceupdates = new tblbinserviceupdates();
							$tblbinserviceupdates::where([
								'idBinService' => $auto_updated_row->idBinService,
								'date' => $v->collectionDate
							])
							->update([
								'stock' => $auto_updated_row->stock+1
							]);
						}
						
						$binstock = $this->getstockupdatedprice($auto_updated_row->idBinService);
						if(!is_null($binstock)){
							$tblbinservicestok = new tblbinservicestok();
							$tblbinservicestok::where([
								'idBinService' => $auto_updated_row->idBinService,
								'date' => $v->collectionDate
							])
							->update([
								'stock' => $auto_updated_row->stock+1
							]);
						}
						
					} else if ($auto_updated_row->stock == $auto_updated_row->default_stock){
						$tblbinserviceupdates = new tblbinserviceupdates();
						$deletedRowsBinUpdates = $tblbinserviceupdates::where([
							'idBinService' => $auto_updated_row->idBinService, 
							'date' => $v->collectionDate
						])->delete();
						
						$tblbinservicestok = new tblbinservicestok();
						$deletestockupdates =  $tblbinservicestok::where([
							'idBinService' => $auto_updated_row->idBinService,
							'date' => $v->collectionDate
						])->delete();
					}
				}
			} 
		}
		$this->info('Alright!');
	}
	
	private function getSupplierData(){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}
	private function get_bin_order(){
		$bin_order = DB::table('tblorderservice')
				->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorderservice.idBinService')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblorderservice.idSupplier')
				->leftJoin('tblbintype', 'tblbintype.idBinType', '=', 'tblbinservice.idBinType')
				->select('tblorderservice.deliveryDate', 'tblorderservice.collectionDate', 'tblorderservice.idOrderService', 'tblorderservice.idBinService'
					, 'tblorderservice.paymentUniqueCode'
				)
				->get();
		return $bin_order;
	}
	
	private function get_bin_order_date($idBinService, $paymentUniqueCode, $date){
		$bin_order = DB::table('tblorderservice')
				->leftJoin('tblbinservice', 'tblbinservice.idBinService', '=', 'tblorderservice.idBinService')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblorderservice.idSupplier')
				->leftJoin('tblbintype', 'tblbintype.idBinType', '=', 'tblbinservice.idBinType')
				->select('tblorderservice.deliveryDate', 'tblorderservice.collectionDate', 'tblorderservice.idOrderService', 'tblorderservice.idBinService'
					, 'tblorderservice.paymentUniqueCode', 'tblbinservice.stock', 'tblbinservice.default_stock'
				)
				->where([
					'tblorderservice.idBinService' => $idBinService, 
					'tblorderservice.paymentUniqueCode' => $paymentUniqueCode, 
					'tblorderservice.collectionDate' => $date
				])
				->first();
		return $bin_order;
	}
	
	private function getbinservicediscprice($idBinService){
		$binserviceprice = DB::table('tblbinserviceupdates')
						->select('tblbinserviceupdates.price','tblbinserviceupdates.stock','tblbinserviceupdates.date')
						->where([
							'tblbinserviceupdates.idBinService' => $idBinService,
							])
						->get();
		return $binserviceprice;
	}
	
	private function getstockupdatedprice($idBinService){
		$binservicestockupdates = DB::table('tblbinservicestok')
				->select('tblbinservicestok.stock','tblbinservicestok.date')
				->where([
					'tblbinservicestok.idBinService' => $idBinService
				])
				->get();
		return $binservicestockupdates;
	}
	
	private function autoupdatestock($idBinType){
		$idUser = 	session('idUser');
		$supplier = $this->getSupplierData($idUser);
		$bnorder = $this->get_bin_order($idBinType, $supplier->idSupplier);
		
		$dateNow = date('Y-m-d', strtotime('now'));
		
		foreach ($bnorder as $k=>$v){
			if($v->collectionDate <= $dateNow){
				$auto_updated_row = $this->get_bin_order_date($v->idBinService, $v->paymentUniqueCode, $v->collectionDate);
				if(!is_null($auto_updated_row)){
					if ($auto_updated_row->stock < $auto_updated_row->default_stock){
						$tblbinservice = new tblbinservice();
						$tblbinservice::where([
							'idBinService' => $auto_updated_row->idBinService
						])
						->update([
							'stock' => $auto_updated_row->stock+1
						]);
					}
				}
			} 
		}
		return true;
	}
}
