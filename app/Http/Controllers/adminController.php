<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblsupplier;
use App\tbluse;
use App\tblorderstatus;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class adminController extends Controller
{
	/****************************** USER MANAGEMENT **************************************/
	public function usermanagement_view(Request $request){
		$userdata = $this->get_pending_user_data();
		return view('user_management', [ 'userdata' => $userdata]);
	}

	public function activeuser_view(Request $request){
		$userdata = $this->get_active_user_data();
		return view('active_user', ['userdata' => $userdata]);
	}

	public function banuser_view(Request $request){
		$userdata = $this->get_ban_user_data();
		return view('ban_user', ['userdata' => $userdata]);
	}

	public function showResult(Request $request){
		$result['status'] = $request['status'];
		$result['message'] = $request['message'];
		$userdata = $this->get_pending_user_data();
		return view('user_management', [ 'userdata' => $userdata, 'result' => $result]);
	}

	public function activeUser_result(Request $request){
		$result['status'] = $request['status'];
		$result['message'] = $request['message'];
		$userdata = $this->get_active_user_data();
		return view('active_user', [ 'userdata' => $userdata, 'result' => $result]);
	}

	public function banUser_result(Request $request){
		$result['status'] = $request['status'];
		$result['message'] = $request['message'];
		$userdata = $this->get_ban_user_data();
		return view('ban_user', [ 'userdata' => $userdata, 'result' => $result]);
	}

	private function get_pending_user_data(){
		//Fetch all pending users data
		$userList = DB::table('tbluser')
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
					->select(
						'tbluser.idUser', 'tbluser.idSupplier', 'tbluser.username', 'tbluser.isActive', 'tbluser.adminApproved', 'tbluser.userStatus',
						'tbluser.registerDate','tblsupplier.name', 'tblsupplier.contactName', 'tblsupplier.email', 'tblsupplier.phonenumber', 'tblsupplier.comments'
					)
					->where(function($query){
        				$query->where( 'tbluser.isActive', '=', 1 )
            			->orWhere( 'tbluser.isActive', '=', 0);
    				})
					->where([
						'tbluser.role' => 2, 
						'tbluser.adminApproved' => 0,
						'tbluser.userStatus' => 2 
					])
					->groupBy(
						'tbluser.idUser', 'tbluser.idSupplier', 'tbluser.username', 'tbluser.isActive', 'tbluser.adminApproved', 'tbluser.userStatus',
						'tbluser.registerDate','tblsupplier.name', 'tblsupplier.contactName', 'tblsupplier.email', 'tblsupplier.phonenumber','tblsupplier.comments'
					)
					->get();
		return $userList;
	}

	private function get_active_user_data(){
		//Fetch all pending users data
		$userList = DB::table('tbluser')
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
					->select(
						'tbluser.idUser', 'tbluser.idSupplier', 'tbluser.username', 'tbluser.isActive', 'tbluser.adminApproved', 'tbluser.userStatus',
						'tbluser.registerDate','tblsupplier.name', 'tblsupplier.contactName', 'tblsupplier.email', 'tblsupplier.phonenumber','tblsupplier.comments'
					)
					->where([
						'tbluser.isActive' => 1,
						'tbluser.role' => 2, 
						'tbluser.adminApproved' => 1,
						'tbluser.userStatus' => 1, 
					])
					->groupBy(
						'tbluser.idUser', 'tbluser.idSupplier', 'tbluser.username', 'tbluser.isActive', 'tbluser.adminApproved', 'tbluser.userStatus',
						'tbluser.registerDate','tblsupplier.name', 'tblsupplier.contactName', 'tblsupplier.email', 'tblsupplier.phonenumber','tblsupplier.comments'
					)
					->get();
		return $userList;
	}

	private function get_ban_user_data(){
		//Fetch all pending users data
		$userList = DB::table('tbluser')
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
					->select(
						'tbluser.idUser', 'tbluser.idSupplier', 'tbluser.username', 'tbluser.isActive', 'tbluser.adminApproved', 'tbluser.userStatus',
						'tbluser.registerDate','tblsupplier.name', 'tblsupplier.contactName', 'tblsupplier.email', 'tblsupplier.phonenumber','tblsupplier.comments'
					)
					->where([
						'tbluser.isActive' => 0,
						'tbluser.role' => 2, 
						'tbluser.userStatus' => 3, 
					])
					->groupBy(
						'tbluser.idUser', 'tbluser.idSupplier', 'tbluser.username', 'tbluser.isActive', 'tbluser.adminApproved', 'tbluser.userStatus',
						'tbluser.registerDate','tblsupplier.name', 'tblsupplier.contactName', 'tblsupplier.email', 'tblsupplier.phonenumber','tblsupplier.comments'
					)
					->get();
		return $userList;
	}

	private function get_user_details_data($idUser){		
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}

	private function get_user_area_data($idUser){
		$mainServiceArea = DB::table('tbluser')
			->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblservicearea', 'tblsupplier.mainServiceArea', '=' , 'tblservicearea.idArea')
			->select('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->first();
		return $mainServiceArea;
	}

	public function aprroveuser(Request $request){
		$idUser = $request['idUser'];

		//First check whether it has been approved or not
		$checkApproval = DB::table('tbluser')
						->select('adminApproved', 'idUser')
						->where([
							'idUser' => $idUser
						])
						->first();
		if (!is_null($checkApproval)){
			//If approved, then update the DB and send the emails 
			if($checkApproval->adminApproved == '0'){
				$approval = $this->approval_method($idUser);
				if($approval['status'] == 'success'){
					$send = $this->mail_confirmation($idUser);
					if($send = 'success'){
						
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'User approved and email has been sent');
						return redirect()->route('user_management');
					} else {
						
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Can not send email. Something is not right.');
						return redirect()->route('user_management');
					}
				}else{

					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Can not approve user. Something is not right');
					return redirect()->route('user_management');
				}
			}
		} else {

			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'User has been confirmed or that user could be not exist at all');
			return redirect()->route('user_management');
		}
	}
	
	public function jimapprove(Request $request){
		$idUser = $request['idUser'];
		$send = $this->mail_confirmation($idUser);
		if($send = 'success'){
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'User approved and email has been sent');
			return redirect()->route('user_management');
		} else {
			
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'Can not send email. Something is not right.');
			return redirect()->route('user_management');
		}
	}
	
	private function approval_method($idUser){
		$tbluse = new tbluse();
		$selectedrow = $tbluse::where('idUser','=', $idUser)->update(['adminApproved' => '1', 'userStatus' => '1']);
		if($selectedrow){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'danger';
		}

		return $result;
	}

	private function mail_confirmation($idUser){
		$supplierData = $this->get_user_details_data($idUser);
		$email = $supplierData->email;
		$contactName = $supplierData->contactName;
		$subject = "Congratulations! Welcome to Ezyskips Online";
		Mail::send('mails.approve_user', ['supplierData' => $supplierData],
		function($mail) use ($email, $contactName, $subject){
			$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
			$mail->to($email, $contactName);
			$mail->subject($subject);
		});

		$verified = 'success';
		return $verified;
	}

	public function kickuser(Request $request){
		$idUser = $request['idUser'];

		//First check whether it has been approved or not
		$checkExist = DB::table('tbluser')
						->select('isActive','userStatus', 'idUser')
						->where([
							'idUser' => $idUser
						])
						->first();
		if (!is_null($checkExist)){
			//If approved, then update the DB and send the emails 
			if($checkExist->isActive == '1' && $checkExist->userStatus == '1'){
				$approval = $this->banned_method($idUser);
				if($approval['status'] == 'success'){
					$send = $this->banning_mail_confirmation($idUser);
					if($send = 'success'){
						
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'User banned and email has been sent');
						return redirect()->route('active_user');
					} else {
						
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Can not send email. Something is not right.');
						return redirect()->route('active_user');
					}
				}else{
					
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Can not approve user. Something is not right');
					return redirect()->route('active_user');
				}
			}
		} else {
			
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'That user could be not exist at all');
			return redirect()->route('active_user');
		}
	}

	private function banned_method($idUser){
		$tbluse = new tbluse();
		$selectedrow = $tbluse::where('idUser','=', $idUser)->update(['isActive' => '0', 'userStatus' => '3']);
		if($selectedrow){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'danger';
		}

		return $result;
	}


	private function banning_mail_confirmation($idUser){
		$supplierData = $this->get_user_details_data($idUser);
		$email = $supplierData->email;
		$contactName = $supplierData->contactName;
		$subject = "Sorry! Your account is banned";
		Mail::send('mails.ban_user', ['supplierData' => $supplierData],
		function($mail) use ($email, $contactName, $subject){
			$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
			$mail->to($email, $contactName);
			$mail->subject($subject);
		});

		$verified = 'success';
		return $verified;
	}

	public function reactivate_user(Request $request){
		$idUser = $request['idUser'];

		//First check whether it has been approved or not
		$checkExist = DB::table('tbluser')
						->select('isActive','userStatus', 'idUser')
						->where([
							'idUser' => $idUser
						])
						->first();
		if (!is_null($checkExist)){
			//If approved, then update the DB and send the emails 
			if($checkExist->isActive == '0' && $checkExist->userStatus == '3'){
				$approval = $this->reactivate_method($idUser);
				if($approval['status'] == 'success'){
					$send = $this->reactivate_mail_confirmation($idUser);
				
					if($send == 'success'){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'User re-activated and email has been sent');
						return redirect()->route('banned_user');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Can not send email. Something is not right.');
						
						return redirect()->route('banned_user');
					}
				}else{
					
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Can not approve user. Something is not right');
					return redirect()->route('banned_user');
				}
			}
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'That user could be not exist at all');
			return redirect()->route('banned_user');
		}
	}

	private function reactivate_method($idUser){
		$tbluse = new tbluse();
		$selectedrow = $tbluse::where('idUser','=', $idUser)->update(['isActive' => '1', 'userStatus' => '1']);
		if($selectedrow){
			$result['status'] = 'success';
		} else {
			$result['status'] = 'danger';
		}

		return $result;
	}

	private function reactivate_mail_confirmation($idUser){
		$supplierData = $this->get_user_details_data($idUser);
		$email = $supplierData->email;
		$contactName = $supplierData->contactName;
		$subject = "Welcome back ! Your account just has been re-activated";
		Mail::send('mails.reactivate_user', ['supplierData' => $supplierData],
		function($mail) use ($email, $contactName, $subject){
			$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
			$mail->to($email, $contactName);
			$mail->subject($subject);
		});
		$verified = 'success';
		return $verified;
	}

	/****************************** ORDER MANAGEMENT **************************************/

	public function manage_order_view(){
		$supplierdata = $this->get_supplier_data();
		$suppliesdata = null;
		$selected_supplierdata = null;
		return view('order_management', [ 'supplierdata' => $supplierdata, 'suppliesdata' => $suppliesdata, 'selected_supplierdata' => $selected_supplierdata]);
	}
	
	public function all_order_view(){
		$suppliesdata = null;
		$selected_supplierdata = null;
		return view('order_all', ['suppliesdata' => $suppliesdata, 'selected_supplierdata' => $selected_supplierdata]);
	}
	
	public function order_detail(Request $request){
		$orderref = $request['orderref'];
		$idSupplier = $request['idSupplier'];
		$idCustomer = $request['idCustomer'];
		$idBinType = $request['idBinType'];
		$idBinService = $request['idBinService'];


		$invoiceDetails = $this->get_order_detail($orderref, $idSupplier);
		$binhire = $this->get_bin_hire($idBinService);
		$supplierdetails = $this->get_supplier_data_by_id($idSupplier);
		
		$customerdetails = $this->get_customer_detail($idCustomer);
		$binhireoptions = $this->get_bin_service_options($idSupplier, $idBinType);
		
		$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
		$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
		$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));
		
		$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
		$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
		$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));
		
		$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
		$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
		$datemargin = $startdate->diffInDays($finishdate, false);

		if ($datemargin > $binhireoptions->extraHireageDays){
			$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
		} else {
			$daysmargin = 0;
		}
			
			
		$booking_price = $this->get_bookingprice();
		return view('invoice_content',  ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails, 
				'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'bookingprice' => $booking_price, 'daysmargin' => $daysmargin]);

	}
	private function get_supplier_data(){
		$supplierdata = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where(['tbluser.role' => 2])
			->orWhere(['tbluser.role' => 1])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->get();
		return $supplierdata;
	}

	private function get_supplier_data_by_id($idSupplier){
		$supplierdata = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tblsupplier.idSupplier' => $idSupplier 
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierdata;
	}

	private function get_customer_detail($idCustomer){
		$customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $idCustomer])
        		->first();
        return $customerdetails;
	}

	private function get_bin_service_options($idSupplier, $idBinType){
		$binhireoptions = DB::table('tblbinserviceoptions')
                ->leftJoin('tblbinservice', function ($query){
							$query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
									->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
							})
				->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
				->where([
					'tblbinserviceoptions.idBinType' => $idBinType,
					'tblbinserviceoptions.idSupplier' =>$idSupplier
					])
				->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
				->first();
		return $binhireoptions;
	}

	private function get_bin_hire($idBinHire){
		$binhire = DB::table('tblbinservice')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
				'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $idBinHire])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description','tblbintype.description2', 'tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

         return $binhire;
	}
	private function get_order_data($idSupplier, $start, $end){
		$orders = DB::table('tblorderservice')
			->leftJoin('tblcustomer','tblorderservice.idConsumer', '=', 'tblcustomer.idCustomer')
			->leftJoin('tblsupplier', 'tblorderservice.idSupplier','=', 'tblsupplier.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbinservice','tblbinservice.idBinService','=','tblorderservice.idBinService')
			->leftJoin('tblbintype','tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize','tblbinservice.idBinSize','=','tblsize.idSize')
			->leftJoin('tblorderstatus', 'tblorderstatus.idOrder', '=', 'tblorderservice.idOrderService')
			->select('tblcustomer.name AS customerName','tblcustomer.company AS customerCompanyName', 'tblcustomer.email AS customerEmail','tblcustomer.phone AS customerPhone',
				'tblbintype.name AS bintypename','tblbintype.CodeType as bintypecode','tblbintype.description AS bintypedescription',
				'tblsize.size AS binsize', 'tblorderservice.idOrderService','tblorderservice.paymentUniqueCode',
				'tblorderservice.orderDate', 'tblorderservice.deliveryDate', 'tblorderservice.collectionDate', 'tblorderservice.totalServiceCharge', 'tblorderservice.deliveryAddress', 
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService', 'tblorderservice.subtotal'
				)
			->where([
				'tblsupplier.idSupplier' => $idSupplier
			])
			->whereBetween('collectionDate',[$start,$end])
			->orderBy('tblorderservice.orderDate', 'DESC')
			->groupBy('customerName','customerCompanyName','customerEmail','customerPhone','bintypename','bintypecode','bintypedescription',
				'binsize','tblorderservice.idOrderService','tblorderservice.paymentUniqueCode','tblorderservice.orderDate', 'tblorderservice.deliveryDate', 
				'tblorderservice.collectionDate', 'tblorderservice.totalServiceCharge', 'tblorderservice.deliveryAddress',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService', 'tblorderservice.subtotal'
				)
			->get();
		return $orders;
	}
	
	private function get_all_order_data($start, $end){
		$orders = DB::table('tblorderservice')
			->leftJoin('tblcustomer','tblorderservice.idConsumer', '=', 'tblcustomer.idCustomer')
			->leftJoin('tblsupplier', 'tblorderservice.idSupplier','=', 'tblsupplier.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbinservice','tblbinservice.idBinService','=','tblorderservice.idBinService')
			->leftJoin('tblbintype','tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize','tblbinservice.idBinSize','=','tblsize.idSize')
			->leftJoin('tblorderstatus', 'tblorderstatus.idOrder', '=', 'tblorderservice.idOrderService')
			->select('tblcustomer.name AS customerName','tblcustomer.company AS customerCompanyName', 'tblcustomer.email AS customerEmail','tblcustomer.phone AS customerPhone',
				'tblbintype.name AS bintypename','tblbintype.CodeType as bintypecode','tblbintype.description AS bintypedescription',
				'tblsize.size AS binsize', 'tblorderservice.idOrderService','tblorderservice.paymentUniqueCode',
				'tblorderservice.orderDate', 'tblorderservice.deliveryDate', 'tblorderservice.collectionDate', 'tblorderservice.totalServiceCharge', 'tblorderservice.deliveryAddress', 
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService', 'tblorderservice.subtotal'
				)
			->whereBetween('collectionDate',[$start,$end])
			->orderBy('tblorderservice.orderDate', 'DESC')
			->groupBy('customerName','customerCompanyName','customerEmail','customerPhone','bintypename','bintypecode','bintypedescription',
				'binsize','tblorderservice.idOrderService','tblorderservice.paymentUniqueCode','tblorderservice.orderDate', 'tblorderservice.deliveryDate', 
				'tblorderservice.collectionDate', 'tblorderservice.totalServiceCharge', 'tblorderservice.deliveryAddress',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService', 'tblorderservice.subtotal'
				)
			->get();
		return $orders;
	}
	
	private function get_order_detail($orderref, $idSupplier){
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode', 'totalServiceCharge', 'gst', 'bookingfee', 'subtotal',
						'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate','card_category','card_type','card_number','card_holder')
        				->where(['paymentUniqueCode' => $orderref, 'idSupplier' => $idSupplier])
        				->first();
		 return $invoiceDetails;
	}

	public function fetch_supplier_order(Request $request){
		$validate = Validator::make($request->all(), [
            'selectsupplier' => 'required|integer',
        ], [
            'selectsupplier.integer' => 'Choose supplier first!'
        ]);

         if($validate->fails()){
         	$supplierdata = $this->get_supplier_data();
            return Redirect::route('supplier_supplies', ['supplierdata' =>$supplierdata])->withErrors($validate)->withInput();
        }

        $idSupplier = $request['selectsupplier'];
        $start = $request['start_order_date'];
        $end = $request['end_order_date'];
        $supplierdata = $this->get_supplier_data();
        $selected_supplierdata = $this->get_supplier_data_by_id($idSupplier);
        $suppliesdata = $this->get_order_data($idSupplier, $start, $end);
		$bookingprice = $this->get_bookingprice();
       	return view('order_management', [ 'supplierdata' => $supplierdata, 'selected_supplierdata' => $selected_supplierdata , 'suppliesdata' => $suppliesdata, 'bookingprice' => $bookingprice]);
	}
	
	public function fetch_all_supplier(Request $request){
		$validate = Validator::make($request->all(), [
            'start_order_date' => 'required|date',
			'end_order_date' => 'required|date'
        ]);

         if($validate->fails()){
         	$supplierdata = $this->get_supplier_data();
            return Redirect::route('supplier_supplies', ['supplierdata' =>$supplierdata])->withErrors($validate)->withInput();
        }

        $start = $request['start_order_date'];
        $end = $request['end_order_date'];
        $suppliesdata = $this->get_all_order_data( $start, $end);
		$bookingprice = $this->get_bookingprice();
       	return view('order_all', ['suppliesdata' => $suppliesdata, 'bookingprice' => $bookingprice]);
	}
	

	public function fetch_supplier_order_status(Request $request){
		$idSupplier = $request['idSupplier'];
		$start = date('Y-m-d', strtotime('-1 month'));
        $end = date('Y-m-d', strtotime('now'));
        $supplierdata = $this->get_supplier_data();
        $selected_supplierdata = $this->get_supplier_data_by_id($idSupplier);
        $suppliesdata = $this->get_order_data($idSupplier, $start, $end);
       	return view('order_management', [ 'supplierdata' => $supplierdata, 'selected_supplierdata' => $selected_supplierdata , 'suppliesdata' => $suppliesdata]);
	}

	public function update_order_status(Request $request){
		$idOrder = $request['idOrder'];
		$idSupplier = $request['idSupplier'];
		$status = $request['selectstatus'];

		$validate = Validator::make($request->all(), [
            'selectstatus' => 'required|integer',
        ], [
            'selectstatus.integer' => 'Select a status first !',
        ]);
        
		 if($validate->fails()){
         	$supplierdata = $this->get_supplier_data();
            return Redirect::route('supplier_supplies', ['supplierdata' =>$supplierdata])->withErrors($validate)->withInput();
        }


		$verify = DB::table('tblorderstatus')
				->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tblorderstatus.idOrder')
				->select('tblorderstatus.idOrderStatus', 'tblorderstatus.idOrder', 'tblorderstatus.operator', 'tblorderservice.idSupplier', 'tblorderservice.paymentUniqueCode')
				->where([
					'tblorderstatus.idOrder' => $idOrder,
					'tblorderservice.idSupplier' => $idSupplier
				])
				->first();

		if(!is_null($verify)){
			$update = $this->update_order_status_method($idOrder, $status);
			
			if($update['status'] == 'success'){
				$idSupplier = $verify->idSupplier; 
				$orderref = $verify->paymentUniqueCode;
				$send = $this->order_status_mail_confirmation($idSupplier, $idOrder, $orderref, $status);

				if($send == 'success'){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating order data. An email has been sent to the customer');
					return redirect()->route('supplier_supplies_result',  ['idSupplier' => $idSupplier]);
				} else {
					$idSupplier = $verify->idSupplier; 
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Something is wrong on sending the confirmation email');
					return redirect()->route('supplier_supplies_result',  ['idSupplier' => $idSupplier]);
				}
				
			} else {
				$idSupplier = $verify->idSupplier; 
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Something is wrong on updating order data');
				return redirect()->route('supplier_supplies_result',  ['idSupplier' => $idSupplier]);
			}
		} else {
				$idSupplier = $request['idSupplier'];
        		$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Something is wrong on updating order data');
				return redirect()->route('supplier_supplies_result',  ['idSupplier' => $idSupplier]);
		}
	}

	private function update_order_status_method($idOrder, $status){
		$orderstatus = new tblorderstatus();

		$updatedrow = $orderstatus::where('idOrder','=', $idOrder)->update([
			'status' => $status, 
			'updated_at' => date('Y-m-d H:i:s',strtotime('now')),
			'operator' => session('idUser')
			]);
		if($updatedrow){
			$result['status'] = 'success';
		} else {
			$updatedrow['status'] = 'danger';
		}

		return $result;

	}

	private function order_status_mail_confirmation($idSupplier, $idOrder, $orderref,$status){
		$invoiceDetails = $this->get_order_detail($orderref, $idSupplier);
		
		if(!is_null($invoiceDetails)){
			$binhire = $this->get_bin_hire($invoiceDetails->idBinService);
			$customerdetails = $this->get_customer_detail($invoiceDetails->idConsumer);

		} else {
			$verified = 'danger';
			return $verified;
		}
		
		$supplierdetails = $this->get_supplier_data_by_id($idSupplier);
		
		
		if (!is_null($binhire)){
			$binhireoptions = $this->get_bin_service_options($idSupplier, $binhire->idBinType);
		} else {
			$verified = 'danger';
			return $verified;
		}

		$orderstatus = $this->order_status_description($status);

		$email = $customerdetails->email;
		$contactName = $customerdetails->name;

		$subject = "Your order status updates. Your order is now ".$orderstatus;
		Mail::send('mails.orderstatus_confirmation', ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails, 'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'orderstatus' => $orderstatus],
		function($mail) use ($email, $contactName, $subject){
			$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
			$mail->to($email, $contactName);
			$mail->subject($subject);
		});
		$verified = 'success';
		return $verified;
	}

	private function order_status_description($status){
		if($status == 1){
			return 'Paid';
		} elseif ($status == 2){
			return 'Accepted';
		} elseif ($status == 4){
			return 'Cancelled before more than 2 days';
		} elseif ($status == 5){
			return 'Cancel, Cancelled before less than 1 day';
		}
	}
	
	private function get_bookingprice(){
		$bookingprice = DB::table('tblbookingprice')
					->select('price')
					->first();
		return $bookingprice;
	}
}
