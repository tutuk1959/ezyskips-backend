<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblbookingservice;
use Session;
use Redirect;
use Validator;
use Illuminate\Support\Facades\DB;

class bookingprice extends Controller
{
	public function index(){
		$data = $this->showpricelist();
		return view('bookingfee',['bookingprice' => $data]);
	}
	
	public function edit_form(Request $request){
		$idBookingPrice = $request['idBookingPrice'];
		$data = $this->showpricelist_id($idBookingPrice);
		return view('bookingfee_form',['bookingprice' => $data, 'action' => 'edit']);
	}
	
	public function bookingform_view(Request $request){
		$data = $this->showpricelist();
		if(!$data->isEmpty()){
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'Only 1 booking price is allowed on the system !');
			return redirect()->route('bookingfee',['action' => '']);
		}
		return view('bookingfee_form', ['action' => '']);
	}
	
	private function showpricelist(){
		$data = DB::table('tblbookingprice')
			->leftJoin('tbluser', 'tbluser.idUser' , '=', 'tblbookingprice.updated_by')
			->select('tblbookingprice.idBookingPrice','tblbookingprice.price', 'tblbookingprice.updated_by', 'tbluser.username', 'tblbookingprice.updated_at')
			->get();
		return $data;
	}
	
	private function showpricelist_id($id){
		$data = DB::table('tblbookingprice')
			->leftJoin('tbluser', 'tbluser.idUser' , '=', 'tblbookingprice.updated_by')
			->select('tblbookingprice.idBookingPrice','tblbookingprice.price', 'tblbookingprice.updated_by', 'tbluser.username', 'tbluser.idUser','tblbookingprice.updated_at')
			->where(['idBookingPrice' => $id])
			->first();
		return $data;
	}
	
	public function add_bookingfee(Request $request){
		$price = $request['price'];
		$validate = Validator::make($request->all(), [
			'price' => 'required',
		]);

		if($validate->fails()){
			$supplierdata = $this->showpricelist_id($idUser);
			return Redirect::route('booking_form', ['action' => ''])->withErrors($validate)->withInput();
		}
		
		$bookingfee = new tblbookingservice();
		$bookingfee->price = $price;
		$bookingfee->updated_by = session('idUser');
		$bookingfee->created_at = date('Y-m-d h:i:s',strtotime('now'));
		$bookingfee->updated_at = date('Y-m-d h:i:s',strtotime('now'));
		if($bookingfee->save()){
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'Success adding price data.');
			return redirect()->route('bookingfee');
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'Error on adding price data.');
			return redirect()->route('bookingfee');
		}
	}
	public function edit_bookingfee(Request $request){
		$idBookingPrice = $request['idBookingFee'];
		$idUser= $request['idUser'];
		$price = $request['price'];
		
		$validate = Validator::make($request->all(), [
			'price' => 'required',
        ]);

        if($validate->fails()){
         	$supplierdata = $this->showpricelist_id($idUser);
            return Redirect::route('edit_form', ['bookingprice' => $data, 'action' => 'edit'])->withErrors($validate)->withInput();
        }
		
		$bookingfee = new tblbookingservice();
		
		$updatedrow = $bookingfee::where('idBookingPrice','=', $idBookingPrice)->update([
			'price' => $price, 
			'updated_at' => date('Y-m-d H:i:s',strtotime('now')),
			'updated_by' => session('idUser')
			]);
	
		if($updatedrow){
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'Success updating price data.');
			return redirect()->route('bookingfee');
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'Error on updating price data.');
			return redirect()->route('bookingfee');
		}
		
	}
	
}
