<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\tblsupplier;
use App\tbluse;
use App\user_confirmation;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Validator;
use Redirect;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;

class loginController extends Controller
{
	public function index($result = null){
		if(!session('role')){
			return view('login', ['result' => $result]);
		} else{
			return view('dashboard');
		}
		
	}
	
	public function debugmode($result = null){
		if(!session('role')){
			return view('debug', ['result' => $result]);
		} else{
			return view('dashboard');
		}
		
	}
	
	public function validateRegister($organizationName, $contactName, $phoneNumber, $mobilePhone, $emailAddress, $username, $password, $retypepassword){
		$error = array();
		if ($organizationName == ''){
			$error[] = 'Organization Name must not empty';
		} 
		if ($contactName == ''){
			$error[] = 'Contact Name must not empty';
		} 
		if ($phoneNumber == ''){
			$error[] = 'Phone Number must not empty';
		}
		if ($mobilePhone == ''){
			$error[] = 'Mobile Phone must not empty';
		}
		if ($emailAddress == ''){
			$error[] = 'Email Address must not empty';
		}
		if ($username == ''){
			$error[] = 'Username must not empty';
		}
		if ($password == ''){
			$error[] = 'Password must not empty';
		}
		if ($retypepassword != $password){
			$error[] = 'Please retpye and match your password';
		}
		return $error;
	}
	
	public function register(Request $request){
		$organizationName = $request['organizationName'];
		$abn = $request['abn'];
		$contactName = $request['contactName'];
		$phoneNumber = $request['daytimePhoneNumber'];
		$mobilePhone = $request['mobilePhoneNumber'];
		$emailAddress = $request['emailAddress'];
		$comments = $request['comments'];
		$username = $request['username'];
		$password = $request['password'];
		$retypepassword = $request['retype_password'];
		$result = array();

		$validate = Validator::make($request->all(), [
            'emailAddress' => 'required|email',
        ], [
            'emailAddress.required' => 'Please input a valid email address',
        ]);
        
        if($validate->fails()){
            return Redirect::back()->withErrors($validate)->withInput();
        }

		if ($request->isMethod('post') == 'POST'){
			$error = $this->validateRegister($organizationName, $contactName, $phoneNumber, $mobilePhone, $emailAddress, $username, $password, $retypepassword);
			if (count($error) > 0){
				$result['status'] = 'danger';
				$result['message'] = $error;
				return view('login', ['result' => $result]);
				//$this->index($result);
			} else {
				/**
				$coundSupplier = DB::table('tblsupplier')
							->select('idSupplier')
							->where(function($query) use ($emailAddress, $organizationName){
								$query->where('email', $emailAddress)
									->orWhere('name', $organizationName);
							})
							->first();
							**/
				//$coundSupplier = DB::table('tblsupplier')
				//			->leftJoin('tbluser', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
				//			->select('tblsupplier.idSupplier')
				//			->where(function($query) use ($username, $organizationName){
				//				$query->where('tbluser.username',$username)
				//					->orWhere('tblsupplier.name', $organizationName);
				//			})
				//			->first();
				$coundSupplier = DB::table('tblsupplier')
							->leftJoin('tbluser', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
							->select('tblsupplier.idSupplier')
							->where('tbluser.username',$username)
							->first();
				if(!is_null($coundSupplier)){
					$result['status'] = 'warning';
					$result['message'] = 'The same username name exists in our system. Please try another one or login to continue!';
					return view('login', ['result' => $result]);
				} else{
					$tblsupplier = new tblsupplier();
					/** ADD SUPPLIER FIRST **/
					$tblsupplier->name = $organizationName;
					$tblsupplier->contactName = $contactName;
					$tblsupplier->phonenumber = $phoneNumber;
					$tblsupplier->mobilePhone = $mobilePhone;
					$tblsupplier->email = $emailAddress;
					$tblsupplier->comments = $comments;
					$tblsupplier->isOpenSaturday = 0;
					$tblsupplier->isOpenSunday = 0;
					$tblsupplier->abn = $abn;
					
					if ($tblsupplier->save()){
						$lastId = DB::table('tblsupplier')
								->select('idSupplier', 'name', 'contactName', 'email')
								->orderBy('idSupplier', 'DESC')
								->first();
						//$result['status'] = 'success';
						//$result['message'] = 'Interest has been requested. Please wait until the admin reviewed your application and send you confirmation email';
						////return view('register_user', ['status' => $status]);
						//$this->index($result);
					}
					$countUser = DB::table('tbluser')
							->select('idUser', 'idSupplier', 'username','password')
							->where('idSupplier', $lastId->idSupplier)
							->first();
					if(!is_null($countUser)){
						$result['status'] = 'warning';
						$result['message'] = 'One organization name should has only 1 account';
						return view('login', ['result' => $result]);
					} else {
						$tbluser = new tbluse();
						$tbluser->idSupplier = $lastId->idSupplier;
						$tbluser->username = $username;
						$tbluser->password = md5($password);
						$tbluser->role = '2';
						$tbluser->isActive = '0';
						$tbluser->adminApproved = '0';
						$tbluser->userStatus = '2';
						$todaydate = date('Y-m-d', strtotime('now'));
						$tbluser->registerDate = $todaydate;
						$tbluser->adminNotifications = '1';
						
						if ($tbluser->save()){
							$lastId = DB::table('tbluser')
									->select('idUser', 'idSupplier','username', 'role')
									->orderBy('idUser', 'DESC')
									->first();
							
							$webmasterId = DB::table('tblsupplier')
										->leftJoin('tbluser', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
										->select('tblsupplier.name', 'tblsupplier.email')
										->where([
											'tbluser.role' => 1,
											'tbluser.adminNotifications' => 1
										])
										->first();
										
							$verification_code = str_random(30);
							$user_confirmation = new user_confirmation;
							$user_confirmation->idSupplier = $lastId->idSupplier;
							$user_confirmation->idUser = $lastId->idUser;
							$user_confirmation->token = $verification_code;
							
							if ($user_confirmation->save()){
								$result['status'] = 'success';
								$result['message'] = 'Interest has been requested. Please wait until the admin reviewed your application and send you confirmation email';
								$subject = "Please verify your email address.";
								
								$attachement_1 ='assets/attachments/Ezyskips_registration_guideline.docx';
								$attachement_2 ='assets/attachments/set_up_form.pdf';
								Mail::send('mails.mail_regiser_user', ['name' => $contactName, 'organizationName' => $organizationName,
								'phoneNumber' => $phoneNumber, 'mobilePhone' => $mobilePhone, 'emailAddress' => $emailAddress,
								'comments' => $comments, 'username' => $username,'verification_code' => $verification_code],
								function($mail) use ($emailAddress, $contactName, $subject, $attachement_1, $attachement_2){
									$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
									$mail->to($emailAddress, $contactName);
									$mail->subject($subject);
									$mail->attach(public_path($attachement_1),[
										'mime' => 'application/pdf',
									]);
									$mail->attach(public_path($attachement_2),[
										'mime' => 'application/pdf',
									]);
								});
								
								$adminEmail = $webmasterId->email;
								$adminName = $webmasterId->name;
								$subject1 = "An user needs to be approved";
								Mail::send('mails.mail_register_user_admin', ['name' => $organizationName, 'email' => $emailAddress, 'phoneNumber' => $phoneNumber, 'comments' => $comments],
								function($mail) use ($adminEmail, $adminName, $subject1){
									$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
									$mail->to($adminEmail, $adminName);
									$mail->subject($subject1);
								});
								
							}
							return view('login', ['result' => $result]);
							//$this->index($result);
						}
					}
				}
			}
		}
	}
	
	public function verifyUser(Request $request){
		$verification_code = $request['verification_code'];
		$check = DB::table('user_confirmation')
		->select('idUser')
		->where('token',$verification_code)->first();
		if(!is_null($check)){
			$tbluser = new tbluse();
			$user = DB::table('tbluser')
				->select('idUser', 'isActive', 'idSupplier')
				->where('idUser',$check->idUser)
				->first();
			if($user->isActive == 1){
				$result['status'] = 'warning';
				$result['message'] = 'User has been actived';
				return view('login', ['result' => $result]);
			}
			$tbluser::where('idUser', $check->idUser)
			->update(['isActive' => 1]);
			DB::table('user_confirmation')->where('token',$verification_code)->delete();
			$result['status'] = 'success';
			$result['message'] = 'Interest has been verified. We will review your interest and confirm your registration';
			return view('login', ['result' => $result]);
		} else {
			$result['status'] = 'danger';
			$result['message'] =  'Link is expired. For further info, please contact webmaster';
			return view('login', ['result' => $result]);
		}
	}
	
	public function login(Request $request){
		$username = $request['username'];
		$password = md5($request['password']);
		
		$check = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email','tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tbluser.username' => $username,
				'tbluser.password' => $password,
				'isActive' => 1,
				'adminApproved' => 1,
				'userStatus' => 1
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email','tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		
		if (!is_null($check)){
			$request->session()->put('username', $check->username);
			$request->session()->put('idUser', $check->idUser);
			$request->session()->put('idSupplier', $check->idSupplier);
			$request->session()->put('orgName', $check->name);
			$request->session()->put('conName', $check->contactName);
			$request->session()->put('email', $check->email);
			$request->session()->put('role', $check->role);
			return view('dashboard',['data' => $check]);
			//$this->dashboard($check);
		} else {
			$result = array();
			$result['status'] = 'danger';
			$result['message'] = 'Invalid Credentials';
			return $this->index($result);
		}
	}
	
	public function debugmode_login(Request $request){
		$username = $request['username'];
		$email = $request['email'];
		
		$check = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email','tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tbluser.username' => $username,
				'tblsupplier.email' => $email,
				'isActive' => 1,
				'adminApproved' => 1,
				'userStatus' => 1
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email','tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		
		if (!is_null($check)){
			$request->session()->put('username', $check->username);
			$request->session()->put('idUser', $check->idUser);
			$request->session()->put('idSupplier', $check->idSupplier);
			$request->session()->put('orgName', $check->name);
			$request->session()->put('conName', $check->contactName);
			$request->session()->put('email', $check->email);
			$request->session()->put('role', $check->role);
			return view('dashboard',['data' => $check]);
			//$this->dashboard($check);
		} else {
			$result = array();
			$result['status'] = 'danger';
			$result['message'] = 'Invalid Credentials';
			return $this->index($result);
		}
	}
	
	public function dashboard($data){
		return view('dashboard',['data' => $data]);
	}

	public function logout(Request $request){
		$request->session()->flush();
		$result['status'] = 'primary';
		$result['message'] = 'You have logged out';
		return redirect()->route('home');
	}
	
	/** mail extension **/
	public function mail_extension(Request $request){
		$name = $request['name'];
		$verification_code = $request['verification_code'];
		return view('mails.registration_extension', ['name' => $name, 'verification_code' => $verification_code]);
	}
	
	/** forgot password ** new **/
	public function forget_password_index($result = null){
		if(!session('role')){
			return view('forgot_password', ['result' => $result]);
		} else{
			return redirect()->route('details_service_zone');
		}
	}
	
	private function find_supplier($username, $password){
		$supplier = DB::table('tblsupplier')
			->leftJoin('tbluser', 'tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.contactName', 'tblsupplier.email')
			->where(function($query) use ($username, $password){
				$query->where('tbluser.username',$username)
					->where('tbluser.password', md5($password));
			})
			->first();
		return $supplier;
	}
	
	public function forgot_password_submit(Request $request){
		$username = $request['username'];
		$password = $request['password'];
		$email = $request['email'];
		$retype_password = $request['password'];
		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'username' => 'required|string',
				'password' => 'required|string',
				'email' => 'required|email',
				'retype_password' => 'required|same:password'
			], [
				'retype_password.same' => 'Password mismatch',
			]);
			if($validate->fails()){
				return Redirect::back()->withErrors($validate)->withInput();
			} else {
				
				$supplier = DB::table('tbluser')
				->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
				->select('tblsupplier.idSupplier')
				->where(['tblsupplier.email' => $email])
				->first();
				
				if(!is_null($supplier)){
					$user = new tbluse();
					$user::where(['username' => $username, 'idSupplier' => $supplier->idSupplier])
					->update([
						'password' => md5($password)
					]);
					if ($user){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating supplier password');
						
						$supplier = $this->find_supplier($username, $password);
						if (!is_null($supplier)){
							$contactName = $supplier->contactName;
							$emailAddress = $supplier->email;
							/** send email **/
							$subject = "Your password has been changed.";
							Mail::send('mails.mail_forgotpassword', ['name' => $contactName, 'emailAddress' => $emailAddress],
							function($mail) use ($contactName, $emailAddress, $subject){
								$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
								$mail->to($emailAddress, $contactName);
								$mail->subject($subject);
							});
						} else {
							$request->session()->flash('status', 'danger');
							$request->session()->flash('message', 'Supplier not found. Error !');
							return redirect()->route('forgot_password_view');
						}
						
						return redirect()->route('forgot_password_view');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update supplier password failed');
						return redirect()->route('forgot_password_view');
					}
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'System can not find specified user.');
					return redirect()->route('forgot_password_view');
				}
				
			}
		} 
	}
}
