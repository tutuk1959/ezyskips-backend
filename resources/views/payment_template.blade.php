<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Ezyskips Online Supplier Zone</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
		
</head>
<body >
	<div class="top-header">
		<div class="container">
			<div class="row align-items-top">
				<div class="col-12">
					<div class="header text-center py-2">
						<a href="{{url('/away')}}" class="logo"><img alt="Logo" src="{{url('assets/images/skipbin-logo_03.png')}}" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-title" style="background-color:#005343;color:#fff;">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center py-2">
					<h1>Payment</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
			<div class="row">
				@section('payment_content')
				@show
			</div>
		</div>
	</div>
	
	<!-- END content-page -->
	
<!-- END main -->
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
<script src="{{url('assets/js/md5.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkoo9V1r_gXHCkBm0ALeI6i6hoFSpIxXg&libraries=places"></script>
<script src="{{url('assets/js/jquery.geocomplete.js')}}"></script>
<script>
	$(function(){
		var defaultBounds = new google.maps.LatLngBounds(
			new google.maps.LatLng(-31.908720437900552, 115.8882572),
			new google.maps.LatLng(-31.908720437900552, 115.8882572)
		);
		
		var input = document.getElementById('autocomplete');
		var options = {
			bounds: defaultBounds,
			types: ['geocode'],
			componentRestrictions: {'country':'AU'}
		};
		
		$('[data-geo="postal_code"]').val('');
		$('#autocomplete').on('keyup', function(){
			if($(this).val().length > 6){
				$("#autocomplete").geocomplete({
					details: "#form_geo",
					types: ["geocode", "establishment"],
					componentRestrictions: {'country':'AU'},
					bounds: defaultBounds,
					detailsAttribute: "data-geo"
				});
			}
		});
		
		$("#paynow").hide();
		$('#submit').on('click', function(){
			$("#paynow").show();
		})
	});
</script>
</body>
</html>