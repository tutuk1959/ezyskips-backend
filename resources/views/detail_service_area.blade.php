@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Details & Service Area</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Detais & Service Zone</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('websiteAdminContact'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminContact') }}
								</div>
							@endif
							@if ($errors->has('websiteAdminPhone'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminPhone') }}
								</div>
							@endif
							@if ($errors->has('websiteAdminMobile'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminMobile') }}
								</div>
							@endif
							@if ($errors->has('customerServicesEmail1'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('customerServicesEmail1') }}
								</div>
							@endif
							@if ($errors->has('password'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('password') }}
								</div>
							@endif
							
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
							
							@if (!empty($result))
								@if ($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										@foreach ($result['message'] as $message)
											{{$message}} <br />
										@endforeach
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Supplier information for: </strong> {{ $mainServiceArea->name}}, {{$mainServiceArea->contactName}} 
								</div>
									
								<div class="card-body">
									<form action="/editSupplierDetails" method="post">
										{{csrf_field()}}
										<input type="hidden" value="{{$supplierData->idSupplier}}" name="idSupplier">
										<input type="hidden" value="{{$supplierData->idUser}}" name="idUser">
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminContact">Website Admin Contact</label>
												<input id="websiteAdminContact" type="text" class="form-control" name="websiteAdminContact" value="{{$supplierData->contactName}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServicesContact">Customer Services Contact</label>
												<input id="customerServicesContact" type="text" class="form-control" name="customerServicesContact" value="{{$supplierData->customerServiceContact}}" />
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminPhone">Website Admin Phone</label>
												<input id="websiteAdminPhone" type="text" class="form-control" name="websiteAdminPhone" value="{{$supplierData->phonenumber}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServicePhone">Customer Services Phone</label>
												<input id="customerServicePhone" type="text" class="form-control" name="customerServicePhone" value="{{$supplierData->customerServicePhone}}"/>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminMobile">Website Admin Mobile</label>
												<input id="websiteAdminMobile" type="text" class="form-control" name="websiteAdminMobile" value="{{$supplierData->mobilePhone}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServiceMobile">Customer Services Mobile</label>
												<input id="customerServiceMobile" type="text" class="form-control" name="customerServiceMobile" value="{{$supplierData->customerServiceMobile}}" />
											</div>
											
										</div>
										<div class="form-group">
											<label for="customerServicesEmail1">Email 1</label>
											<input id="customerServicesEmail1" type="text" class="form-control" name="customerServicesEmail1" value="{{$supplierData->email}}" required=""/>
										</div>
										<div class="form-group">
											<label for="customerServicesEmail2">Email 2</label>
											<input id="customerServicesEmail2" type="text" class="form-control" name="customerServicesEmail2" value="{{$supplierData->email2}}" />
										</div>
										
										<div class="form-group">
											<label for="fullAddress">Address</label>
											<textarea name="fullAddress" id="fullAddress" class="form-control">{{$supplierData->fullAddress}}</textarea>
										</div>
										<div class="form-group">
											<label for="fullAddress">ABN</label>
											<input id="abn" type="text" class="form-control" name="abn" value="{{$supplierData->abn}}" />
										</div>
										<div class="form-group">
  											<p><strong>By ticking these fields, your bin hire services are eligible to be booked on the weekend.</strong> <strong class="text-danger">If you not ticking these fields, then the system will exclude you from the weekend bookings. </strong></p>
										</div>
										<div class="form-group">
  											<input class="openSaturday" name="openSaturday" type="checkbox" <?=($supplierData->isOpenSaturday == 1) ? 'checked' : ''?> id="openSaturday">
  											<label class="form-check-label" for="openSaturday">
   					 							Open on Saturday
  											</label>
										</div>
										<div class="form-group">
  											<input class="openSunday" name="openSunday" type="checkbox"  <?=($supplierData->isOpenSunday == 1) ? 'checked' : ''?>  id="openSunday">
  											<label class="form-check-label" for="openSunday">
   					 							Open on Sunday
  											</label>
										</div>
										<!--<div class="form-group">
  											<input class="password" name="password" type="text" id="password">
  											<label class="form-label" for="password">
   					 							Password
  											</label>
										</div>
										<div class="form-group">
  											<input class="confirmPassword" name="confirmPassword" type="checkbox" id="confirmPassword">
  											<label class="form-label" for="confirmPassword">
   					 							Confirm Password
  											</label>
										</div>-->

										<button name="submit" type="submit" class="btn btn-primary">Save User Detail</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>

						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>User credentials for: </strong> {{ $mainServiceArea->name}}, {{$mainServiceArea->contactName}} 
								</div>
									
								<div class="card-body">
									<form action="/editUserDetails" method="post">
										{{csrf_field()}}
										<input type="hidden" value="{{$supplierData->idUser}}" name="idUser">
										<input type="hidden" value="{{$supplierData->idSupplier}}" name="idSupplier">
										<div class="form-row">
											<div class="col-12">
												<label for="username">Username</label>
												<input id="username" type="text" class="form-control" name="username" value="{{$supplierData->username}}" required=""/>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="password">Password</label>
												<input id="password" type="password" class="form-control" name="password" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="confirmPassword">Confirm Password</label>
												<input id="confirmPassword" type="text" class="form-control" name="confirmPassword" required=""/>
											</div>
											
										</div>
										
										<button name="submit" type="submit" class="btn btn-primary">Save User Detail</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>

						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Service area for: </strong> {{ $mainServiceArea->name}}, {{$mainServiceArea->contactName}} , Western Australia
									@if(session('role'))
										@if(session('role') == '1')
											<a href="{{url('/serviceareaimporter')}}" class="btn btn-primary float-right d-none d-lg-block">CSV Importer for Area Data</a>
										@endif
									@endif
								</div>
									
								<div class="card-body">
									@if(session('role'))
										@if(session('role') == '1')
											<a href="{{url('/serviceareaimporter')}}" class="btn btn-primary mb-3 d-block d-lg-none">CSV Importer for Area Data</a>
										@endif
									@endif
									
									<div id="accordion" role="tablist">
										<?php $i = 1;?>
										@if(!is_null($serviceAreaParent))
											@foreach ($serviceAreaParent as $data)
											<div class="card mb-2">
												<div class="card-header" role="tab" id="headingOne">
									  				<h5 class="mb-0">
														<a data-toggle="collapse" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
										  				{{$data->area}}
														</a>
									  				</h5>
												</div>

												<div id="collapse{{$i}}" class="collapse <?=($i == 1) ? 'show' : ''?>" role="tabpanel" aria-labelledby="heading{{$i}}" data-parent="#accordion">
									  				<div class="card-body">
														<div class="form-group">
															<input class="selectall" name="selectall"  type="checkbox" value="selectall" id="selectall">
  															<label class="form-check-label" for="zipcode">
   					 											Select All
  															</label>

  															<input class="unselectall" name="unselectall"  type="checkbox" value="unselectall" id="unselectall">
  															<label class="form-check-label" for="zipcode">
   					 											Un-select All
  															</label>
  														</div>
									  					<form class="areaform" action="/editArea/{{$data->zipcode}}" method="post">
									  						{{csrf_field()}}
									  						<input type="hidden" name="zipcodeparent" value="{{$data->zipcode}}">
									  						<input type="hidden" value="{{$supplierData->idUser}}" name="idUser">
															<input type="hidden" value="{{$supplierData->idSupplier}}" name="idSupplier">
									  						<?php $index = 0;?>
															
									  						@if(!is_null($childArea))
																@foreach ($childArea[$data->idArea][$data->zipcode] as $child)
																	<div class="form-group">
																		<?php $status = '';?>
																		@if(!is_null($childServiceArea))
																		<?php foreach($childServiceArea[$child->parentareacode][$child->idArea] as $servArea):?>
																			<?php if($servArea->idServiceArea == $child->idArea):?>
																				<?php $status = 'checked';break;?>
																			<?php else:?>
																				<?php $status = '';?>
																			<?php endif;?>
																		<?php endforeach;?>
																		@else
																			<?php $status = '';?>
																		@endif
																		<input class="zipcode" name="idArea[]" <?=(!is_null($status) ? $status : '')?> type="checkbox" value="{{$child->idArea}}" id="zipcode">
																		<label class="form-check-label" for="zipcode">
																				{{$child->zipcode}} - {{$child->area}}
																		</label>
																	</div>
	
																	<?php $index++;?>
																@endforeach
															@endif
									  						<button name="submit" type="submit" class="btn btn-primary">Save Area</button>
									  					</form>
													</div>
												</div>
											</div>
											<?php $i++?>
											@endforeach
										@endif
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
