@extends('template')
@section('content')
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<h1 class="main-title float-left">Instructions & Information</h1>
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						
						@if (!empty(session('access_violated_status')))
							@if (session('access_violated_status') == 'danger')
								<div class="alert alert-danger" role="alert">
									{{session('access_violated_message')}} <br />
								</div>
							@endif
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card mb-3">
							<div class="card-header">
								<h3><i class="fa fa-bell-o"></i> Welcome, {{session('username')}} !</h3>
							</div>
					
							<div class="card-body">
								<strong>Instructions and Information for Suppliers</strong>
								<p class="text-primary"><strong class="text-danger">Deliveries:</strong> All bins must be delivered to the customer by 1.00pm on the date of their scheduled delivery or on a day prior to the delivery date. Please contact the customer by phone if the delivery is going to be delayed beyond this time. 
								</p>

								<p class="text-primary">Bins must not be removed until the collection date or on a day following the collection date.</p>
								
								<p class="text-primary"><strong class="text-danger">Note:</strong> Please ensure you click on the 'save' button following any amendments to details.</p>
								<strong>Initial Setup:</strong>
								<ol>
									<li style="list-style-type:decimal">
										<p>Click on the “Details & Servicing Zones” page – please check that all details are correct.</p>
										<p class="text-danger"><strong>Note: You can nominate up to 2 emails that the Ezyskips Online job orders will be sent to. If you require more please let us know so we can arrange this.</strong></p>
									</li>
									<li style="list-style-type:decimal">
										<p>Scroll down to "Saturday & Sunday Deliveries" - if you provide a bin delivery service on Saturdays and/or Sundays mark the box.</p>
										<p>If the box is left empty the program will assume that you are not able to deliver skip bins on Saturdays and/or Sundays.</p>
										<p><strong class="text-danger">Note: Do not leave Saturdays and/or Sundays on your "manage rates" pages empty.</strong>When looking for skip bin availability the program will check thru all the days a customer requires a skip bin to ensure you have sufficient stock on each day. If you leave Saturdays and/or Sundays blank the program will assume you do not have sufficient stock over the weekend to service the customer. </p>
										<strong class="text-danger">Make sure you set your preferred password and confirm.</strong>
										<p>Save any changes as you go.</p>
									</li>
									<li style="list-style-type:decimal">
										<p>Scroll down to ‘Service Area’ please tick all postcodes for areas that you service. Please contact us if there are areas you service that are not listed.</p>
										<strong class="text-danger">Note: please ensure you click on the 'save' button once all postcodes have been entered or if you make any changes to postcode areas serviced.</strong>
										
									</li>
								</ol>
								
								<strong>Manage Pricing & Stock Quantities:</strong>
								<ol>
									<li style="list-style-type:decimal">
										<p>The website has 4 schedules:</p>
										<ul>
											<li>General Waste  - Light Domestic and Commercial Waste</li>
											<li>Mixed Heavy Waste  - Domestic, Commercial, Demolition, Construction, Renovation</li>
											<li>Green Waste - Green garden waste</li>
											<li>Soil / Dirt - 100% pure soil / dirt</li>
										</ul>
										<p></p>
									</li>
									<li style="list-style-type:decimal">
										<p>Click on the schedule containing the waste type you want to update;</p>
									</li>
									<li style="list-style-type:decimal">
										<p>Prices entered in the schedule grid are to be inclusive of bin hire (for the number of days you specify), all bin delivery and removal transport costs, all tip fees based on a weight allowance of 150kg per cubic metre and GST if applicable.</p>
										
									</li>
									<li style="list-style-type:decimal">
										<p>Costs for additional bin hire days and tip fees (for general waste and green waste) for bins in excess of 150kg per cubic meter must be entered in the first section of each schedule.</p>
										<p><strong class="text-danger">Note:  please ensure you click the 'save' button for the costs to be effective.</strong></p>
										
									</li>
									<li style="list-style-type:decimal">
										<p>Click on the Blue Pencil at the end of the grid. This will allow you to make changes to you pricing and stock levels.</p>
										<p><strong class="text-danger">Note: the price quoted to the customer will be the price entered into the grid for the first day of service (the pricing is not a daily charge); additional days hire will be calculated automatically if applicable.</strong></p>
										<p>Example: For a skip bin ordered from Friday to Monday – the price the customer is quoted is the price entered into the grid for Friday.</p>
										<p><strong class="text-danger">Note: please ensure you click the 'save' button after you have entered your stock and prices.</strong></p>
									</li>
									<li style="list-style-type:decimal">
										<p>If you have standard bin prices and stock availability please enter these in the ‘Default Column’ on the schedules – these rates will automatically extend out to the individual dates. To then flex individual days depending on stock availability and pricing you can adjust the daily numbers by following the instructions directly above the grid.</p>
										<p><strong class="text-danger">Note: the default pricing and stock availability will automatically be extended to new dates as time progresses.</strong></p>
									</li>
									<li style="list-style-type:decimal">
										<p>Bin Orders you receive via ezyskipsonline.com.au will be automatically deducted from your available bin stocks for each day of hire. Any other changes to stock levels and pricing must be managed by the supplier via the schedules.</p>
									</li>
									<li style="list-style-type:decimal">
										<p>If you require help setting up your web pages please contact the Sales & Marketing team on <a href="mailto:info@ezyskipsonline.com.au">info@ezyskipsonline.com.au</a> or <a href="tel:0410704294">0410 704 294</a></p>
									</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection
