@extends('template')
@section('content')
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<h1 class="main-title float-left">Details & Service Area</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item">Detais & Service Zone</li>
									<li class="breadcrumb-item active">Service Area Importer</li>
								</ol>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						@if (!empty(session('status')))
							@if (session('status') == 'danger')
								<div class="alert alert-danger" role="alert">
										{{session('message')}} <br />
								</div>
							@elseif (session('status') == 'success')
								<div class="alert alert-success" role="alert">
										{{session('message')}} <br />
									
								</div>
							@endif
						@endif
					</div>
				</div>
				
				<div class="row">
					<div class="col-12">
						<div class="card mb-3 mt-2">
							<div class="card-header">
								<strong>CSV Importer for Service Area Data</strong>
							</div>
								
							<div class="card-body">
								<form action="{{ route('import_parse') }}" method="post" enctype="multipart/form-data">
									{{csrf_field()}}
									
									<div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
										<label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

										<div class="col-md-6">
											<input id="csv_file" type="file" class="form-control" name="csv_file" required>
		
											@if ($errors->has('csv_file'))
												<span class="help-block">
													<strong style="color:#ca0202">{{ $errors->first('csv_file') }}</strong>
												</span>
											@endif
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="header" checked> File contains header row?
												</label>
											</div>
										</div>
									</div>
									
									<button name="submit" type="submit" class="btn btn-primary">Import CSV</button>
								</form>
							</div>
						</div><!-- end card-->
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
