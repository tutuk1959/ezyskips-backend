@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Webmaster User Management</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item"><a href="/manage_user">User Management</a></li>
									<li class="breadcrumb-item active">Active User List </li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									Active User List
								</div>
									
								<div class="card-body">
									<a href="{{ url('/') }}/manage_user/" class="btn btn-primary mr-2">Pending Users List</a>
									<a href="{{ url('/') }}/banned_user/" class="btn btn-primary">Banned Users List</a> <br/>
									<?php if (count($userdata) > 0):?>
										<div class="table-responsive">
											<table class="table table-responsive table-bordered" style="border:none;">
												<thead>
													<th>Supplier Name</th>
													<th>Supplier Contact Name</th>
													<th>Supplier Email</th>
													<th>Supplier Phone Number</th>
													<th>Username</th>
													<th>Register Date</th>
													<th>Action</th>
												</thead>
												<tbody>
													@foreach($userdata as $user)
														<tr>
															<td><a href="{{ url('/') }}/user_details_service_zone/{{$user->idUser}}">{{$user->name}}</a></td>
															<td>{{$user->contactName}}</td>
															<td>{{$user->email}}</td>
															<td>{{$user->phonenumber}}</td>
															<td>{{$user->username}}</td>
															<td><?php echo date('d-m-Y', strtotime($user->registerDate))?></td>
															
															<td>
																@if($user->adminApproved == '1' && $user->isActive == '1' && $user->userStatus == '1')
																	<a href="{{ url('/') }}/kick_user/{{$user->idUser}}" class="mb-2 float-left">
																		<button name="button" type="button" class="btn btn-danger">
																			<i class="fa fa-trash"></i> Kick User
																		</button>
																	</a>
																@endif
															</td>
														</tr>	
													@endforeach
												</tbody>
											</table>
										</div>
									<?php else: ?>
										No active user list
									<?php endif;?>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
