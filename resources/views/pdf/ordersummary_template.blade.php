
<htmlpageheader name="page-header">
	Annual Order Summary
</htmlpageheader>
<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					body{
						font-size:11px;
						line-height:1.9px;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.7;
					}
					table{
						line-height:1.7;
					}
				} 
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr >
						<td width="50%" style="text-align:left">
							<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								Ezyskips Online<br/>
								PO Box 14 Woodvale<br/>
								6026 <br/>
								0410 704 294<br/>
								info@ezyskipsonline.com.au<br/>
							</address>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="table-responsive">
				<table class="table table-condensed" border="0" cellpadding="3">
					<tr>
						<td>
							<h3 style="text-transform:uppercase;">Order Summary</h3> <br />
							<strong style="text-transform:uppercase;">Date:</strong> {{$startDate}} - {{$endDate}}</strong> <br />
							<strong style="text-transform:uppercase;">Supplier: {{$selected_supplierdata->name}}</strong>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr>
						<td>
							<h3 style="text-transform:uppercase;">Supplier Details:</h3>
							<address >
								{{$selected_supplierdata->name}}<br/>
								{{$selected_supplierdata->email}} <br/>
								{{$selected_supplierdata->phonenumber}} <br/>
								{{$selected_supplierdata->fullAddress}} <br/>
							</address>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="table-responsive">
				
				<table class="data table table-bordered" border="1" style="border:1px solid #b1b2b2;" width="100%" cellpadding="3">
					<tr class="text-center" align="center" style="color:#fff !important;background:#005343;">
						<td><strong style="color:#fff ">Order Ref</strong></td>
						<td><strong style="color:#fff ">Order Date</strong></td>
						<td><strong style="color:#fff ">Bin Type</strong></td>
						<td><strong style="color:#fff ">Bin Size</strong></td>
						<td><strong style="color:#fff ">Collection Date</strong></td>
						<td><strong style="color:#fff ">Revenue</strong></td>
					</tr>
					<tbody>
						<?php $sum = 0;$sub=0;?>
						@foreach($suppliesdata as $data)
							<tr>
								<td><a href="{{ url('/') }}/order_detail/{{$data->paymentUniqueCode}}/{{$data->idSupplier}}/{{$data->idCustomer}}/{{$data->idBinType}}/{{$data->idBinService}}">{{$data->paymentUniqueCode}}</a></td>
								<td >{{date('d-m-Y', strtotime($data->orderDate))}}</td>
								<td >{{$data->bintypename}}</td>
								<td >{{$data->binsize}}</td>
								<td >{{date('d/m/Y', strtotime($data->collectionDate))}}</td>
								<td>
									<?php
										$gst = $data->subtotal*0.10;
										$subtotal = $data->subtotal;
										$sub = $sub+ $subtotal;
										$sum = 0.85*$sub;
									?>
									${{sprintf('%1.2f',$data->subtotal)}}
								</td>	
							</tr>	
						@endforeach
							<tr>
								<td colspan="5" class="text-center">
									<strong>Subtotal</strong>
								</td>
								<td>
									<strong>${{sprintf('%1.2f',$sub)}}</strong>
								</td>
							</tr>
							<tr>
								<td colspan="5" class="text-center">
									<strong>Grand Total (After deducted 15% for Ezyskips Online)</strong>
								</td>
								<td>
									<strong>${{sprintf('%1.2f',$sum)}}</strong>
								</td>
							</tr>
					</tbody>
				</table>		
			</div>
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>