<htmlpageheader name="page-header">
	Bin Hire Quote from Invoice {{$invoiceDetails->paymentUniqueCode}}
</htmlpageheader>
<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					body{
						font-size:11px;
						line-height:1.9px;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.7;
					}
					table{
						line-height:1.7;
					}
				} 
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr style="text-align:center">
						<td width="50%">
							<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								Ezyskips Online<br/>
								PO Box 14 Woodvale<br/>
								6026 <br/>
								0410 704 294<br/>
								info@ezyskipsonline.com.au<br/>
								ABN : 44 331 419 402
							</address>
						</td>
					</tr>
					<tr style="text-align:center">
						<td colspan="2">
							<h3 style="text-transform:uppercase;">Order Slip </h3> <br />
							<strong style="text-transform:uppercase;">Invoice no. {{$invoiceDetails->paymentUniqueCode}}</strong><br />
							<strong><?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?></strong> </strong><br />
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr>
						<td width="50%">
							<h3 style="text-transform:uppercase;">Customer Delivery Details:</h3>
							<address style="font-style:12px;">
								{{$customerdetails->name}}<br/>
								{{$customerdetails->address}} <br/>
								{{$customerdetails->zipcode}} <br/>
								{{$customerdetails->phone}} <br/>
								{{$customerdetails->email}}
							</address>
						</td>
						
					</tr>
				</table>
			</div>

			<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%" cellpadding="3">
					<tr>
						<td  width="50%" >
							<h3 style="text-transform:uppercase;">Delivery Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
							</address>
						</td>
						<td  width="50%" style="padding-left: 20px;">
							<h3 style="text-transform:uppercase;">Collection Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
							</address>
						</td>
					</tr>
				</table>
			</div>

			<div class="table-responsive">
				<table class="table table-condensed" border="1" style="border:1px solid #b1b2b2;" width="100%" cellpadding="3">
					<tbody>
						<tr style="background:#005343;">
							<td colspan="3" style="color:#fff;text-align:center;text-transform:uppercase;"><strong><h3>Order summary</h3></strong></td>
						</tr>
						<tr>
							<td ><strong>Bin type</strong></td>
							<td colspan="2" >{{$binhire->name}}</td>
						</tr>
						<tr>
							<td ><strong>Bin size</strong></td>
							<td colspan="2" >{{$binhire->size}}</td>
						</tr>
						<tr>
							<td ><strong>Description</strong></td>
							<td colspan="2" >
								<?php $tags = array("strong", "b");?>
								<p><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
								<p style="color:#c40005;"><?php echo $binhire->description; ?></p>
							</td>
						</tr>
						
						<!--<tr>
							<td  style="text-transform:uppercase;"><strong>Bin price</strong></td>
							<td colspan="2"  style="text-transform:uppercase;">${{sprintf('%1.2f',$binhire->price)}}</td>
						</tr>-->
						<!--<tr>
							<td><strong>GST 10%</strong></td>
							<?php $gst = $invoiceDetails->subtotal*0.10;?>
							<td colspan="2">${{sprintf('%1.2f',$gst)}}</td>
						</tr>-->
						<?php 
							$subtotal = $invoiceDetails->subtotal;
							$commision = 0.15*$subtotal;
							$grand_total = 0.85*$subtotal;
						?>
						<!--<tr>
							<td><strong>Fee</strong></td>
							<td colspan="2">- ${{sprintf('%1.2f',$commision)}}</td>
						</tr>-->
						<tr style="color:#fff;background:#005343">
							<td style="color:#fff;"><strong>Price charged</strong></td>
							<td colspan="2" style="color:#fff;">
							$<?php echo sprintf('%1.2f',$invoiceDetails->subtotal);?></td>
						</tr>
					</tbody>
				</table>
			</div>
			@if(!is_null($invoiceDetails->deliveryComments))
				<div class="table-responsive">
					<table class="table table-condensed" border="0" width="100%" cellpadding="3">
						<tr>
							<td width="50%">
								<h3 style="text-transform:uppercase;">Additional Note:</h3>
								<address  style="font-style:12px;">
									{{$invoiceDetails->deliveryComments}}
								</address>
							</td>
						</tr>
					</table>
				</div>
			@endif
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>