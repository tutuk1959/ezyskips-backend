@extends('login_template')
@section('login_content')
	<div class="content">
		<div class="container">
			<div class="content-wrapper">
				<div class="row">
					<div class="col-12 mb-3">
						<div class="row align-items-center">
							<div class="col-12">
								<div class="table-responsive">
									<table class="table table-condensed" border="0" width="100%">
										<tr >
											<td width="50%" style="text-align:left">
												<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
											</td>
											<td width="50%" style="padding-left: 20px;text-align:right">
												<address style="font-style:12px;">
													Ezyskips Online<br/>
													PO Box 14 Woodvale<br/>
													6026 <br/>
													0410 704 294<br/>
													info@ezyskipsonline.com.au<br/>
													ABN : 44 331 419 402
												</address>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<h3 >Sorry! Your account has been deactivated</h3><br />
						<p>Hi {{ $supplierData->contactName }},</p> 
						<p>According to Ezyskips Online policy, you have violated the policy and regulations on the Ezyskips Online. In response, your account has been ban and deactivated.</p> 
						<p>Please contact <a href="https://ezyskipsonline.com.au/supplier/">Ezyskips Online Supplier Support</a> if you wish to re-evaluate your account and become an active member again.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
