@extends('login_template')
@section('login_content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														Ezyskips Online<br/>
														PO Box 14 Woodvale<br/>
														6026 <br/>
														0410 704 294<br/>
														info@ezyskipbinonline.com.au<br/>
														ABN : 44 331 419 402
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3 class="float-left">Confirm your email</h3><br />
							<p><i><a href="{{ url('user/verify', $verification_code)}}">Click here if you can not see this email</a></i></p>
							<p>Hi {{ $name }},</p>
							<p>Thank you for creating an account with us. Don't forget to complete your registration!</p>
							<p>Please click on the link below or copy it into the address bar of your browser to confirm your email address:</p>
							<p><a href="{{ url('user/verify', $verification_code)}}" class="btn btn-primary">Confirm my email address </a></p>
							<p>After that, our webmaster will review your registration request. We will send you the confirmation email once we approve your account.</p>
							
							<p>Thank you ! <br /></p>
							<p>Good Day !</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
