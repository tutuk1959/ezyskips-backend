@extends('login_template')
@section('login_content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
			
			.bin_desc ul li{
				color:#c40005;
			}
			ul li{
				padding-left:0;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														Ezyskips Online<br/>
														PO Box 14 Woodvale<br/>
														6026 <br/>
														0410 704 294<br/>
														info@ezyskipsonline.com.au<br/>
														ABN : 44 331 419 402
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="card mb-3">
								<div class="card-body">	
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table" border="0" width="100%" cellpadding="3">
														<tr>
															<td style="text-align:left">
																<h3 style="text-transform:uppercase; ">Order slip</h3><br />
																<strong style="text-transform:uppercase;">Invoice no. {{$invoiceDetails->paymentUniqueCode}}</strong><br />
																<strong >Date: <?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?></strong>
															</td>
														</tr>
													</table>
												</div>

												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">1. Order details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Bin type</strong>
															</td>
															<td >
																{{$binhire->name}} <br />
																{{$binhire->size}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Description</strong>
															</td>
															<td  >
																<?php $tags = array("strong", "b");?>
																<p><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
																<div style="color:#c40005;" class="bin_desc"><?php echo $binhire->description; ?></div>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Delivery date</strong>
															</td>
															<td >
																<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Collection date</strong>
															</td>
															<td >
																<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
															</td>
														</tr>
														@if(!is_null($invoiceDetails->deliveryComments))
														<tr>
															<td width="30%">
																<strong >Customer additional note</strong>
															</td>
															<td >
																{{$invoiceDetails->deliveryComments}}
															</td>
														</tr>
														@endif
													</table>
												</div>
												<hr />

												
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3 style="text-transform:uppercase;">2. Customer delivery details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Name</strong>
															</td>
															<td >
																{{$customerdetails->name}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Address</strong>
															</td>
															<td  >
																{{$customerdetails->address}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Zipcode</strong>
															</td>
															<td >
																{{$customerdetails->zipcode}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Phone</strong>
															</td>
															<td >
																{{$customerdetails->phone}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong>Email</strong>
															</td>
															<td >
																{{$customerdetails->email}}
															</td>
														</tr>
													</table>
												</div>
												<hr />
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">3. Payment confirmation</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Price charged</strong>
															</td>
															<td >
																<strong>${{sprintf('%1.2f',$invoiceDetails->subtotal)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card category</strong>
															</td>
															<td >
																{{$invoiceDetails->card_category}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card type</strong>
															</td>
															<td >
																{{$invoiceDetails->card_type}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card holder</strong>
															</td>
															<td >
																{{$invoiceDetails->card_holder}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card number</strong>
															</td>
															<td >
																{{$invoiceDetails->card_number}}
															</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>	
								</div>														
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection