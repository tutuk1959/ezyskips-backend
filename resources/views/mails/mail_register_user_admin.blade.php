@extends('login_template')
@section('login_content')
	 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														Ezyskips Online<br/>
														PO Box 14 Woodvale<br/>
														6026 <br/>
														0410 704 294<br/>
														info@ezyskipsonline.com.au<br/>
														ABN : 44 331 419 402
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3 >User registration verification email</h3><br />
							<p>Hi Webmaster,</p>
							<p>There is a new user registration interest happens on Ezyskips Online.</p>
							<p>Please review this user with details as follows:</p>
							<p>Organization Name : {{$name}}</p>
							<p>Organization Email : {{$email}}</p>
							<p>Organization Phone : {{$phoneNumber}}</p>
							<p>Registration Note : {{$comments}}</p>
							<p>Once you have finished reviewing, make decision whether you approve or neglect the request !</p>
							
							<p>Good Day! <br /></p>
							<p>Happy hard working !</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
