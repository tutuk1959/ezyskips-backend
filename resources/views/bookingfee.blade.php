@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Booking Fee Management</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Booking Fee</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
						<div class="col-12">
							<a href="{{ url('/') }}/booking_form/" class="mb-2 mr-2 ">
								<button name="button" type="button" class="btn btn-primary">
									<i class="fa fa-plus"></i> Add Booking Price
								</button>
							</a>
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Price List</strong>
								</div>
									
								<div class="card-body">
									@if(!$bookingprice->isEmpty())
										<div class="table-responsive">
											<table class="table table-bordered" style="border:none;">
												<thead class="text-center">
													<th>Price</th>
													<th>Updated By</th>
													<th>Updated On</th>
													<th>Action</th>
												</thead>
												<tbody class="text-center">
													@foreach($bookingprice as $data)
														<tr>
															<td>${{$data->price}}</td>
															<td>{{$data->username}}</td>
															<td>
																<?php $timestamps = strtotime($data->updated_at);?>
																{{date('r', $timestamps)}}
															</td>
															<td>
																<a href="{{ url('/') }}/booking_fee/{{$data->idBookingPrice}}" class="mb-2 mr-2 ">
																	<button name="button" type="button" class="btn btn-primary">
																		<i class="fa fa-pencil"></i> Change
																	</button>
																</a>
															
															</td>
														</tr>	
													@endforeach
												</tbody>
											</table>		
										</div>
									@else
										{{'No data to show'}}
									@endif
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
