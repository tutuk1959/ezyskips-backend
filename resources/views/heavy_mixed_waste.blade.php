@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Mixed Heavy Waste Bin Hire</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Mixed Heavy Waste</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('extraHireage'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('extraHireage') }}
								</div>
							@endif
							@if ($errors->has('extraHireageDays'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('extraHireageDays') }}
								</div>
							@endif
							
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
							
							@if (!empty($result))
								@if ($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										@foreach ($result['message'] as $message)
											{{$message}} <br />
										@endforeach
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'warning')
									<div class="alert alert-warning" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'notif')
									<div class="alert alert-danger" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Manage Heavy Mixed Waste</strong>
								</div>
									
								<div class="card-body">
									<p>Clicking the "Save" button below will save the figures entered in these three input boxes only. All rates and inventory figures are saved on a per-row basis when the "tick" button is clicked".<p>
									<form class="mb-2" action="/editMiscDetails/2" method="POST">
										{{csrf_field()}}
										<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
										<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
										<input type="hidden" name="idBinType" value="2">
										<div class=" form-row mb-2"> 
											<div class="col-12">
												<label for="extraHireage"><strong>Extra Hireage:</strong></label>
											</div>
											<div class="col-12 col-md-4">
												<div class="row">
													<div class="col-1">
														<span>$</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->extraHireagePrice}}" id="extraHireage" type="text" class="form-control ml-2 mr-2" name="extraHireage" required=""/>
														@else
															<input id="extraHireage" type="text" class="form-control ml-2 mr-2" name="extraHireage" required=""/>
														@endif
														
													</div>
													<div class="col-3 text-center">
														<span>extra hire per day after</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->extraHireageDays}}" id="extraHireageDays" type="text" class="form-control ml-2 mr-2" name="extraHireageDays"  required=""/>
														@else
															<input id="extraHireageDays" type="text" class="form-control ml-2 mr-2" name="extraHireageDays"  required=""/>
														@endif
													</div>
													<div class="col-2">
														<span>days</span>
													</div>
												</div>
											</div>
										</div>

										<button name="submit" type="submit" class="btn btn-primary">Save</button>
									</form>
									
									<p>Use this page to manage your rates:</p>
									<ul>
										<li style="list-style-type:none">
											<i class="fa fa-pencil mr-2"></i> Click on the edit pencil button to change your daily rates. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-check mr-2"></i>  Click on the tick button to save your changes. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-trash mr-2"></i> Click on the delete trash button to reset all your rates and inventory to 0 for the selected bin size. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-ban mr-2"></i> Click on each checkbox to block any delivery dates. Skip bins will not be offered for dates that are checked.
										</li>
									</ul>
									
									<div class="schedule-wrapper">
										<div class="row">
											<div class="col-12">
												@if(!is_null($offset))
    												<?php $offsetPage = $offset; ?>	
    											@else
    												<?php $offsetPage = $dateNow;?>
    											@endif
												<strong class="mb-2">{{$dateNow}}</strong>

												<table class="table table-responsive table-bordered" style="border:none;">
													
  													<tr>

    													<th class="tg-yw4l" colspan="4">
    														<?php $prevDate = date('d-m-Y', $offsetPage)?>
    														<a href="{{ url('/') }}/2/calendarOffset/<?= date('d-m-Y', strtotime($prevDate.'-2 weeks'))?>">Prev</a>
    													</th>
    													<th class="tg-yw4l" style="background:#eaeaea">Base</th>
    													
														
														@for ($i = 0; $i < 14; $i++)
															<?php $a[] = $offsetPage+($i*24*60*60);?>
    														<th class="tg-yw4l"><?php echo date('D d-m', $a[$i])?></th>
    														<?php $lastDate = date('d-m-Y', $a[$i]);?>
														@endfor
    													<th class="tg-yw4l"><a href="{{ url('/') }}/2/calendarOffset/<?= date('d-m-Y', strtotime($lastDate.'+1 day'))?>">Next</a></th>
  													</tr>

  													<tr>
    													<td class="tg-lqy6" colspan="5">Non Delivery Days</td>
    									
    													<form action="{{ url('/') }}/2/editNonDeliveryDays" method="post">
    														{{csrf_field()}}
    														<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
															<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
															<input type="hidden" name="idBinType" value="2">
															<?php $status = '';?>
									  						
    														@for ($i = 0; $i < 14; $i++)
															<?php $a[] = $offsetPage+($i*24*60*60);?>
																<?php $get_day_now = date('D', $a[$i]); ?>
																@if($get_day_now == 'Sat')
																	@if($supplierData->isOpenSaturday == 0)
																		<?php $status = 'checked';?>
																	@else
																		<?php $status = '';?>
																	@endif
																@elseif($get_day_now == 'Sun')
																	@if($supplierData->isOpenSunday == 0)
																		<?php $status = 'checked';?>
																	@else
																		<?php $status = '';?>
																	@endif
																@else 
																	<?php $status = '';?>
																@endif
																
																@if(!is_null($nonDelivery))
									  								<?php foreach($nonDelivery as $dataDelivery):?>
																		<?php if($dataDelivery->date == date('Y-m-d', $a[$i])):?>
																			<?php $status = 'checked';break;?>
																		<?php else:?>
																			<?php $status = '';?>
																		<?php endif;?>
																	<?php endforeach;?>
																@else
																		<?php $status = '';?>
																@endif
																
																
    															<th class="tg-yw4l">
    																<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																<input name="nondelivery[<?=$i?>]" <?=$status;?> type="checkbox" id="nondelivery">
    															</th>
															@endfor
															<td class="tg-yw4l">
																<button name="submit" type="submit" class="btn btn-danger">
																	<i class="fa fa-ban"></i>
																</button>
															</td>
    													</form>
  													</tr>
  													<!---BIN PRICE -->
  													
  													@foreach($getBinSize as $size)
  														
    													<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
														<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
														<input type="hidden" name="idBinType" value="2">
														<input type="hidden" name="idBinSize" value="{{$size->idSize}}">
																<tr>
																	<td class="tg-baqh" colspan="3" rowspan="2">{{$size->size}}</td>
																	<td class="tg-yw4l">Price</td>
																	<td style="background:#eaeaea">
																		<strong>
																		<?php $value = 0?>
																		@if(!is_null($baseprice))
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																						<?php $value = $items->baseprice ?>
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																		@else
																			$value = 0;
																		@endif
																		{{$value}}
																		</strong>
																	</td>
																	@for ($i = 0; $i < 14; $i++)
																		<?php $a[] = $offsetPage+($i*24*60*60);?>
																		
																		@if(!is_null($baseprice))
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							<?php $value = 0 ?>
																						@else
																							<?php $value = $baseitems->baseprice ?>
																						@endif
																					<?php endforeach?>
																					
																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if(is_null($baseitems->baseprice))
																							@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																								<?php $value =  $value1; break;?>
																							@endif
																						@else
																							@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																								<?php $value =  $itemsvalue->price; break;?>
																							@endif
																						@endif
																						
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																		@else
																			<?php $value = 0?>
																		@endif
																		
																		<td class="tg-yw4l">
																			{{$value}}
																		</td>
																	@endfor
																	<td class="tg-yw4l" rowspan="2">
																		<a href="{{ url('/') }}/2/showEditPricingForm/2/{{$size->idSize}}" class="mb-2 float-left">
																			<button name="button" type="button" class="btn btn-primary">
																				<i class="fa fa-pencil"></i>
																			</button>
																		</a>
																		<form action="{{ url('2/resetPricing', ['bintype' => 2, 'binsize' => $size->idSize]) }}" method="post">
																				<input type="hidden" name="_method" value="delete" />
																				{{csrf_field()}}
																				<button name="button" type="submit" class="btn btn-warning">
																					<i class="fa fa-trash"></i>
																				</button>
																		</form>
																	</td>
																</tr>
																<tr>
    																<td class="tg-yw4l">Stock</td>
    																<td style="background:#eaeaea">
    																	<strong>
    																	<?php $var = 0;?>
    																	@if(!is_null($baseprice))
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																						<?php $var = $items->default_stock; ?>
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																		@else
																			<?php $var = 0?>
																		@endif
																		{{$var}}
																	</strong>
    																</td>
    																<?php $data = '';?>
																	@for ($i = 0; $i < 14; $i++)
																		<?php $a[] = $offsetPage+($i*24*60*60);?>
																		
																		@if(!is_null($baseprice))
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->basestock))
																							<?php $value = 0 ?>
																						@else
																							<?php $value = $baseitems->default_stock ?>
																						@endif
																					<?php endforeach?>

																					<?php foreach($binservicestok[$data->idBinService] as $discitems => $itemsvalue):?>

																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							<?php $value =  $itemsvalue->stock; break;?>
																					
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																		@endif
																		
																		<td class="tg-yw4l">
																			{{$value}}
																		</td>
																	@endfor
																</tr>
  													@endforeach
												</table>
											</div>
										</div>
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
