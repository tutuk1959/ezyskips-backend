$(document).ready(function(){
	$('iframe#paynow').contents().find("head").append(
			$("<style type='text/css'>"+
			"body {"+
				"background-color: #efefef;"+
			"} "+
			" </style>")
	);
	$('.collapse [name="selectall"]').change(function(){
		var prop = $(this).prop('checked');
		
		if(prop == true){
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', true);
			})
		} else {
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
		}
	});
	
	$('.collapse [name="unselectall"]').change(function(){
		var prop = $(this).prop('checked');
		
		if(prop == true){
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
		} 
	});

	var start = moment().startOf('month');
	var end =  moment().startOf('month').add(14, 'days');

	function cb(start, end) {
        $('input[name="order_management_datepicker"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        $('input[name="start_order_date"]').val(start.format('YYYY-MM-DD'));
        $('input[name="end_order_date"]').val(end.format('YYYY-MM-DD'));
	}

	$('input[name="order_management_datepicker"]').daterangepicker({
		startDate: start,
        endDate: end,
		showDropdowns: true,
		linkedCalendar:true,
        ranges: {
			'First Fortnight': [moment().startOf('month'), moment().startOf('month').add(14, 'days')],
			'Second Fortnight': [moment().subtract(1,'months').startOf('month').add(15, 'days'), moment().subtract(1,'months').endOf('month')],
        },
		locale: {
			format: 'DD/MM/YYYY'
		}
	}, cb);
	cb(start, end);
	
	$('#per_transaction').hide();
	$('input[name="ordersummary_checkbox_all"]').on('change', function(){
		if($(this).is(':checked')){
			var checkboxes = $(document).find('input[name="ordersummary_checkbox[]"]');
			checkboxes.each(function(){
				$(this).prop('checked', true);
			})
			$('#per_transaction').show();
		} else {
			var checkboxes = $(document).find('input[name="ordersummary_checkbox[]"]');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
			$('#per_transaction').hide();
		}
		
	});
	
	
	$('input[name="ordersummary_checkbox[]"]').on('change', function(){
		if($(this).is(':checked')){
			$('#per_transaction').show();
		} else {
			$('#per_transaction').hide();
		}
		
	});
	
	$('#per_transaction').on('click', function(e){
		e.preventDefault();
		var uniquecode = [];
		var startDate = $('input[name="startDate"]').val();
		var endDate = $('input[name="endDate"]').val();
		var idsupplier = $('input[name="idsupplier"]').val();
		
		var checkboxes = $(document).find('input[name="ordersummary_checkbox[]"]');
		checkboxes.each(function(){
			if($(this).is(':checked')){
				uniquecode.push($(this).val());
			}
		});
		//alert(uniquecode);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: generate_pdf_checbox_url,
			data:{
				uniquecode:uniquecode,
				startDate: startDate, 
				endDate:endDate, 
				idsupplier:idsupplier
			},
			success: function(result){
				console.log(result);
			},
			error: function( data, status, error ) { 
				console.log(data);
				console.log(status);
				console.log(error);
			}
		})
		.done(function(result ) {
			var element = document.createElement('a');
			element.setAttribute('href', result);
			element.setAttribute('download', 'Ezyskips Online Report.pdf');
			element.style.display = 'none';
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		})
		.fail(function( result ) {
			console.log('Failed AJAX Call :' + result);
		});
	})
});