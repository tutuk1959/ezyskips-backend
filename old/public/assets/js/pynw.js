
	//alert(hash);
	var username = document.getElementById("username").value;
	var shared_secret = document.getElementById("shared_secret").value;
	var currency = document.getElementById("currency").value;
	var invoice = document.getElementById("payment").value;
	var total = document.getElementById("totalprice").value;
	//var d = new Date,
	//dformat = [d.getFullYear(),
	//	d.getMonth()+1,
	//	d.getDate()].join('') +
	//	[d.getHours(),
	//	d.getMinutes(),
	//	d.getSeconds()].join('');
	//alert(dformat);
	
	var hash = hex_hmac_md5(shared_secret, invoice+":"+total+":"+currency+":https://supplier.ezyskipsonline.com.au/payment_engagement");
	//alert("INV" + dformat + ' ' + hash);
	url = "https://paynow.pmnts.io/v2/"+username+"/"+ invoice+ "/"+currency+"/"+total+"/"+hash +'?return_path=https://supplier.ezyskipsonline.com.au/payment_engagement&iframe=true&show_email=false&postmessage=true';
	console.log(url);
	//window.location = url;
	document.getElementById('paynow').src = url;
	
	window.paymentHost = "https://paynow.pmnts.io";
	
	var messageListener = function(e) {
		if (e.origin !== window.paymentHost) {
			return;
		}
	
		// Older browsers will have a query-string style data payload
		// Whereas newer browsers will have an object
		var payload = event.data;
		if (typeof event.data == 'string') {
			if (/\[object/i.test(event.data)) {
				alert("Sorry, it looks like there has been a problem communicating with your browser..."); // Raised if the serialization failed
			}
			// Deserialize into an object
			var pairs = payload.split("&");
			payload = {};
			for (var i = 0; i < pairs.length; i++) {
				var element = pairs[i];
				var kv = element.split("=");
				payload[kv[0]] = kv[1];
			}
		}
		if ('data' in payload) {
			var payload = payload.data;
		}
		switch (payload.message) {
			case "transaction.complete":
			// Handle the transaction complete message.
			// Payload will be in e.data.data (eg. payload.data.r == 1)
			return true;
			case "transaction.processing":
			// Handle the processing of the transaction - implementation optional.
			return true;
			case "transaction.cancelled":
			// Handle the transaction being cancelled (i.e. show message, re-display the window etc).
			return true;
		}
	};
	
	if (window.addEventListener) {
		window.addEventListener("message", messageListener);
	} else {
		window.attachEvent("onmessage", messageListener);
	}
