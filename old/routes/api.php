<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('bintype','ApiController@showBinType');
Route::get('bintype/{idBinType}','ApiController@findBinType');
Route::get('sizes','ApiController@showSizes');
Route::get('sizes/{idSize}','ApiController@findSize');
Route::get('binhire/{idBinType}/{idBinSize}/{zipcode}/{deliverydate}/{collectiondate}','ApiController@showBinHire');
Route::get('getbestdeal','ApiController@getBestDeal');
Route::get('showstate/','ApiController@showState');
Route::get('showarea/{zipcode}','ApiController@showArea');
Route::get('showpostcode/{zipcode}','ApiController@showPostcode');
Route::get('findarea/{zipcode}', 'ApiController@findArea');
Route::get('binhireoptions/{idBinType}/{idSupplier}','ApiController@binhireOptions');
Route::get('payment/{idbinhire}/{zipcode}/{deliverydate}/{collectiondate}','ApiController@payment');