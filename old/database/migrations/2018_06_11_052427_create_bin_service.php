<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('tblbinservice', function (Blueprint $table) {
			$table->increments('idBinService');
			$table->integer('idSupplier');
			$table->integer('idBinType');
			$table->float('price');
			$table->float('discPrice');
			$table->integer('stock');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
