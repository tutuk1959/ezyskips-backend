<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use PDF;
use Illuminate\Support\Facades\Mail;

class orderSummary extends Controller
{

	public function ordersummary_view(){
		$supplierdata = $this->get_supplier_data();
		$suppliesdata = null;
		return view('ordersummary_view', [ 'supplierdata' => $supplierdata, 'suppliesdata' => $suppliesdata]);
	}

	private function get_supplier_data(){
		$supplierdata = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where(['tbluser.role' => 2])
			->orWhere(['tbluser.role' => 1])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->get();
		return $supplierdata;
	}

	private function get_supplier_data_by_id($idSupplier){
		$supplierdata = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tblsupplier.idSupplier' => $idSupplier 
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierdata;
	}

	private function get_order_data($idSupplier, $start, $end){
		$orders = DB::table('tblorderservice')
			->leftJoin('tblcustomer','tblorderservice.idConsumer', '=', 'tblcustomer.idCustomer')
			->leftJoin('tblsupplier', 'tblorderservice.idSupplier','=', 'tblsupplier.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbinservice','tblbinservice.idBinService','=','tblorderservice.idBinService')
			->leftJoin('tblbintype','tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize','tblbinservice.idBinSize','=','tblsize.idSize')
			->leftJoin('tblorderstatus', 'tblorderstatus.idOrder', '=', 'tblorderservice.idOrderService')
			->select('tblcustomer.name AS customerName','tblcustomer.company AS customerCompanyName', 'tblcustomer.email AS customerEmail','tblcustomer.phone AS customerPhone',
				'tblbintype.name AS bintypename','tblbintype.CodeType as bintypecode','tblbintype.description AS bintypedescription',
				'tblsize.size AS binsize', 'tblorderservice.idOrderService','tblorderservice.paymentUniqueCode',
				'tblorderservice.orderDate', 'tblorderservice.deliveryDate', 'tblorderservice.collectionDate','tblorderservice.deliveryAddress', 
				 'tblorderservice.totalServiceCharge', 'tblorderservice.subtotal', 'tblorderservice.gst','tblorderservice.bookingfee',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService'
				)
			->where([
				'tblsupplier.idSupplier' => $idSupplier, 
			])
			->whereBetween('orderDate',[$start,$end])
			->where(function($query){
        		$query->where( 'tblorderstatus.status', '=', 1 )
            	->orWhere( 'tblorderstatus.status', '=', 2)
            	->orWhere( 'tblorderstatus.status', '=', 3);
    		})
			->groupBy('customerName','customerCompanyName','customerEmail','customerPhone','bintypename','bintypecode','bintypedescription',
				'binsize','tblorderservice.idOrderService','tblorderservice.paymentUniqueCode','tblorderservice.orderDate', 'tblorderservice.deliveryDate', 
				'tblorderservice.collectionDate', 'tblorderservice.deliveryAddress',
				 'tblorderservice.totalServiceCharge', 'tblorderservice.subtotal', 'tblorderservice.gst','tblorderservice.bookingfee',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService'
				)
			->get();
		return $orders;
	}

	private function sum_the_price($idSupplier, $start, $end){
		$sum = DB::table('tblorderservice')
			->leftJoin('tblcustomer','tblorderservice.idConsumer', '=', 'tblcustomer.idCustomer')
			->leftJoin('tblsupplier', 'tblorderservice.idSupplier','=', 'tblsupplier.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbinservice','tblbinservice.idBinService','=','tblorderservice.idBinService')
			->leftJoin('tblbintype','tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize','tblbinservice.idBinSize','=','tblsize.idSize')
			->leftJoin('tblorderstatus', 'tblorderstatus.idOrder', '=', 'tblorderservice.idOrderService')
			->where([
				'tblsupplier.idSupplier' => $idSupplier, 
			])
			->whereBetween('orderDate',[$start,$end])
			->where(function($query){
        		$query->where( 'tblorderstatus.status', '=', 1 )
            	->orWhere( 'tblorderstatus.status', '=', 2)
            	->orWhere( 'tblorderstatus.status', '=', 3);
    		})
			->sum('tblorderservice.totalServiceCharge');
		return $sum;
	}

	public function fetch_supplier_order(Request $request){
		$validate = Validator::make($request->all(), [
            'selectsupplier' => 'required|integer',
        ], [
            'selectsupplier.integer' => 'Choose supplier first!'
        ]);

         if($validate->fails()){
         	$supplierdata = $this->get_supplier_data();
            return Redirect::route('supplier_supplies', ['supplierdata' =>$supplierdata])->withErrors($validate)->withInput();
        }

        $idSupplier = $request['selectsupplier'];
        $start = $request['start_order_date'];
        $end = $request['end_order_date'];

        $actualStartDate = date('d-m-Y', strtotime($start));
        $actualEndDate = date('d-m-Y', strtotime($end));

        $supplierdata = $this->get_supplier_data();
        $selected_supplierdata = $this->get_supplier_data_by_id($idSupplier);
        $suppliesdata = $this->get_order_data($idSupplier, $start, $end);
        $sum = $this->sum_the_price($idSupplier, $start, $end);
		$bookingprice = $this->get_bookingprice();
       	return view('ordersummary_view', [ 'supplierdata' => $supplierdata,
       	'selected_supplierdata' => $selected_supplierdata , 'suppliesdata' => $suppliesdata, 'sum' => $sum,
       	'actualStartDate' => $actualStartDate, 'actualEndDate' => $actualEndDate, 'bookingprice' => $bookingprice 
       	 ]);
	}

	public function pdfexporter(Request $request){
		$idSupplier = $request['idSupplier'];
		$startDate = $request['startDate'];
		$endDate = $request['endDate'];

		$start = date('Y-m-d', strtotime($startDate));
		$end = date('Y-m-d', strtotime($endDate));

		$selected_supplierdata = $this->get_supplier_data_by_id($idSupplier);
		$suppliesdata = $this->get_order_data($idSupplier, $start, $end);
        $sum = $this->sum_the_price($idSupplier, $start, $end);
		$bookingprice = $this->get_bookingprice();
		$data =  ['selected_supplierdata' => $selected_supplierdata, 'suppliesdata' => $suppliesdata, 'sum'=> $sum, 
				'startDate' => $startDate, 'endDate' => $endDate, 'bookingprice' => $bookingprice];
		$pdf = PDF::loadView('pdf.ordersummary_template', $data);

		$filename = 'Order summary '.$startDate.' - '.$endDate.'.pdf';
		return $pdf->download($filename);
	}
	
	private function get_bookingprice(){
		$bookingprice = DB::table('tblbookingprice')
					->select('price')
					->first();
		return $bookingprice;
	}
}
