<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Carbon\Carbon;
use App\tblcustomer;
use App\tblorderservice;
use Illuminate\Support\Facades\Mail;

class scnpayment extends Controller
{
	private $username;
	private $apiurl;
	private $token;
	private $shared_secret;
	private $currency;
	//public function __construct(){
	//	//set the stuff up
	//	$this->username = 'SC-scnet';
	//	$this->token = 'a5e39fa4dd713e37cb45726954e176d4';
	//	$this->shared_secret = 'f5a12739';
	//	$this->currency = 'AUD';
	//}
	public function paymentStatus(Request $request){
		return view('paymentstatus');
    }
	
	public function addPayment(Request $request){
		$idpaymenttemp = $request['idpaymenttemp'];
		$paid = $this->check_paid_status($idpaymenttemp);
		if (is_null($paid)){
			\Session::put('error','No purchase traced');
			return Redirect::route('payment_status');
		}
		
		if ($paid->paid == 1){
			\Session::put('error','Payment failed. This purchase has already closed');
			return Redirect::route('payment_status');
		}
		$purchasedetails = $this->purchasedetails_id($idpaymenttemp);
		if (!is_null($purchasedetails)){
			$deliveryYear = date('Y', strtotime($purchasedetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($purchasedetails->deliveryDate));
			$deliveryDay = date('d', strtotime($purchasedetails->deliveryDate));
			
			$collectionYear = date('Y', strtotime($purchasedetails->collectionDate));
			$collectionMonth = date('m', strtotime($purchasedetails->collectionDate));
			$collectionDay = date('d', strtotime($purchasedetails->collectionDate));
			
			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$daysmargin = $startdate->diffInDays($finishdate, false);
			
			$binhire = $this->binhire_id($purchasedetails->idBinService);
			
			$binhireoptions = $this->binhire_options($purchasedetails->idBinType, $purchasedetails->idSupplier);
			
			$bookingprice = $this->get_bookingprice();
			
			if ($daysmargin > $binhireoptions->extraHireageDays){
				$exactplusdays = $daysmargin - $binhireoptions->extraHireageDays;
				$totalprice = $binhire->price + ($exactplusdays * $binhireoptions->extraHireagePrice);
			} else {
				$exactplusdays = 0;
				$totalprice = $binhire->price;
			}
			$subtotal_bookingfee = $totalprice + $bookingprice->price;
			$gst = $subtotal_bookingfee / 110  * 10;
			$grandtotal = $subtotal_bookingfee;
			$servicearea = $this->serviceareas($purchasedetails->zipcode);
			return view('paywithpaypal',['purchasedetails' => $purchasedetails , 'binhire' => $binhire, 'binhireoptions' => $binhireoptions, 
			'daysmargin' => $daysmargin, 'idpaymenttemp' => $idpaymenttemp, 'servicearea' => $servicearea,
			'bookingprice' => $bookingprice ,'totalprice' => $totalprice,'subtotal_bookingfee' => $subtotal_bookingfee, 'gst' => $gst, 'grandtotal' => $grandtotal,
			'exactplusdays' => $exactplusdays]);
		} else {
            return view('paywithpaypal',['purchasedetails' => $purchasedetails, 'idpaymenttemp' => $idpaymenttemp]);
        } 
	}
	
	public function post_details(Request $request){
		$idpaymenttemp = $request['idpaymenttemp'];
		//Session::put('idpaymenttemp',$idpaymenttemp);
		$totalprice = $request['totalprice'];
		//Session::put('totalprice', $totalprice);
		$gst = $request['gst'];
		$subtotal = $request['subtotal'];
		$bookingfee = $request['bookingfee'];
		$validate = Validator::make($request->all(), [
			'first_name' => 'required|string|max:100',
			'last_name' => 'required|string|max:100',
			'address' => 'required|string',
			'suburb' => 'required|string|max:100',
			'phone' => 'required|string|max:12',
			'email' => 'required|email',
			'agree' => 'required',
			'postal_code' => 'required|same:zipcode'
		], [
			'first_name.required' => 'First name is required',
			'last_name.required' => 'Last name is required',
			'address.required' => 'Delivery Address is required',
			'suburb.required' => 'Suburb is required',
			'phone.required' => 'Phone is required',
			'email.required' => 'Email is required',
			'agree.required' => 'Tick agree to proceed',
			'postal_code.same' => 'Typed address mismatch from selected postal code'
		]);
		if($validate->fails()){
			return Redirect::route('addPayment', ['idpaymenttemp' =>$request['idpaymenttemp']])->withErrors($validate)->withInput();
		}
		
		$order = $this->purchase_id($idpaymenttemp);
		$binservice = $this->binhire_details_id($order->idBinHire);
		$paymentUniqueCode = $this->generateUniqueID();
		$insertpaymentform = DB::table('tblpaymentformtemp')->insertGetId(
		[
			'idPaymentTemp' => $idpaymenttemp,
			'paymentUniqueCode' => $paymentUniqueCode,
			'first_name' => $request['first_name'],
			'last_name' => $request['last_name'],
			'address' => $request['address'],
			'suburb' => $request['suburb'],
			'zipcode' => $request['zipcode'],
			'phone' => $request['phone'],
			'email' => $request['email'],
			'special_note' => $request['note'],
			'totalprice' => $totalprice,
			'gst' => $gst,
			'subtotal' => $subtotal,
			'bookingfee' => $bookingfee
		]);
		Session::put('idpaymentform',$insertpaymentform);
		return Redirect::route('host', ['data' => $insertpaymentform, 'order' => $order->idBinHire]);
	}
	
	public function scn_iframe(Request $request){
		$data = DB::table('tblpaymentformtemp')
				->select('idPaymentForm', 'idPaymentTemp', 'paymentUniqueCode', 'totalprice', 'gst',
				'subtotal', 'bookingfee','first_name', 'last_name', 'address',
				'suburb', 'zipcode', 'phone', 'email', 'special_note')
				->where(['idPaymentForm' => $request['data']])
				->first();
		
		$purchasedetails = $this->purchasedetails_id($data->idPaymentTemp);
		if (!is_null($purchasedetails)){
			$deliveryYear = date('Y', strtotime($purchasedetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($purchasedetails->deliveryDate));
			$deliveryDay = date('d', strtotime($purchasedetails->deliveryDate));
			
			$collectionYear = date('Y', strtotime($purchasedetails->collectionDate));
			$collectionMonth = date('m', strtotime($purchasedetails->collectionDate));
			$collectionDay = date('d', strtotime($purchasedetails->collectionDate));
			
			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$daysmargin = $startdate->diffInDays($finishdate, false);
			
			$binhire = $this->binhire_id($purchasedetails->idBinService);
			
			$binservice = $this->binhire_details_id($purchasedetails->idBinService);
			
			$binhireoptions = $this->binhire_options($purchasedetails->idBinType, $purchasedetails->idSupplier);
			
			$bookingprice = $this->get_bookingprice();
			
			$paymenttemp = $data->idPaymentTemp;
			$payment = $data->paymentUniqueCode;
			$order = $request['order'];
			$insert_payment = $request['data'];
		
			if ($daysmargin > $binhireoptions->extraHireageDays){
				$exactplusdays = $daysmargin - $binhireoptions->extraHireageDays;
				$subtotal = $binhire->price + ($exactplusdays * $binhireoptions->extraHireagePrice);
			} else {
				$exactplusdays = 0;
				$subtotal = $binhire->price;
			}
			$subtotal_bookingfee = $subtotal + $bookingprice->price;
			$gst = $subtotal_bookingfee / 110  * 10;
			$totalprice = $data->totalprice;
			
			
			return view('template_iframe', ['paymenttemp' => $paymenttemp, 'payment' => $payment, 
			'order' => $order, 'insert_payment' => $insert_payment, 
			'totalprice' => $totalprice,'purchasedetails' => $purchasedetails, 
			'startdate' => $startdate, 'finishdate' => $finishdate,
			'daysmargin' => $daysmargin, 'binhire' => $binhire, 
			'binhireoptions' => $binhireoptions, 'bookingprice' => $bookingprice, 
			'exactplusdays' => $exactplusdays, 'subtotal' => $subtotal, 'gst' => $gst,
			'data' => $data, 'binservice' => $binservice]);
		} else {
			\Session::put('error','Payment error.');
			return Redirect::route('payment_status');
		}
	}
	
	public function scn_iframe_response(Request $request){
		if(!is_null($request['r'])){
			$r = $request['r'];
			$amount = $request['amount'];
			$currency = $request['currency'];
			$id = $request['id'];
			$token = $request['token'];
			$v = $request['v'];
			$successful = $request['successful'];
			$reference = $request['reference'];
			$card_category = $request['card_category'];
			$card_type = $request['card_type'];
			$card_holder = $request['card_holder'];
			$card_number = $request['card_number'];
			switch ($r){
				case 1:
					Session::flash('success', 'Payment received');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful, 'v' => $v, 'reference' => $reference, 'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 2:
					Session::flash('error', 'Payment Declined - examine the message parameter for possible explanations');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful, 'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 94:
					Session::flash('error', 'Cancelled - the merchant clicked the Cancel button on the payment or checkout form');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful, 'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 95:
					Session::flash('error', 'Merchant Not Found - possible incorrect username');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful, 'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 96:
					Session::flash('error', 'Duplicate invoice number. Reference ID is not unique');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful,
					'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 97:
					Session::flash('error', 'Validation error - check the errors[] parameters for error messages');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful,
					'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 99:
					Session::flash('error', 'Invalid Verification - the verification value does not match the parameters supplied');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful,
					'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
				case 999:
					Session::flash('error', 'Gateway error - an unknown error has occurred and the merchant should investigate with Cloud Payments Support');
					return view('template_iframe_status',['r' => $r, 'amount' => $amount, 'currency' => $currency, 'id' => $id, 'token' => $token, 'successful' => $successful,
					'v' => $v,'reference' => $reference,'card_category' => $card_category, 'card_type' => $card_type,'card_holder' => $card_holder, 'card_number' => $card_number]);
			}
			
		} else {
			Session::flash('error', 'Payment failed. Something wrong on the communication.');
			return view('template_iframe_status');
		}
	}
	
	public function ajax_hash_post(Request $request){
		$data = $request['data'];
		$v = $request['v'];
		$reference = $request['reference'];
		$card_category = $request['card_category'];
		$card_type = $request['card_type'];
		$card_holder = $request['card_holder'];
		$card_number = $request['card_number'];
		$initial = $this->getidpaymenttemp($reference);
		
		if (is_null($initial)){
			\Session::flash('error','Payment failed. This purchase has already expired');
			return response()->json('0');
		}
		
		$paid = $this->check_paid_status($initial->idPaymentTemp);
		
		if (is_null($paid)){
			\Session::flash('error','No purchase traced');
			return response()->json('0');
		}
		
		if ($paid->paid == 1){
			\Session::flash('error','Payment failed. This purchase has already closed');
			return response()->json('0');
		}
		
		if ($data != $v){
			return response()->json('0');
		}
		$proceeed = $this->after_successful_payment($reference, $card_category, $card_type, $card_holder, $card_number);
		return response()->json('1');
	}
	/************************************************************************************/
	//tocheck whether if its paid or not
	private function check_paid_status($idpaymenttemp){
		$paid = DB::table('tblpaymenttemp')
			->select('paid')
			->where(['idPaymentTemp' => $idpaymenttemp])
			->first();
		return $paid;
	}
	//fetch purchase details by id
	private function purchasedetails_id($idpaymenttemp){
		$purchasedetails = DB::table('tblpaymenttemp')
			->leftJoin('tblbinservice','tblpaymenttemp.idBinHire','=','tblbinservice.idBinService')
			->select('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
				'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
			->where([
					'tblpaymenttemp.idPaymentTemp' => $idpaymenttemp
				])
			->orderBy('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
				'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
			->first();
		return $purchasedetails;
	}
	
	private function purchase_id($idpaymenttemp){
		$purchasedetails = DB::table('tblpaymenttemp')
                ->select('idpaymenttemp', 'idBinHire', 'zipcode', 'deliveryDate', 'collectionDate')
                ->where(['idpaymenttemp' => $idpaymenttemp])
                ->first();
		return $purchasedetails;
	}
	
	//fetch supplier service details by id
	private function binhire_id($idBinService){
		$binhire = DB::table('tblbinservice')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->where(['tblbinservice.idBinService' => $idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier')
			->orderBy('tblbinservice.price','asc')
			->first();
		return $binhire;
	}
	
	//fetch supplier service details by id
	private function binhire_details_id($idBinService){
		$binhire = DB::table('tblbinservice')
			->leftJoin('tblbintype', 'tblbinservice.idBinType', '=', 'tblbintype.idBinType')
			->leftJoin('tblsize', 'tblbinservice.idBinSize', '=', 'tblsize.idSize')
			->select('tblbinservice.idBinService', 'tblbinservice.idSupplier', 'tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price', 'tblbinservice.stock'
				, 'tblbintype.name', 'tblbintype.description', 'tblsize.size')
			->where([
				'idBinService' => $idBinService
				])
			->orderBy('tblbinservice.idBinService', 'tblbinservice.idSupplier', 'tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price', 'tblbinservice.stock'
				, 'tblbintype.name', 'tblbintype.description', 'tblsize.size')
			->first();

		return $binhire;
	}
	
	//fetch additional options for supplier service details by id
	private function binhire_options($idBinType, $idSupplier){
		$binhireoptions = DB::table('tblbinserviceoptions')
			->leftJoin('tblbinservice', function ($query){
				$query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
					->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
			})
			->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
			->where([
				'tblbinserviceoptions.idBinType' => $idBinType,
				'tblbinserviceoptions.idSupplier' =>$idSupplier
				])
			->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
			->first();
		return $binhireoptions;
	}
	
	private function get_bookingprice(){
		$bookingprice = DB::table('tblbookingprice')
					->select('price')
					->first();
		return $bookingprice;
	}
	
	private function serviceareas($zipcode){
		$servicearea = DB::table('tblservicearea')
			->select('area')
			->where(['zipcode' => $zipcode])
			->first();
		
		return $servicearea;
	}
	
	public function generateUniqueID(){
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pin = mt_rand(10000, 99999)
			. mt_rand(10000, 99999)
			. $characters[rand(0, strlen($characters) - 1)];
		$rand = str_shuffle($pin);
		return $rand;
    }
	
	private function getidpaymenttemp($reference){
		$initial = DB::table('tblpaymentformtemp')
					->select('idPaymentTemp', 'idPaymentForm', 'totalprice')
					->where(['paymentUniqueCode' => $reference])
					->first();
		return $initial;
	}
	public function after_successful_payment($reference, $card_category, $card_type, $card_holder, $card_number){
		$initial = DB::table('tblpaymentformtemp')
					->select('idPaymentTemp', 'idPaymentForm', 'totalprice', 'gst', 'subtotal','bookingfee')
					->where(['paymentUniqueCode' => $reference])
					->first();
		if (!is_null($initial->idPaymentTemp)){
			$purchasedetails = DB::table('tblpaymenttemp')
					->leftJoin('tblbinservice','tblpaymenttemp.idBinHire','=','tblbinservice.idBinService')
					->select('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
						'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
					->where([
							'tblpaymenttemp.idPaymentTemp' => $initial->idPaymentTemp
						])
					->orderBy('tblbinservice.idBinService','tblbinservice.idSupplier','tblbinservice.idBinType', 'tblbinservice.idBinSize', 'tblbinservice.price',
						'tblpaymenttemp.deliveryDate', 'tblpaymenttemp.collectionDate', 'tblpaymenttemp.zipcode')
					->first();
	
			$paymentform = DB::table('tblpaymentformtemp')
							->select('idpaymenttemp', 'first_name', 'last_name', 'address',
							'suburb', 'zipcode', 'phone', 'email', 'special_note')
							->where(['idPaymentForm' => $initial->idPaymentForm])
							->first();

			if (!is_null($purchasedetails)){
				if (!is_null($paymentform)){
					//insert order
					$idcustomer = DB::table('tblcustomer')->insertGetId([
						'name' => $paymentform->first_name.' '.$paymentform->last_name,
						'address' => $paymentform->address, 
						'suburb' => $paymentform->suburb,
						'zipcode' => $paymentform->zipcode,
						'phone' => $paymentform->phone,
						'email' => $paymentform->email
					]);
					if (!is_null($idcustomer)){
						$datenow = date('Y-m-d', strtotime('now'));
						$gmtTimezone = new \DateTimeZone('Australia/Perth');
						$orderDate = new \DateTime($datenow, $gmtTimezone);
						
						$insertOrder = DB::table('tblorderservice')->insertGetId([
							'idBinService' => $purchasedetails->idBinService,
							'idConsumer' => $idcustomer,
							'paymentUniqueCode' => $reference,
							'deliveryDate' => $purchasedetails->deliveryDate,
							'collectionDate' => $purchasedetails->collectionDate,
							'deliveryAddress' => $paymentform->address,
							'deliveryComments' => $paymentform->special_note,
							'idSupplier' => $purchasedetails->idSupplier,
							'orderDate' => $orderDate->format('Y-m-d'),
							'totalServiceCharge' => $initial->totalprice,
							'gst' => $initial->gst,
							'subtotal' => $initial->subtotal,
							'bookingfee' => $initial->bookingfee,
							'card_category' => $card_category, 
							'card_type' => $card_type,
							'card_holder' => $card_holder,
							'card_number' => $card_number,
							]);

						if(!is_null($insertOrder)){
							//update paid status
							$updatepaid = DB::table('tblpaymenttemp')
										->where(['idPaymentTemp' => $initial->idPaymentTemp])
										->update(['paid' => 1]);


							$invoiceDetails = DB::table('tblorderservice')
											->select('paymentUniqueCode', 'totalServiceCharge', 'gst', 'subtotal', 'bookingfee',
											'deliveryDate', 'collectionDate', 
											'deliveryAddress', 'deliveryComments', 'orderDate', 'card_category','card_type','card_number','card_holder')
											->where(['idOrderService' => $insertOrder])
											->first();

							//insertorderstatushere
							$currenttimestamps = date('Y-m-d H:i:s',strtotime('now'));
							$insertOrderStatus = DB::table('tblorderstatus')->insertGetId([
								'idOrder' => $insertOrder,
								'status' => 1,
								'created_at' => $currenttimestamps,
								'updated_at' => $currenttimestamps,
								'operator' => 11,
							]);
								
							//update skip bin stock here
							$stock = DB::table('tblbinservice')
										->select('stock')
										->where(['idBinService' => $purchasedetails->idBinService])
										->first();
										
							$updatedvalue = $stock->stock - 1;
							$updatestock = DB::table('tblbinservice')
										->where(['idBinService' => $purchasedetails->idBinService])
										->update(['stock' => $updatedvalue]);
										
							$customerdetails = DB::table('tblcustomer')
										->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
										->where(['idCustomer' => $idcustomer])
										->first();

							$binhire = DB::table('tblbinservice')
								->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
								->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
								->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
								->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
								->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
								->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
								->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
								->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
									'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
								->where(['tblbinservice.idBinService' => $purchasedetails->idBinService])
								->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
									'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
								->orderBy('tblbinservice.price','asc')
							->first();

							$supplierdetails = DB::table('tblsupplier')
								->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2', 
									'customerServiceContact', 'customerServicePhone', 'customerServiceMobile','abn')
								->where(['idSupplier' => $binhire->idSupplier])
								->first();

							$binhireoptions = DB::table('tblbinserviceoptions')
								->leftJoin('tblbinservice', function ($query){
									$query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
									->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
								})
								->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
								->where([
									'tblbinserviceoptions.idBinType' => $purchasedetails->idBinType,
									'tblbinserviceoptions.idSupplier' =>$purchasedetails->idSupplier
								])
								->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
								->first();
							
							$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
							$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
							$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));
							
							$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
							$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
							$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));
							
							$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
							$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
							$datemargin = $startdate->diffInDays($finishdate, false);
			
							if ($datemargin > $binhireoptions->extraHireageDays){
								$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
							} else {
								$daysmargin = 0;
							}
			
							$bookingprice = $this->get_bookingprice();
							$tomail = $customerdetails->email;
							$tosender = $customerdetails->name;
							$subject = "Ezyskips Online Payment Confirmation";
							$invoice_url = $this->invoiceimporter($insertOrder);
							$orderslip_url = $this->orderslipimporter($insertOrder);
							
							
							Mail::send('mails.mail_invoice', ['invoiceDetails' => $invoiceDetails, 'customerdetails' => $customerdetails, 
							'supplierdetails' => $supplierdetails, 'binhire' => $binhire, 'binhireoptions' => $binhireoptions,
							'bookingprice' => $bookingprice,'daysmargin' => $daysmargin],
							function($mail) use ($tomail, $tosender, $subject, $invoice_url){
								$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
								$mail->to($tomail,  $tosender);
								$mail->subject($subject);
								$mail->attach(public_path($invoice_url),[
									'mime' => 'application/pdf',
								]);
							});
							
							$suppliertomail = $supplierdetails->email;
							$suppliertosender = $supplierdetails->name;
							$suppliersubject = "Ezyskips Online Bin Hire Order";
							Mail::send('mails.mail_receipt', ['invoiceDetails' => $invoiceDetails, 'customerdetails' => $customerdetails, 'binhire' => $binhire, 'binhireoptions' => $binhireoptions,'bookingprice' => $bookingprice, 'daysmargin' => $daysmargin],
							function($mail) use ($suppliertomail, $suppliertosender, $suppliersubject,$orderslip_url){
								$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
								$mail->to($suppliertomail,  $suppliertosender);
								$mail->subject($suppliersubject);
								$mail->attach(public_path($orderslip_url),[
									'mime' => 'application/pdf',
								]);
							});
							
							if ($supplierdetails->email2 != null || $supplierdetails->email2 != ''){
								$suppliertomail_copy = $supplierdetails->email2;
								$suppliertosender_copy = $supplierdetails->name;
								$suppliersubject_copy = "Ezyskips Online Bin Hire Order";
								Mail::send('mails.mail_receipt', ['invoiceDetails' => $invoiceDetails, 'customerdetails' => $customerdetails, 'binhire' => $binhire, 'binhireoptions' => $binhireoptions,'bookingprice' => $bookingprice, 'daysmargin' => $daysmargin],
								function($mail) use ($suppliertomail_copy, $suppliertosender_copy, $suppliersubject_copy,$orderslip_url){
									$mail->from("admin@ezyskipsonline.com.au", "Ezyskips Online");
									$mail->to($suppliertomail_copy,  $suppliertosender_copy);
									$mail->subject($suppliersubject_copy);
									$mail->attach(public_path($orderslip_url),[
										'mime' => 'application/pdf',
									]);
								});
							}
						}
					}       
				}
			}
		}
    }
	
	private function invoiceimporter($idorderservice){
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode', 
						'totalServiceCharge','gst', 'subtotal', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress',
						'deliveryComments', 'orderDate')
        				->where(['idOrderService' => $idorderservice])
        				->first();
        if (is_null($invoiceDetails)){
        	return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
        } else {
        	$binhire = DB::table('tblbinservice')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $invoiceDetails->idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

            $customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $invoiceDetails->idConsumer])
        		->first();

        	$supplierdetails = DB::table('tblsupplier')
            					->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2', 
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile','abn')
            					->where(['idSupplier' => $invoiceDetails->idSupplier])
            					->first();

            $binhireoptions = DB::table('tblbinserviceoptions')
                            ->leftJoin('tblbinservice', function ($query){
                                $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                	->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                })
                            ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                            ->where([
								'tblbinserviceoptions.idBinType' => $binhire->idBinType,
								'tblbinserviceoptions.idSupplier' =>$invoiceDetails->idSupplier
                            ])
							->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
							->first();
			$bookingprice = $this->get_bookingprice();
			
			$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
			$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));
			
			$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
			$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
			$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));
			
			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$datemargin = $startdate->diffInDays($finishdate, false);
			
			if ($datemargin > $binhireoptions->extraHireageDays){
					$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
			} else {
				$daysmargin = 0;
			}
							
			$data =  ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails, 
					'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'bookingprice' => $bookingprice, 'daysmargin' => $daysmargin];
			$pdf = PDF::loadView('pdf.template', $data);
			
			$filename = 'Invoice '.$invoiceDetails->paymentUniqueCode.'.pdf';
			$url = 'invoices/pdf/pdf_1.pdf';
			$dest = 'invoices/pdf/sent/'.$filename;
			copy($url, $dest);
			$pdf->save($dest);
			return $dest;
        }
	}
	
	
	private function orderslipimporter($idorderservice){
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode', 
						'totalServiceCharge','gst', 'subtotal', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress',
						'deliveryComments', 'orderDate')
        				->where(['idOrderService' => $idorderservice])
        				->first();
        if (is_null($invoiceDetails)){
        	return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
        } else {
        	$binhire = DB::table('tblbinservice')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $invoiceDetails->idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType', 
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

            $customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $invoiceDetails->idConsumer])
        		->first();

        	$supplierdetails = DB::table('tblsupplier')
            					->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2', 
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile')
            					->where(['idSupplier' => $invoiceDetails->idSupplier])
            					->first();

            $binhireoptions = DB::table('tblbinserviceoptions')
                            ->leftJoin('tblbinservice', function ($query){
                                $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                	->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                })
                            ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                            ->where([
								'tblbinserviceoptions.idBinType' => $binhire->idBinType,
								'tblbinserviceoptions.idSupplier' =>$invoiceDetails->idSupplier
                            ])
							->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
							->first();
			$bookingprice = $this->get_bookingprice();
			
			$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
			$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));
			
			$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
			$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
			$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));
			
			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$datemargin = $startdate->diffInDays($finishdate, false);
			
			if ($datemargin > $binhireoptions->extraHireageDays){
					$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
			} else {
				$daysmargin = 0;
			}
							
			$data =  ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails, 
					'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'bookingprice' => $bookingprice, 'daysmargin' => $daysmargin];
			$pdf = PDF::loadView('pdf.orderslip', $data);
			
			$filename = 'Order Slip '.$invoiceDetails->paymentUniqueCode.'.pdf';
			$url = 'invoices/pdf/pdf_1.pdf';
			$dest = 'invoices/pdf/sent/'.$filename;
			copy($url, $dest);
			$pdf->save($dest);
			return $dest;
		}
	}
	
	public function away(){
        return redirect()->away('https://ezyskipsonline.com.au/');
    }
}	
