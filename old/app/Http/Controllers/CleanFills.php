<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use Illuminate\Support\Facades\DB;

class CleanFills extends Controller
{
	public function index($result = null){
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$offset = time();

		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);


		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('clean_fills',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset,'serviceOptions' => $serviceOptions, 'nonDelivery' => $nonDelivery, 
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice]);
	}

	public function showResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('clean_fills',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice]);
	}

	public function showForm(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));

		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('clean_fills_form',['supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice]);
	}

	public function showFormResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}

		return view('clean_fills_form',['supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice]);
	}

	private function getBinSize (){
		$sizes = DB::table('tblsize')
				->select('idSize','size', 'dimensions')
				->orderBy('idSize', 'asc')
				->get();
		return $sizes;
	}

	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}

	private function getServiceOptions($idUser, $idSupplier){
		$serviceOptions = DB::table('tblbinserviceoptions')
						->select('idUser', 'idSupplier', 'idBinType', 'extraHireagePrice', 'extraHireageDays', 'excessWeightPrice')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier,
							'idBinType' => 3
						])
						->first();
		return $serviceOptions;
	}

	private function getNonDeliveryDays($idUser, $idSupplier){
		$nonDelivery = DB::table('tblbinnondelivery')
					->select('date')
					->where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3
					])
					->get();
		return $nonDelivery;
	}

	private function getbinservicebaseprice($idSupplier){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinType' => 3,
							])
						->get();
		return $binserviceprice;
	}

	private function getbinservicediscprice($idBinService){
		$binserviceprice = DB::table('tblbinserviceupdates')
						->select('tblbinserviceupdates.price','tblbinserviceupdates.stock','tblbinserviceupdates.date')
						->where([
							'tblbinserviceupdates.idBinService' => $idBinService,
							])
						->get();
		return $binserviceprice;
	}

	private function getspecificbaseprice($idSupplier, $idBinSize){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinSize' => $idBinSize,
							'tblbinservice.idBinType' => 3,
							])
						->get();
		return $binserviceprice;
	}

	private function validateForm($extraHireage, $extraHireageDays){
		$error = array();
		if ($extraHireage == ''){
			$error[] = 'Extra hireage must not empty';
		} 
		if ($extraHireageDays == ''){
			$error[] = 'Extra hireage days must not empty';
		} 
		return $error;
	}

	private function validatePricing($basePrice, $baseStock){
		$error = array();
		if ($basePrice == ''){
			$error[] = 'Please fill a specific base price for your bin';
		} 
		if ($baseStock == ''){
			$error[] = 'Please fill a specific stock for your bin';
		} 
		return $error;
	}

	public function editMiscDetail(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$extraHireage = $request['extraHireage'];
		$extraHireageDays = $request['extraHireageDays'];
		$excessWeight = 0; 

		if ($request->isMethod('post') == 'POST'){
			$error = $this->validateForm($extraHireage, $extraHireageDays);
			if(count($error) > 0){
				$result['status'] = 'danger';
				$result['message'] = $error;
				return redirect()->route('clean.fills.schedule', ['status' => $result['status'], 'message' => $result['message']]);
			} else {
				$check = DB::table('tblbinserviceoptions')
						->select('idUser','idSupplier', 'idBinType')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier, 
							'idBinType' => 3
						])
						->first();
				if (!is_null($check)){
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions::where(['idUser' => $idUser, 'idSupplier' => $idSupplier, 'idBinType' => $idBinType])
					->update([
						'extraHireagePrice' => $extraHireage, 
						'extraHireageDays' => $extraHireageDays, 
						'excessWeightPrice' => $excessWeight
					]);
					if ($serviceOptions){
						$result['status'] = 'success';
						$result['message'] = 'Success updating rates optional data';
						return redirect()->route('clean.fills.schedule', ['status' => $result['status'], 'message' => $result['message']]);
					} else {
						$result['status'] = 'danger';
						$result['message'] = 'Update user data rates optiona failed';
						return redirect()->route('clean.fills.schedule', ['status' => $result['status'], 'message' => $result['message']]);
					}
				} else {
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions->idUser = $idUser;
					$serviceOptions->idSupplier = $idSupplier;
					$serviceOptions->idBinType = 3;
					$serviceOptions->extraHireagePrice = $extraHireage;
					$serviceOptions->extraHireageDays = $extraHireageDays;
					$serviceOptions->excessWeightPrice = $excessWeight;
					if ($serviceOptions->save()){
						$result['status'] = 'success';
						$result['message'] = 'Success updating rates optional data';
						return redirect()->route('clean.fills.schedule', ['status' => $result['status'], 'message' => $result['message']]);
					} else {
						$result['status'] = 'notif';
						$result['message'] = 'Update user data rates optiona failed';
						return redirect()->route('clean.fills.schedule', ['status' => $result['status'], 'message' => $result['message']]);
					}
				}
			}
		}
	}

	public function editNonDeliveryDays(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$date[] = $request['date'];
		$nonDelivery[] = $request['nondelivery'];

		$nondeliverydays = array('date' => $date, 'nondelivery' => $nonDelivery);
		/** Delete first **/
		$tblbinnondelivery = new tblbinnondelivery();
		//check for deletion first
		if (is_null($nondeliverydays['nondelivery'][0])){
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}

					if($deletedRows){
						$result['status'] = 'success';
						$result['message'] = 'Success updating non delivery data';
					} else {
						$result['status'] = 'notif';
						$result['message'] = 'Update non delivery days failed';
					}
				}
			}
		} else{
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
				}
			}
			
			/** Then insert **/
			foreach($nondeliverydays['nondelivery'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 3,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 3;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
				}
			}
			
		}

		return redirect()->route('general.waste', ['status' => $result['status'], 'message' => $result['message']]);
	}

	public function editBinServicePrice(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$idBinSize = $request['idBinSize'];
		$basePrice = $request['basePrice'];
		$date[] = $request['date'];
		$binPrice[] = $request['binPrice'];
		$baseStock = $request['baseStock'];
		$stock[] = $request['stock'];

		/* Main pricing data */
		$mainData = array(
			'date' => $date,
			'binPrice' => $binPrice,
			'stock' => $stock
		);
		

		if ($request->isMethod('POST') == 'POST'){
			
			$error = $this->validatePricing($binPrice, $stock);
			if(count($error) > 0){
				$result['status'] = 'danger';
				$result['message'] = $error;
				return redirect()->route('clean.fills.pricing.form', ['status' => $result['status'], 'message' => $result['message']]);
			} else {
				/* check base price first */
				
				$checkBasePrice = DB::table('tblbinservice')
							->select('idBinService', 'price', 'stock')
							->where([
								'idSupplier' => $idSupplier,
								'idBinSize' => $idBinSize,
								'idBinType' => $idBinType
							])
							->first();

				if (!is_null($checkBasePrice)){
					$binservice = new tblbinservice();

					$updates = $binservice::where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType,
					])
					->update(['price' => $basePrice, 'stock' => $baseStock]);

					$checkupdatesSuccessful = DB::table('tblbinservice')
											->select('idBinService', 'price', 'stock')
											->where([
												'idSupplier' => $idSupplier,
												'idBinSize' => $idBinSize,
												'idBinType' => $idBinType,
												'price' => $basePrice,
												'stock' => $baseStock
											])
											->first();
					if(!is_null($checkupdatesSuccessful)){
						$tempResultBasePrice = 'success';
					} else {
						$tempResultBasePrice = 'danger';
					}

					/*fetch last id */
					$lastid = DB::table('tblbinservice')
					->select('idBinService', 'price', 'stock')
					->where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType
					])
					->first();

				} else {
					
					$binservice = new tblbinservice();
					$binservice->idSupplier = $idSupplier;
					$binservice->idBinType = $idBinType;
					$binservice->idBinSize = $idBinSize;
					$binservice->price = $basePrice;
					$binservice->stock = $baseStock;

					if ($binservice->save()){
						$tempResultBasePrice = 'success';
						/*fetch last id */
						$lastid = DB::table('tblbinservice')
							->select('idBinService', 'price', 'stock')
							->orderBy('idBinService', 'desc')
							->first();
					} else {
						$tempResultBasePrice = 'danger';
					}
				}

				/* check and update the discount on specific date*/
				if (!is_null($mainData['binPrice'])){
					foreach($mainData['binPrice'] as $k => $v){
						foreach($v as $r => $d){
							if(!is_null($mainData['binPrice'][$k][$r])){
								$checkDisc = DB::table('tblbinserviceupdates')
									->select('idBinService', 'price', 'stock')
									->where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
								->first();

								if (!is_null($checkDisc)){
									$binserviceupdates = new tblbinserviceupdates();
									$update = $binserviceupdates::where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
									->update(['price' => $mainData['binPrice'][$k][$r], 'stock' => $mainData['stock'][$k][$r]]);

									if ($update) {
										$tempResultDiscPrice = 'success';
									
									} else {
										$tempResultDiscPrice = 'danger';
									}
								} else {
									if(!is_null($checkBasePrice)){
										if(($checkBasePrice->price != $mainData['binPrice'][$k][$r]) || ($checkBasePrice->stock != $mainData['stock'][$k][$r])){
											$binserviceupdates = new tblbinserviceupdates();
											$binserviceupdates->idBinService = $lastid->idBinService;
											$binserviceupdates->price = $mainData['binPrice'][$k][$r];
											$binserviceupdates->stock = $mainData['stock'][$k][$r];
											$binserviceupdates->date = $mainData['date'][$k][$r];
											if ($binserviceupdates->save()){
												$tempResultDiscPrice = 'success';
											} else {
												$tempResultDiscPrice = 'danger';
											}
										} else {
											$tempResultDiscPrice = 'success';
										}
									}
								}
							}
						}
					}
				}
				

				if ($tempResultBasePrice == 'success'){
					$result['status'] = 'success';
					$result['message'] = 'Success updating rates data';
				} else {
					$result['status'] = 'notif';
					$result['message'] = 'Update rates data failed';
					
				}
				return redirect()->route('clean.fills.schedule', ['status' => $result['status'], 'message' => $result['message'], 'binsize' => $idBinSize, 'bintype' => $idBinType]);
			}
		}
	}

	public function resetRates(Request $request){
		$idUser = session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$idBinType = $request['bintype'];
		$idBinSize = $request['binsize'];

		$binservice = new tblbinservice();
		$binserviceupdates = new tblbinserviceupdates();

		$getbinservice = DB::table('tblbinservice')
						->select('idBinService')
						->where([
							'idSupplier' => $supplierData->idSupplier,
							'idBinSize' => $idBinSize,
							'idBinType' => $idBinType
						])
						->first();
		if (!is_null($getbinservice)){
			$deletedRowsBinUpdates = $binserviceupdates::where([
			'idBinService' => $getbinservice->idBinService
			])->delete();

			$deletedRowsBin = $binservice::where([
				'idSupplier' => $supplierData->idSupplier,
				'idBinSize' => $idBinSize,
				'idBinType' => $idBinType
			])->delete();

			if ($deletedRowsBin){
				$deletedRowsBin = 'success';
			} else {
				$deletedRowsBin = 'danger';
			}

			if ($deletedRowsBin == 'success'){
				$result['status'] = 'success';
				$result['message'] = 'Success deleting rates data';
			} else {
				$error = 'Delete rates data failed';
				$result['status'] = 'notif';
				$result['message'] = $error;
			}
		} else {
			$error = 'No data entry found';
			$result['status'] = 'notif';
			$result['message'] = $error;
		}
		

		return redirect()->route('clean.fills.waste', ['status' => $result['status'], 'message' => $result['message']]);
	}
}
