<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblsupplier extends Model
{
	protected $table = 'tblsupplier';
	public $timestamps = false;
}
