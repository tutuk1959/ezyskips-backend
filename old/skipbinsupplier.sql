-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2018 at 01:14 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skipbinsupplier`
--

-- --------------------------------------------------------

--
-- Table structure for table `csv_data`
--

CREATE TABLE `csv_data` (
  `id` int(11) NOT NULL,
  `csv_filename` varchar(255) DEFAULT NULL,
  `csv_header` tinyint(1) DEFAULT NULL,
  `csv_data` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `csv_data`
--

INSERT INTO `csv_data` (`id`, `csv_filename`, `csv_header`, `csv_data`, `created_at`, `updated_at`) VALUES
(1, 'district.csv', 1, '[{\"zipcode\":\"0000\",\"area\":\"Western Australia\",\"parentareacode\":0},{\"zipcode\":\"0001\",\"area\":\"City of Armadale\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0002\",\"area\":\"Town of Bassendean\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0003\",\"area\":\"City of Bayswater\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0004\",\"area\":\"City of Belmont\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0005\",\"area\":\"Town of Cambridge\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0006\",\"area\":\"City of Canning\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0007\",\"area\":\"Town of Claremont\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0008\",\"area\":\"City of Cockburn\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0009\",\"area\":\"Town of Cottesloe\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0010\",\"area\":\"Town of East Fremantle\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0011\",\"area\":\"City of Fremantle\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0012\",\"area\":\"City of Gosnells\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0013\",\"area\":\"City of Joondalup\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0014\",\"area\":\"City of Kalamunda\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0015\",\"area\":\"City of Kwinana\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0016\",\"area\":\"City of Melville\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0017\",\"area\":\"Town of Mosman Park\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0018\",\"area\":\"Shire of Mundaring\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0019\",\"area\":\"City of Nedlands\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0020\",\"area\":\"Shire of Peppermint Grove\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0021\",\"area\":\"City of Perth\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0022\",\"area\":\"City of Rockingham\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0023\",\"area\":\"Shire of Serpentine-Jarrahdale\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0024\",\"area\":\"City of South Perth\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0025\",\"area\":\"City of Stirling\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0026\",\"area\":\"City of Subiaco\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0027\",\"area\":\"City of Swan\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0028\",\"area\":\"Town of Victoria Park\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0029\",\"area\":\"City of Vincent\",\"parentareacode\":\"0000\"},{\"zipcode\":\"0030\",\"area\":\"City of Wanneroo\",\"parentareacode\":\"0000\"}]', '2018-09-24 18:58:48', '2018-09-24 18:58:48'),
(2, 'City A to B.csv', 1, '[{\"zipcode\":6111,\"area\":\"Ashendon, Canning Mills, Champion Lakes, Karragullen, Kelmscott, Lesley, Roleystone, Westfield\",\"parentareacode\":\"0001\"},{\"zipcode\":6112,\"area\":\"Armadale, Bedfordale, Brookdale, Forrestdale, Mount Nasura, Mount Richon, Seville Grove, Wungong\",\"parentareacode\":\"0001\"},{\"zipcode\":6992,\"area\":\"Armadale\",\"parentareacode\":\"0001\"},{\"zipcode\":6991,\"area\":\"Kelmscott\",\"parentareacode\":\"0001\"},{\"zipcode\":6997,\"area\":\"Kelmscott\",\"parentareacode\":\"0001\"},{\"zipcode\":6051,\"area\":\"Maylands\",\"parentareacode\":\"0003\"},{\"zipcode\":6053,\"area\":\"Bayswater\",\"parentareacode\":\"0003\"},{\"zipcode\":6062,\"area\":\"Embleton, Morley, Noranda\",\"parentareacode\":\"0003\"},{\"zipcode\":6931,\"area\":\"Maylands\",\"parentareacode\":\"0003\"},{\"zipcode\":6933,\"area\":\"Bayswater\",\"parentareacode\":\"0003\"},{\"zipcode\":6103,\"area\":\"Rivervale\",\"parentareacode\":\"0004\"},{\"zipcode\":6104,\"area\":\"Ascot, Belmont, Redcliffe\",\"parentareacode\":\"0004\"},{\"zipcode\":6105,\"area\":\"Cloverdale, Kewdale, Perth Airport\",\"parentareacode\":\"0004\"},{\"zipcode\":6985,\"area\":\"Cloverdale\",\"parentareacode\":\"0004\"},{\"zipcode\":6984,\"area\":\"Belmont\",\"parentareacode\":\"0004\"},{\"zipcode\":6230,\"area\":\"Bunbury, Carey Park, College Grove, Dalyellup, Davenport, East Bunbury, Gelorup, Glen Iris, Pelican Point, South Bunbury, Usher, Vittoria, Withers\",\"parentareacode\":\"0031\"},{\"zipcode\":6231,\"area\":\"Bunbury\",\"parentareacode\":\"0031\"}]', '2018-09-24 19:16:18', '2018-09-24 19:16:18'),
(3, 'City canning to kwinana.csv', 1, '[{\"zipcode\":6102,\"area\":\"Bentley, St James\",\"parentareacode\":\"0006\"},{\"zipcode\":6106,\"area\":\"Welshpool\",\"parentareacode\":\"0006\"},{\"zipcode\":6107,\"area\":\"Beckenham, Cannington, Kenwick, Queens Park, Wattle Grove, Wilson\",\"parentareacode\":\"0006\"},{\"zipcode\":6147,\"area\":\"Langford, Lynwood, Parkwood\",\"parentareacode\":\"0006\"},{\"zipcode\":6148,\"area\":\"Ferndale, Riverton, Rossmoyne, Shelley\",\"parentareacode\":\"0006\"},{\"zipcode\":6149,\"area\":\"Bull Creek, Leeming\",\"parentareacode\":\"0006\"},{\"zipcode\":6155,\"area\":\"Canning Vale, Willetton\",\"parentareacode\":\"0006\"},{\"zipcode\":6970,\"area\":\"Canning Vale\",\"parentareacode\":\"0006\"},{\"zipcode\":6982,\"area\":\"Bentley\",\"parentareacode\":\"0006\"},{\"zipcode\":6983,\"area\":\"Bentley\",\"parentareacode\":\"0006\"},{\"zipcode\":6986,\"area\":\"Welshpool\",\"parentareacode\":\"0006\"},{\"zipcode\":6955,\"area\":\"Willetton\",\"parentareacode\":\"0006\"},{\"zipcode\":6987,\"area\":\"Cannington\",\"parentareacode\":\"0006\"},{\"zipcode\":6163,\"area\":\"Bibra Lake, Coolbellup, Hamilton Hill, Hilton, Kardinya, North Coogee, North Lake, O Connor, Samson, Spearwood\",\"parentareacode\":\"0008\"},{\"zipcode\":6164,\"area\":\"Atwell, Aubin Grove, Banjup, Beeliar, Cockburn Central, Hammond Park, Jandakot, South Lake, Success, Yangebup\",\"parentareacode\":\"0008\"},{\"zipcode\":6166,\"area\":\"Coogee, Henderson, Munster, Wattleup\",\"parentareacode\":\"0008\"},{\"zipcode\":6964,\"area\":\"South Lake\",\"parentareacode\":\"0008\"},{\"zipcode\":6965,\"area\":\"Bibra Lake\",\"parentareacode\":\"0008\"},{\"zipcode\":6963,\"area\":\"Halmilton Hill\",\"parentareacode\":\"0008\"},{\"zipcode\":6159,\"area\":\"North Fremantle\",\"parentareacode\":\"0011\"},{\"zipcode\":6160,\"area\":\"Fremantle\",\"parentareacode\":\"0011\"},{\"zipcode\":6162,\"area\":\"Beaconsfield, South Fremantle, White Gum Valley\",\"parentareacode\":\"0011\"},{\"zipcode\":6959,\"area\":\"Fremantle\",\"parentareacode\":\"0011\"},{\"zipcode\":6108,\"area\":\"Thornlie\",\"parentareacode\":\"0012\"},{\"zipcode\":6109,\"area\":\"Maddington, Orange Grove\",\"parentareacode\":\"0012\"},{\"zipcode\":6110,\"area\":\"Gosnells, Huntingdale, Martin, Southern River\",\"parentareacode\":\"0012\"},{\"zipcode\":6988,\"area\":\"Thornlie\",\"parentareacode\":\"0012\"},{\"zipcode\":6989,\"area\":\"Maddington\",\"parentareacode\":\"0012\"},{\"zipcode\":6990,\"area\":\"Gosnells\",\"parentareacode\":\"0012\"},{\"zipcode\":6919,\"area\":\"Joondalup\",\"parentareacode\":\"0013\"},{\"zipcode\":6023,\"area\":\"Duncraig\",\"parentareacode\":\"0013\"},{\"zipcode\":6024,\"area\":\"Greenwood, Warwick\",\"parentareacode\":\"0013\"},{\"zipcode\":6924,\"area\":\"Greenwood\",\"parentareacode\":\"0013\"},{\"zipcode\":6025,\"area\":\"Craigie, Hillarys, Kallaroo, Padbury\",\"parentareacode\":\"0013\"},{\"zipcode\":6027,\"area\":\"Beldon, Connolly, Edgewater, Heathridge, Joondalup, Mullaloo, Ocean Reef\",\"parentareacode\":\"0013\"},{\"zipcode\":6028,\"area\":\"Burns Beach, Currambine, Iluka, Kinross\",\"parentareacode\":\"0013\"},{\"zipcode\":6923,\"area\":\"Hillarys\",\"parentareacode\":\"0013\"},{\"zipcode\":6405,\"area\":\"Greenwoods Valley, Meckering, Quelagetting, Warding East\",\"parentareacode\":\"0013\"},{\"zipcode\":6020,\"area\":\"Carine, Marmion, North Beach, Sorrento, Watermans Bay\",\"parentareacode\":\"0013\"},{\"zipcode\":6057,\"area\":\"High Wycombe, Maida Vale\",\"parentareacode\":\"0014\"},{\"zipcode\":6076,\"area\":\"Bickley, Carmel, Gooseberry Hill, Hacketts Gully, Kalamunda, Lesmurdie, Paulls Valley, Pickering Brook, Piesse Brook, Reservoir, Walliston\",\"parentareacode\":\"0014\"},{\"zipcode\":6925,\"area\":\"Walliston\",\"parentareacode\":\"0014\"},{\"zipcode\":6926,\"area\":\"Kalamunda\",\"parentareacode\":\"0014\"},{\"zipcode\":6058,\"area\":\"Forrestfield\",\"parentareacode\":\"0014\"},{\"zipcode\":6165,\"area\":\"Hope Valley, Naval Base\",\"parentareacode\":\"0015\"},{\"zipcode\":6167,\"area\":\"Anketell, Bertram, Calista, Casuarina, Kwinana Beach, Kwinana Town Centre, Mandogalup, Medina, Orelia, Parmelia, Postans, The Spectacles, Wandi\",\"parentareacode\":\"0015\"},{\"zipcode\":6170,\"area\":\"Leda, Wellard\",\"parentareacode\":\"0015\"},{\"zipcode\":6966,\"area\":\"Kwinana\",\"parentareacode\":\"0015\"},{\"zipcode\":6714,\"area\":\"Antonymyre, Balla Balla, Baynton, Bulgarra, Burrup, Cleaverville, Cooya Pooya, Gap Ridge, Gnoorea, Karratha, Karratha Industrial Estate, Maitland, Mardie, Millars Well, Mount Anketell, Mulataga, Nickol, Pegs Creek, Sherlock, Stove Hill\",\"parentareacode\":\"0015\"}]', '2018-09-24 21:25:13', '2018-09-24 21:25:13'),
(4, 'City melville to swan (guilford).csv', 1, '[{\"zipcode\":6180,\"area\":\"Lakelands, Parklands\",\"parentareacode\":\"0032\"},{\"zipcode\":6181,\"area\":\"Stake Hill\",\"parentareacode\":\"0032\"},{\"zipcode\":6207,\"area\":\"Myara, Nambeelup, North Dandalup, Solus, Whittaker\",\"parentareacode\":\"0032\"},{\"zipcode\":6208,\"area\":\"Blythewood, Fairbridge, Meelon, Nirimba, North Yunderup, Oakley, Pinjarra, Point Grey, Ravenswood, South Yunderup, West Pinjarra\",\"parentareacode\":\"0032\"},{\"zipcode\":6209,\"area\":\"Barragup, Furnissdale\",\"parentareacode\":\"0032\"},{\"zipcode\":6210,\"area\":\"Coodanup, Dudley Park, Erskine, Falcon, Greenfields, Halls Head, Madora Bay, Mandurah, Meadow Springs, San Remo, Silver Sands, Wannanup\",\"parentareacode\":\"0032\"},{\"zipcode\":6211,\"area\":\"Bouvard, Clifton, Dawesville, Herron\",\"parentareacode\":\"0032\"},{\"zipcode\":6213,\"area\":\"Banksiadale, Dwellingup, Etmilyn, Holyoake, Inglehope, Marrinup, Teesdale\",\"parentareacode\":\"0032\"},{\"zipcode\":6214,\"area\":\"Birchmont, Coolup\",\"parentareacode\":\"0032\"},{\"zipcode\":6150,\"area\":\"Bateman, Murdoch, Winthrop\",\"parentareacode\":\"0016\"},{\"zipcode\":6153,\"area\":\"Applecross, Ardross, Brentwood, Mount Pleasant\",\"parentareacode\":\"0016\"},{\"zipcode\":6154,\"area\":\"Alfred Cove, Booragoon, Myaree\",\"parentareacode\":\"0016\"},{\"zipcode\":6156,\"area\":\"Attadale, Melville, Willagee\",\"parentareacode\":\"0016\"},{\"zipcode\":6157,\"area\":\"Bicton, Palmyra\",\"parentareacode\":\"0016\"},{\"zipcode\":6953,\"area\":\"Applecross\",\"parentareacode\":\"0016\"},{\"zipcode\":6954,\"area\":\"Booragoon\",\"parentareacode\":\"0016\"},{\"zipcode\":6956,\"area\":\"Melville\",\"parentareacode\":\"0016\"},{\"zipcode\":6957,\"area\":\"Palmyra\",\"parentareacode\":\"0016\"},{\"zipcode\":6961,\"area\":\"Palmyra\",\"parentareacode\":\"0016\"},{\"zipcode\":6960,\"area\":\"Myaree\",\"parentareacode\":\"0016\"},{\"zipcode\":6008,\"area\":\"Daglish, Shenton Park, Subiaco\",\"parentareacode\":\"0019\"},{\"zipcode\":6907,\"area\":\"Nedlands\",\"parentareacode\":\"0019\"},{\"zipcode\":6909,\"area\":\"Nedlands\",\"parentareacode\":\"0019\"},{\"zipcode\":6911,\"area\":\"Nedlands\",\"parentareacode\":\"0019\"},{\"zipcode\":6000,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6003,\"area\":\"Highgate, Northbridge\",\"parentareacode\":\"0021\"},{\"zipcode\":6004,\"area\":\"East Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6005,\"area\":\"Kings Park, West Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6009,\"area\":\"Crawley, Dalkeith, Nedlands\",\"parentareacode\":\"0021\"},{\"zipcode\":6831,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6832,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6837,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6838,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6839,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6840,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6841,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6842,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6843,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6844,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6845,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6846,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6847,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6848,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6849,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6850,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6865,\"area\":\"Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6892,\"area\":\"East Perth\",\"parentareacode\":\"0021\"},{\"zipcode\":6168,\"area\":\"Cooloongup, East Rockingham, Garden Island, Hillman, Peron, Rockingham\",\"parentareacode\":\"0022\"},{\"zipcode\":6169,\"area\":\"Safety Bay, Shoalwater, Waikiki, Warnbro\",\"parentareacode\":\"0022\"},{\"zipcode\":6172,\"area\":\"Baldivis\",\"parentareacode\":\"0022\"},{\"zipcode\":6172,\"area\":\"Port Kennedy\",\"parentareacode\":\"0022\"},{\"zipcode\":6173,\"area\":\"Secret Harbour\",\"parentareacode\":\"0022\"},{\"zipcode\":6174,\"area\":\"Golden Bay\",\"parentareacode\":\"0022\"},{\"zipcode\":6175,\"area\":\"Singleton\",\"parentareacode\":\"0022\"},{\"zipcode\":6176,\"area\":\"Karnup\",\"parentareacode\":\"0022\"},{\"zipcode\":6958,\"area\":\"Rockingham\",\"parentareacode\":\"0022\"},{\"zipcode\":6967,\"area\":\"Rockingham\",\"parentareacode\":\"0022\"},{\"zipcode\":6968,\"area\":\"Rockingham\",\"parentareacode\":\"0022\"},{\"zipcode\":6969,\"area\":\"Rockingham\",\"parentareacode\":\"0022\"},{\"zipcode\":6152,\"area\":\"Como, Karawara, Manning, Salter Point, Waterford\",\"parentareacode\":\"0024\"},{\"zipcode\":6951,\"area\":\"South Perth\",\"parentareacode\":\"0024\"},{\"zipcode\":6952,\"area\":\"Como\",\"parentareacode\":\"0024\"},{\"zipcode\":6941,\"area\":\"Mirrabooka\",\"parentareacode\":\"0025\"},{\"zipcode\":6022,\"area\":\"Hamersley\",\"parentareacode\":\"0025\"},{\"zipcode\":6716,\"area\":\"Fortescue, Hamersley Range, Millstream, Pannawonica\",\"parentareacode\":\"0025\"},{\"zipcode\":6016,\"area\":\"Glendalough, Mount Hawthorn\",\"parentareacode\":\"0025\"},{\"zipcode\":6017,\"area\":\"Herdsman, Osborne Park\",\"parentareacode\":\"0025\"},{\"zipcode\":6018,\"area\":\"Churchlands, Doubleview, Gwelup, Innaloo, Karrinyup, Woodlands\",\"parentareacode\":\"0025\"},{\"zipcode\":6019,\"area\":\"Scarborough, Wembley Downs\",\"parentareacode\":\"0025\"},{\"zipcode\":6021,\"area\":\"Balcatta, Stirling\",\"parentareacode\":\"0025\"},{\"zipcode\":6029,\"area\":\"Trigg\",\"parentareacode\":\"0025\"},{\"zipcode\":6052,\"area\":\"Bedford, Inglewood\",\"parentareacode\":\"0025\"},{\"zipcode\":6060,\"area\":\"Joondanna, Tuart Hill, Yokine\",\"parentareacode\":\"0025\"},{\"zipcode\":6913,\"area\":\"Wembley\",\"parentareacode\":\"0025\"},{\"zipcode\":6914,\"area\":\"Balcatta\",\"parentareacode\":\"0025\"},{\"zipcode\":6916,\"area\":\"Osborne Park\",\"parentareacode\":\"0025\"},{\"zipcode\":6917,\"area\":\"Osborne Park\",\"parentareacode\":\"0025\"},{\"zipcode\":6918,\"area\":\"Innaloo City\",\"parentareacode\":\"0025\"},{\"zipcode\":6920,\"area\":\"North Beach\",\"parentareacode\":\"0025\"},{\"zipcode\":6921,\"area\":\"Karrinyup\",\"parentareacode\":\"0025\"},{\"zipcode\":6922,\"area\":\"Scarborough\",\"parentareacode\":\"0025\"},{\"zipcode\":6929,\"area\":\"Mount Lawley\",\"parentareacode\":\"0025\"},{\"zipcode\":6939,\"area\":\"Tuart Hill\",\"parentareacode\":\"0025\"},{\"zipcode\":6059,\"area\":\"Dianella\",\"parentareacode\":\"0025\"},{\"zipcode\":6061,\"area\":\"Balga, Mirrabooka, Nollamara, Westminster\",\"parentareacode\":\"0025\"},{\"zipcode\":6904,\"area\":\"Subiaco\",\"parentareacode\":\"0026\"},{\"zipcode\":6055,\"area\":\"Caversham, Guildford, Hazelmere, Henley Brook, South Guildford, West Swan\",\"parentareacode\":\"0027\"},{\"zipcode\":6056,\"area\":\"Baskerville, Bellevue, Boya, Greenmount, Helena Valley, Herne Hill, Jane Brook, Koongamia, Middle Swan, Midland, Midvale, Millendon, Red Hill, Stratton, Swan View, Viveash, Woodbridge\",\"parentareacode\":\"0027\"},{\"zipcode\":6063,\"area\":\"Beechboro\",\"parentareacode\":\"0027\"},{\"zipcode\":6067,\"area\":\"Cullacabardee\",\"parentareacode\":\"0027\"},{\"zipcode\":6068,\"area\":\"Whiteman\",\"parentareacode\":\"0027\"},{\"zipcode\":6069,\"area\":\"Aveley, Belhus, Brigadoon, Ellenbrook, The Vines, Upper Swan\",\"parentareacode\":\"0027\"},{\"zipcode\":6083,\"area\":\"Gidgegannup, Morangup\",\"parentareacode\":\"0027\"},{\"zipcode\":6084,\"area\":\"Bullsbrook, Chittering, Lower Chittering\",\"parentareacode\":\"0027\"},{\"zipcode\":6066,\"area\":\"Ballajura\",\"parentareacode\":\"0027\"},{\"zipcode\":6090,\"area\":\"Malaga\",\"parentareacode\":\"0027\"},{\"zipcode\":6935,\"area\":\"Guilford\",\"parentareacode\":\"0027\"}]', '2018-09-24 22:08:29', '2018-09-24 22:08:29'),
(5, 'City end.csv', 1, '[{\"zipcode\":6936,\"area\":\"Midland\",\"parentareacode\":\"0027\"},{\"zipcode\":6944,\"area\":\"Malaga\",\"parentareacode\":\"0027\"},{\"zipcode\":6945,\"area\":\"Malaga\",\"parentareacode\":\"0027\"},{\"zipcode\":6872,\"area\":\"West Perth\",\"parentareacode\":\"0029\"},{\"zipcode\":6006,\"area\":\"North Perth\",\"parentareacode\":\"0029\"},{\"zipcode\":6050,\"area\":\"Coolbinia, Menora, Mount Lawley\",\"parentareacode\":\"0029\"},{\"zipcode\":6902,\"area\":\"Leederville\",\"parentareacode\":\"0029\"},{\"zipcode\":6903,\"area\":\"Leederville\",\"parentareacode\":\"0029\"},{\"zipcode\":6915,\"area\":\"Mount Hawthorn\",\"parentareacode\":\"0029\"},{\"zipcode\":6906,\"area\":\"North Perth\",\"parentareacode\":\"0029\"},{\"zipcode\":6026,\"area\":\"Kingsley, Woodvale\",\"parentareacode\":\"0030\"},{\"zipcode\":6065,\"area\":\"Ashby, Darch, Hocking, Landsdale, Lexia, Madeley, Melaleuca, Pearsall, Sinagra, Tapping, Wangara, Wanneroo\",\"parentareacode\":\"0030\"},{\"zipcode\":6077,\"area\":\"Gnangara, Jandabup\",\"parentareacode\":\"0030\"},{\"zipcode\":6078,\"area\":\"Mariginiup, Pinjar\",\"parentareacode\":\"0030\"},{\"zipcode\":6946,\"area\":\"Wanneroo\",\"parentareacode\":\"0030\"},{\"zipcode\":6947,\"area\":\"Wangara\",\"parentareacode\":\"0030\"},{\"zipcode\":6030,\"area\":\"Clarkson, Merriwa, Mindarie, Quinns Rocks, Ridgewood, Tamala Park\",\"parentareacode\":\"0030\"},{\"zipcode\":6031,\"area\":\"Banksia Grove, Carramar, Neerabup\",\"parentareacode\":\"0030\"},{\"zipcode\":6032,\"area\":\"Nowergup\",\"parentareacode\":\"0030\"},{\"zipcode\":6033,\"area\":\"Carabooda\",\"parentareacode\":\"0030\"},{\"zipcode\":6034,\"area\":\"Eglinton\",\"parentareacode\":\"0030\"},{\"zipcode\":6035,\"area\":\"Yanchep\",\"parentareacode\":\"0030\"},{\"zipcode\":6036,\"area\":\"Butler, Jindalee\",\"parentareacode\":\"0030\"},{\"zipcode\":6037,\"area\":\"Two Rocks\",\"parentareacode\":\"0030\"},{\"zipcode\":6038,\"area\":\"Alkimos\",\"parentareacode\":\"0030\"},{\"zipcode\":6064,\"area\":\"Alexander Heights, Girrawheen, Koondoola, Marangaroo\",\"parentareacode\":\"0030\"},{\"zipcode\":6161,\"area\":\"Rottnest Island\",\"parentareacode\":\"0033\"},{\"zipcode\":6232,\"area\":\"Eaton, Millbridge\",\"parentareacode\":\"0034\"},{\"zipcode\":6041,\"area\":\"Caraban, Gabbadah, Guilderton, Wilbinga, Woodridge\",\"parentareacode\":\"0035\"},{\"zipcode\":6042,\"area\":\"Seabird\",\"parentareacode\":\"0035\"},{\"zipcode\":6043,\"area\":\"Breton Bay, Ledge Point\",\"parentareacode\":\"0035\"},{\"zipcode\":6044,\"area\":\"Karakin, Lancelin, Nilgen, Wedge Island\",\"parentareacode\":\"0035\"},{\"zipcode\":6233,\"area\":\"Australind, Binningup, Leschenault, Parkfield, Wellesley\",\"parentareacode\":\"0036\"},{\"zipcode\":6556,\"area\":\"Beechina, Chidlow, Gorrie, Malmalling, The Lakes\",\"parentareacode\":\"0037\"},{\"zipcode\":6558,\"area\":\"Wooroloo\",\"parentareacode\":\"0037\"},{\"zipcode\":6070,\"area\":\"Darlington\",\"parentareacode\":\"0037\"},{\"zipcode\":6071,\"area\":\"Glen Forrest, Hovea\",\"parentareacode\":\"0037\"},{\"zipcode\":6072,\"area\":\"Mahogany Creek\",\"parentareacode\":\"0037\"},{\"zipcode\":6073,\"area\":\"Mundaring\",\"parentareacode\":\"0037\"},{\"zipcode\":6074,\"area\":\"Sawyers Valley\",\"parentareacode\":\"0037\"},{\"zipcode\":6081,\"area\":\"Parkerville, Stoneville\",\"parentareacode\":\"0037\"},{\"zipcode\":6082,\"area\":\"Mount Helena\",\"parentareacode\":\"0037\"},{\"zipcode\":6271,\"area\":\"Capel, Capel River, Forrest Beach, Peppermint Grove Beach, Stirling Estate\",\"parentareacode\":\"0020\"},{\"zipcode\":6122,\"area\":\"Byford, Cardup, Darling Downs, Karrakup\",\"parentareacode\":\"0023\"},{\"zipcode\":6124,\"area\":\"Jarrahdale\",\"parentareacode\":\"0023\"},{\"zipcode\":6125,\"area\":\"Hopeland, Mardella, Serpentine\",\"parentareacode\":\"0023\"},{\"zipcode\":6126,\"area\":\"Keysbrook\",\"parentareacode\":\"0023\"},{\"zipcode\":6121,\"area\":\"Oakford, Oldbury\",\"parentareacode\":\"0023\"},{\"zipcode\":6123,\"area\":\"Mundijong, Whitby\",\"parentareacode\":\"0023\"},{\"zipcode\":6932,\"area\":\"Bassendean\",\"parentareacode\":\"0002\"},{\"zipcode\":6934,\"area\":\"Bassendean\",\"parentareacode\":\"0002\"},{\"zipcode\":6054,\"area\":\"Ashfield, Bassendean, Eden Hill, Kiara, Lockridge\",\"parentareacode\":\"0002\"},{\"zipcode\":6942,\"area\":\"Bassendean\",\"parentareacode\":\"0002\"},{\"zipcode\":6943,\"area\":\"Bassendean\",\"parentareacode\":\"0002\"},{\"zipcode\":6010,\"area\":\"Claremont, Karrakatta, Mount Claremont, Swanbourne\",\"parentareacode\":\"0005\"},{\"zipcode\":6007,\"area\":\"Leederville, West Leederville\",\"parentareacode\":\"0005\"},{\"zipcode\":6014,\"area\":\"Floreat, Jolimont, Wembley\",\"parentareacode\":\"0005\"},{\"zipcode\":6015,\"area\":\"City Beach\",\"parentareacode\":\"0005\"},{\"zipcode\":6901,\"area\":\"West Leederville\",\"parentareacode\":\"0005\"},{\"zipcode\":6910,\"area\":\"Claremont\",\"parentareacode\":\"0007\"},{\"zipcode\":6011,\"area\":\"Cottesloe, Peppermint Grove\",\"parentareacode\":\"0009\"},{\"zipcode\":6158,\"area\":\"East Fremantle\",\"parentareacode\":\"0010\"},{\"zipcode\":6012,\"area\":\"Mosman Park\",\"parentareacode\":\"0017\"},{\"zipcode\":6912,\"area\":\"Mosman Park\",\"parentareacode\":\"0017\"},{\"zipcode\":6979,\"area\":\"Victoria Park\",\"parentareacode\":\"0028\"},{\"zipcode\":6981,\"area\":\"Victoria Park East\",\"parentareacode\":\"0028\"},{\"zipcode\":6100,\"area\":\"Burswood, Lathlain, Victoria Park\",\"parentareacode\":\"0028\"},{\"zipcode\":6101,\"area\":\"Carlisle, East Victoria Park\",\"parentareacode\":\"0028\"},{\"zipcode\":6151,\"area\":\"Kensington, South Perth\",\"parentareacode\":\"0028\"}]', '2018-09-24 22:34:57', '2018-09-24 22:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_06_11_052427_create_bin_service', 1),
(3, '2018_06_11_053118_create_bin_service_updates', 1),
(4, '2018_06_11_053255_create_bin_type', 1),
(5, '2018_06_11_054938_create_supplier_table', 1),
(6, '2018_06_11_055545_create_service_area', 1),
(7, '2018_06_11_055941_create_size_table', 1),
(8, '2018_06_11_060117_create_order_service_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinnondelivery`
--

CREATE TABLE `tblbinnondelivery` (
  `idNonDeliveryDays` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `idSupplier` int(10) DEFAULT NULL,
  `idUser` int(10) DEFAULT NULL,
  `idBinType` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservice`
--

CREATE TABLE `tblbinservice` (
  `idBinService` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `idBinType` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `idBinSize` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinservice`
--

INSERT INTO `tblbinservice` (`idBinService`, `idSupplier`, `idBinType`, `price`, `stock`, `idBinSize`) VALUES
(1, 12, 1, 160.00, 1, 1),
(2, 11, 1, 130.00, 0, 1),
(3, 17, 1, 60.00, 0, 1),
(4, 17, 1, 70.00, 1, 2),
(5, 11, 2, 200.00, 6, 1),
(6, 19, 2, 200.00, 6, 1),
(7, 19, 2, 0.00, 0, 4),
(8, 19, 2, 300.00, 3, 3),
(9, 19, 2, 250.00, 6, 2),
(10, 20, 2, 195.00, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceoptions`
--

CREATE TABLE `tblbinserviceoptions` (
  `idBinServiceOptions` int(12) NOT NULL,
  `idUser` int(12) DEFAULT NULL,
  `idSupplier` int(12) DEFAULT NULL,
  `idBinType` int(12) DEFAULT NULL,
  `extraHireagePrice` bigint(20) DEFAULT NULL,
  `extraHireageDays` int(11) DEFAULT NULL,
  `excessWeightPrice` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbinserviceoptions`
--

INSERT INTO `tblbinserviceoptions` (`idBinServiceOptions`, `idUser`, `idSupplier`, `idBinType`, `extraHireagePrice`, `extraHireageDays`, `excessWeightPrice`) VALUES
(1, 11, 17, 1, 20, 8, 300),
(2, 5, 11, 1, 200, 2, 50),
(3, 5, 11, 2, 50, 7, 0),
(4, 13, 19, 2, 50, 7, 0),
(5, 14, 20, 2, 50, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceupdates`
--

CREATE TABLE `tblbinserviceupdates` (
  `idBinServiceUpdates` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinserviceupdates`
--

INSERT INTO `tblbinserviceupdates` (`idBinServiceUpdates`, `idBinService`, `price`, `stock`, `date`) VALUES
(1, 4, 75.00, 1, '2018-09-17');

-- --------------------------------------------------------

--
-- Table structure for table `tblbintype`
--

CREATE TABLE `tblbintype` (
  `idBinType` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodeType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description2` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbintype`
--

INSERT INTO `tblbintype` (`idBinType`, `name`, `CodeType`, `description`, `description2`) VALUES
(1, 'General Waste Schedule', '1210', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No cleanfill/hardfill</li>\r\n	<li>No soil</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Light domestic waste</li>\r\n	<li>Light construction waste</li>\r\n	<li>Office Waste</li>\r\n</ul>\r\n\r\n<strong>(NB: Mattresses, Carpet, E-Waste and Tyres may incur extra costs – check with your supplier after booking)</strong>'),
(2, 'Mixed Heavy Waste (suitable for domestic, commercial, demolition, construction and renovation) ', '1211', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No Sand, Soil, Clay or Dirt</li>\r\n	<li>No TV sets or computer monitors</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Household waste</li>\r\n	<li>Builders waste</li>\r\n	<li>Furniture & appliances</li>\r\n<li>Timber</li>\r\n<li>Bricks, tiles, concrete</li>\r\n<li>Green waste</li>\r\n<li>Metal/steel</li>\r\n</ul>\r\n\r\n<strong>(NB:Mattresses, Carpet, tyres and e-waste may incur extra costs - check with your supplier after booking)</strong>'),
(3, 'Clean Fills Waste', '1212', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No Sand, Soil, Clay or Dirt</li>\r\n	<li>No TV sets or computer monitors</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Household waste</li>\r\n	<li>Builders waste</li>\r\n	<li>Furniture & appliances</li>\r\n<li>Timber</li>\r\n<li>Bricks, tiles, concrete</li>\r\n<li>Green waste</li>\r\n<li>Metal/steel</li>\r\n</ul>\r\n\r\n<strong>(NB:Mattresses, Carpet, tyres and e-waste may incur extra costs - check with your supplier after booking)</strong>'),
(4, 'Green Waste', '1213', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No Sand, Soil, Clay or Dirt</li>\r\n	<li>No TV sets or computer monitors</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Household waste</li>\r\n	<li>Builders waste</li>\r\n	<li>Furniture & appliances</li>\r\n<li>Timber</li>\r\n<li>Bricks, tiles, concrete</li>\r\n<li>Green waste</li>\r\n<li>Metal/steel</li>\r\n</ul>\r\n\r\n<strong>(NB:Mattresses, Carpet, tyres and e-waste may incur extra costs - check with your supplier after booking)</strong>'),
(5, 'Dirt Waste', '1214', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No Sand, Soil, Clay or Dirt</li>\r\n	<li>No TV sets or computer monitors</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Household waste</li>\r\n	<li>Builders waste</li>\r\n	<li>Furniture & appliances</li>\r\n<li>Timber</li>\r\n<li>Bricks, tiles, concrete</li>\r\n<li>Green waste</li>\r\n<li>Metal/steel</li>\r\n</ul>\r\n\r\n<strong>(NB:Mattresses, Carpet, tyres and e-waste may incur extra costs - check with your supplier after booking)</strong>');

-- --------------------------------------------------------

--
-- Table structure for table `tblbookingprice`
--

CREATE TABLE `tblbookingprice` (
  `idBookingPrice` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbookingprice`
--

INSERT INTO `tblbookingprice` (`idBookingPrice`, `price`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 4, 5, '2018-10-31 03:12:11', '2018-10-30 16:12:11');

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `idCustomer` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `address` text,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcustomer`
--

INSERT INTO `tblcustomer` (`idCustomer`, `name`, `company`, `address`, `email`, `phone`, `suburb`, `zipcode`) VALUES
(1, 'John Doe', 'SOMS', '7/48 Prindiville Drive Wangara', 'info@baliwebdesignservice.id', '089658595049', 'Wangara', 6065),
(2, 'Putu Winata', NULL, 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'support@somsweb.com.au', '089658595043', 'Duncraig', 6023),
(3, 'Putu Winata', NULL, 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'support@somsweb.com.au', '089658595043', 'Duncraig', 6023),
(4, 'Putu Winata', NULL, 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'support@somsweb.com.au', '089658595043', 'Duncraig', 6023),
(5, 'Putu Winata', NULL, 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'support@somsweb.com.au', '089658595043', 'Duncraig', 6023),
(6, 'Putu Winata', NULL, 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'support@somsweb.com.au', '089658595043', 'Duncraig', 6023),
(7, 'Si Ganteng Sekali', NULL, 'Goldmead Street, Bayswater WA, Australia', 'support@somsweb.com.au', '089658595043', 'Bayswater', 6053),
(8, 'Putu Winata', NULL, 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'support@somsweb.com.au', '089658595043', 'Camillo', 6111),
(9, 'Putu Winata', NULL, 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'support@somsweb.com.au', '089658595043', 'Camillo', 6111),
(10, 'Putu Winata', NULL, 'Kelmscott Primary School, River Road, Kelmscott WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Kelmscott', 6111),
(11, 'Putu Winata', NULL, 'Kelmscott Station, Kelmscott WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Kelmscott', 6111),
(12, 'Putu Winata', NULL, 'Kelmscott Station, Kelmscott WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Kelmscott', 6111),
(13, 'Putu Winata', NULL, 'Kelmscott Plaza, Albany Highway, Kelmscott WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Kelmscott', 6111),
(14, 'Putu Winata', NULL, 'Joondalup Health Campus - Mr Colin Kikiros, Joondalup WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Joondalup', 6027),
(15, 'Putu Winata', NULL, 'Joondalup Health Campus - Mr Colin Kikiros, Joondalup WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Joondalup', 6027),
(16, 'Putu Winata', NULL, 'Joondalup Health Campus - Mr Colin Kikiros, Joondalup WA, Australia', 'tutuk.soms@gmail.com', '089658595043', 'Joondalup', 6027);

-- --------------------------------------------------------

--
-- Table structure for table `tblorderservice`
--

CREATE TABLE `tblorderservice` (
  `idOrderService` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `idConsumer` int(11) NOT NULL,
  `paymentUniqueCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryDate` date NOT NULL,
  `totalServiceCharge` double(8,2) NOT NULL,
  `collectionDate` date NOT NULL,
  `deliveryAddress` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryComments` text COLLATE utf8mb4_unicode_ci,
  `idSupplier` int(10) DEFAULT NULL,
  `orderDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblorderservice`
--

INSERT INTO `tblorderservice` (`idOrderService`, `idBinService`, `idConsumer`, `paymentUniqueCode`, `deliveryDate`, `totalServiceCharge`, `collectionDate`, `deliveryAddress`, `deliveryComments`, `idSupplier`, `orderDate`) VALUES
(1, 1, 1, 'WRTWETW1234', '2018-09-08', 160.00, '2018-09-09', '7/48 Prindiville Drive', NULL, 12, '2018-09-07'),
(2, 1, 1, 'qwert123234', '2018-09-15', 130.00, '2018-09-15', 'Test', NULL, 11, '2018-09-14'),
(6, 3, 5, '350I4345677', '2018-09-18', 60.00, '2018-09-19', 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'Test', 17, '2018-09-17'),
(7, 3, 6, '850741H6953', '2018-09-18', 60.00, '2018-09-19', 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'Test', 17, '2018-09-17'),
(8, 2, 7, '369977625Z9', '2018-09-26', 130.00, '2018-09-27', 'Goldmead Street, Bayswater WA, Australia', NULL, 11, '2018-09-25'),
(9, 2, 8, '99X61780235', '2018-10-27', 130.00, '2018-11-02', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Test', 11, '2018-10-26'),
(10, 2, 9, '5467381G498', '2018-10-27', 330.00, '2018-10-30', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Test', 11, '2018-10-26'),
(13, 3, 12, '755D8334482', '2018-11-01', 64.00, '2018-11-08', 'Kelmscott Station, Kelmscott WA, Australia', 'test', 17, '2018-10-31'),
(14, 2, 13, '243Z5101297', '2018-11-01', 134.00, '2018-11-02', 'Kelmscott Plaza, Albany Highway, Kelmscott WA, Australia', NULL, 11, '2018-10-31'),
(17, 8, 16, '5482L230215', '2018-11-02', 304.00, '2018-11-03', 'Joondalup Health Campus - Mr Colin Kikiros, Joondalup WA, Australia', 'Test', 19, '2018-11-09');

-- --------------------------------------------------------

--
-- Table structure for table `tblorderstatus`
--

CREATE TABLE `tblorderstatus` (
  `idOrderStatus` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `status` char(1) NOT NULL COMMENT '1 = Ongoing, Paid 2= Processed, Paid, 3 Delivered, Paid 4.Cancel, Refund Issued 5. Cancel, Refund Paid',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operator` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblorderstatus`
--

INSERT INTO `tblorderstatus` (`idOrderStatus`, `idOrder`, `status`, `created_at`, `updated_at`, `operator`) VALUES
(1, 1, '3', '2018-09-12 05:02:57', '2018-09-16 01:50:18', '5'),
(2, 2, '3', '2018-09-14 05:14:50', '2018-09-13 22:38:32', '5'),
(4, 6, '4', '2018-09-16 16:43:34', '2018-09-23 17:02:26', '5'),
(5, 7, '3', '2018-09-16 16:46:35', '2018-09-16 18:57:50', '5'),
(6, 8, '2', '2018-09-24 21:26:13', '2018-09-24 21:36:31', '5'),
(7, 9, '1', '2018-10-25 21:04:30', '2018-10-25 21:04:30', '11'),
(8, 10, '1', '2018-10-25 21:20:55', '2018-10-25 21:20:55', '11'),
(11, 13, '1', '2018-10-30 16:58:12', '2018-10-30 16:58:12', '11'),
(12, 14, '1', '2018-10-30 19:00:19', '2018-10-30 19:00:19', '11'),
(15, 17, '1', '2018-11-09 01:34:00', '2018-11-09 01:34:00', '11');

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymentformtemp`
--

CREATE TABLE `tblpaymentformtemp` (
  `idPaymentForm` int(11) NOT NULL,
  `idPaymentTemp` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `special_note` text,
  `paymentUniqueCode` varchar(20) DEFAULT NULL,
  `totalprice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpaymentformtemp`
--

INSERT INTO `tblpaymentformtemp` (`idPaymentForm`, `idPaymentTemp`, `first_name`, `last_name`, `address`, `suburb`, `zipcode`, `phone`, `email`, `special_note`, `paymentUniqueCode`, `totalprice`) VALUES
(34, 35, 'Putu', 'Winata', 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'Duncraig', '6023', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(35, 36, 'Putu', 'Winata', 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'Duncraig', '6023', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(36, 36, 'Putu', 'Winata', 'Duncraig Senior High School, Sullivan Road, Duncraig WA, Australia', 'Duncraig', '6023', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(37, 40, 'Si Ganteng', 'Sekali', 'Goldmead Street, Bayswater WA, Australia', 'Bayswater', '6053', '089658595043', 'support@somsweb.com.au', NULL, NULL, NULL),
(38, 41, 'jonjo', 'boo', '2 Smoothstone Court, Joondalup WA, Australia', 'Joondalup', '6027', '432049351', 'stevey@jimsskipbins.com.au', NULL, NULL, NULL),
(39, 45, 'Putu', 'Winata', 'Canning Mills WA, Australia', 'Canning Mills', '6111', '089658595043', 'support@somsweb.com.au', NULL, NULL, NULL),
(40, 46, 'Putu', 'Winata', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Camillo', '6111', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(41, 46, 'Putu', 'Winata', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Camillo', '6111', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(42, 46, 'Putu', 'Winata', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Camillo', '6111', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(43, 46, 'Putu', 'Winata', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Camillo', '6111', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(44, 47, 'Putu', 'Winata', 'Camillo Kebab House, Westfield Road, Camillo WA, Australia', 'Camillo', '6111', '089658595043', 'support@somsweb.com.au', 'Test', NULL, NULL),
(48, 51, 'Putu', 'Winata', 'Kelmscott Plaza, Albany Highway, Kelmscott WA, Australia', 'Kelmscott', '6111', '089658595043', 'tutuk.soms@gmail.com', NULL, NULL, NULL),
(47, 50, 'Putu', 'Winata', 'Kelmscott Station, Kelmscott WA, Australia', 'Kelmscott', '6111', '089658595043', 'tutuk.soms@gmail.com', 'test', NULL, NULL),
(78, 53, 'Putu', 'Winata', 'Joondalup Health Campus - Mr Colin Kikiros, Joondalup WA, Australia', 'Joondalup', '6027', '089658595043', 'tutuk.soms@gmail.com', 'Test', '5482L230215', 304),
(77, 53, 'Putu', 'Winata', 'Joondalup Health Campus - Mr Colin Kikiros, Joondalup WA, Australia', 'Joondalup', '6027', '089658595043', 'tutuk.soms@gmail.com', 'Test', '92399V49311', 304);

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymenttemp`
--

CREATE TABLE `tblpaymenttemp` (
  `idPaymentTemp` int(11) NOT NULL,
  `idBinHire` int(11) NOT NULL,
  `deliveryDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `paid` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpaymenttemp`
--

INSERT INTO `tblpaymenttemp` (`idPaymentTemp`, `idBinHire`, `deliveryDate`, `collectionDate`, `zipcode`, `paid`) VALUES
(36, 3, '2018-09-18', '2018-09-19', '6023', '1'),
(37, 3, '2018-09-20', '2018-09-21', '6023', '0'),
(38, 3, '2018-09-21', '2018-09-22', '6020', '0'),
(39, 2, '2018-09-26', '2018-09-27', '6933', '0'),
(40, 2, '2018-09-26', '2018-09-27', '6053', '1'),
(41, 2, '2018-10-23', '2018-10-30', '6027', '0'),
(42, 8, '2018-10-24', '2018-10-31', '6027', '0'),
(43, 8, '2018-10-25', '2018-11-02', '6027', '0'),
(44, 8, '2018-10-25', '2018-11-02', '6027', '0'),
(45, 2, '2018-10-27', '2018-10-28', '6111', '0'),
(46, 2, '2018-10-27', '2018-11-02', '6111', '1'),
(47, 2, '2018-10-27', '2018-10-30', '6111', '1'),
(51, 2, '2018-11-01', '2018-11-02', '6111', '1'),
(50, 3, '2018-11-01', '2018-11-08', '6111', '1'),
(52, 2, '2018-11-01', '2018-11-02', '6111', '0'),
(53, 8, '2018-11-02', '2018-11-03', '6027', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tblservicearea`
--

CREATE TABLE `tblservicearea` (
  `idArea` int(11) NOT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `area` text,
  `parentareacode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblservicearea`
--

INSERT INTO `tblservicearea` (`idArea`, `zipcode`, `area`, `parentareacode`) VALUES
(1, '0000', 'Western Australia', '0'),
(2, '0001', 'City of Armadale', '0000'),
(3, '0002', 'Town of Bassendean', '0000'),
(4, '0003', 'City of Bayswater', '0000'),
(5, '0004', 'City of Belmont', '0000'),
(6, '0005', 'Town of Cambridge', '0000'),
(7, '0006', 'City of Canning', '0000'),
(8, '0007', 'Town of Claremont', '0000'),
(9, '0008', 'City of Cockburn', '0000'),
(10, '0009', 'Town of Cottesloe', '0000'),
(11, '0010', 'Town of East Fremantle', '0000'),
(12, '0011', 'City of Fremantle', '0000'),
(13, '0012', 'City of Gosnells', '0000'),
(14, '0013', 'City of Joondalup', '0000'),
(15, '0014', 'City of Kalamunda', '0000'),
(16, '0015', 'City of Kwinana', '0000'),
(17, '0016', 'City of Melville', '0000'),
(18, '0017', 'Town of Mosman Park', '0000'),
(20, '0019', 'City of Nedlands', '0000'),
(21, '0020', 'Shire of Peppermint Grove', '0000'),
(22, '0021', 'City of Perth', '0000'),
(23, '0022', 'City of Rockingham', '0000'),
(24, '0023', 'Shire of Serpentine-Jarrahdale', '0000'),
(25, '0024', 'City of South Perth', '0000'),
(26, '0025', 'City of Stirling', '0000'),
(27, '0026', 'City of Subiaco', '0000'),
(28, '0027', 'City of Swan', '0000'),
(29, '0028', 'Town of Victoria Park', '0000'),
(30, '0029', 'City of Vincent', '0000'),
(31, '0030', 'City of Wanneroo', '0000'),
(32, '0031', 'City of Burnburry', '0000'),
(33, '6111', 'Ashendon, Canning Mills, Champion Lakes, Karragullen, Kelmscott, Lesley, Roleystone, Westfield', '0001'),
(34, '6112', 'Armadale, Bedfordale, Brookdale, Forrestdale, Mount Nasura, Mount Richon, Seville Grove, Wungong', '0001'),
(35, '6992', 'Armadale', '0001'),
(36, '6991', 'Kelmscott', '0001'),
(37, '6997', 'Kelmscott', '0001'),
(38, '6051', 'Maylands', '0003'),
(39, '6053', 'Bayswater', '0003'),
(40, '6062', 'Embleton, Morley, Noranda', '0003'),
(41, '6931', 'Maylands', '0003'),
(42, '6933', 'Bayswater', '0003'),
(43, '6103', 'Rivervale', '0004'),
(44, '6104', 'Ascot, Belmont, Redcliffe', '0004'),
(45, '6105', 'Cloverdale, Kewdale, Perth Airport', '0004'),
(46, '6985', 'Cloverdale', '0004'),
(47, '6984', 'Belmont', '0004'),
(48, '6230', 'Bunbury, Carey Park, College Grove, Dalyellup, Davenport, East Bunbury, Gelorup, Glen Iris, Pelican Point, South Bunbury, Usher, Vittoria, Withers', '0031'),
(49, '6231', 'Bunbury', '0031'),
(50, '6102', 'Bentley, St James', '0006'),
(51, '6106', 'Welshpool', '0006'),
(52, '6107', 'Beckenham, Cannington, Kenwick, Queens Park, Wattle Grove, Wilson', '0006'),
(53, '6147', 'Langford, Lynwood, Parkwood', '0006'),
(54, '6148', 'Ferndale, Riverton, Rossmoyne, Shelley', '0006'),
(55, '6149', 'Bull Creek, Leeming', '0006'),
(56, '6155', 'Canning Vale, Willetton', '0006'),
(57, '6970', 'Canning Vale', '0006'),
(58, '6982', 'Bentley', '0006'),
(59, '6983', 'Bentley', '0006'),
(60, '6986', 'Welshpool', '0006'),
(61, '6955', 'Willetton', '0006'),
(62, '6987', 'Cannington', '0006'),
(63, '6163', 'Bibra Lake, Coolbellup, Hamilton Hill, Hilton, Kardinya, North Coogee, North Lake, O Connor, Samson, Spearwood', '0008'),
(64, '6164', 'Atwell, Aubin Grove, Banjup, Beeliar, Cockburn Central, Hammond Park, Jandakot, South Lake, Success, Yangebup', '0008'),
(65, '6166', 'Coogee, Henderson, Munster, Wattleup', '0008'),
(66, '6964', 'South Lake', '0008'),
(67, '6965', 'Bibra Lake', '0008'),
(68, '6963', 'Halmilton Hill', '0008'),
(69, '6159', 'North Fremantle', '0011'),
(70, '6160', 'Fremantle', '0011'),
(71, '6162', 'Beaconsfield, South Fremantle, White Gum Valley', '0011'),
(72, '6959', 'Fremantle', '0011'),
(73, '6108', 'Thornlie', '0012'),
(74, '6109', 'Maddington, Orange Grove', '0012'),
(75, '6110', 'Gosnells, Huntingdale, Martin, Southern River', '0012'),
(76, '6988', 'Thornlie', '0012'),
(77, '6989', 'Maddington', '0012'),
(78, '6990', 'Gosnells', '0012'),
(79, '6919', 'Joondalup', '0013'),
(80, '6023', 'Duncraig', '0013'),
(81, '6024', 'Greenwood, Warwick', '0013'),
(82, '6924', 'Greenwood', '0013'),
(83, '6025', 'Craigie, Hillarys, Kallaroo, Padbury', '0013'),
(84, '6027', 'Beldon, Connolly, Edgewater, Heathridge, Joondalup, Mullaloo, Ocean Reef', '0013'),
(85, '6028', 'Burns Beach, Currambine, Iluka, Kinross', '0013'),
(86, '6923', 'Hillarys', '0013'),
(87, '6405', 'Greenwoods Valley, Meckering, Quelagetting, Warding East', '0013'),
(88, '6020', 'Carine, Marmion, North Beach, Sorrento, Watermans Bay', '0013'),
(89, '6057', 'High Wycombe, Maida Vale', '0014'),
(90, '6076', 'Bickley, Carmel, Gooseberry Hill, Hacketts Gully, Kalamunda, Lesmurdie, Paulls Valley, Pickering Brook, Piesse Brook, Reservoir, Walliston', '0014'),
(91, '6925', 'Walliston', '0014'),
(92, '6926', 'Kalamunda', '0014'),
(93, '6058', 'Forrestfield', '0014'),
(94, '6165', 'Hope Valley, Naval Base', '0015'),
(95, '6167', 'Anketell, Bertram, Calista, Casuarina, Kwinana Beach, Kwinana Town Centre, Mandogalup, Medina, Orelia, Parmelia, Postans, The Spectacles, Wandi', '0015'),
(96, '6170', 'Leda, Wellard', '0015'),
(97, '6966', 'Kwinana', '0015'),
(98, '6714', 'Antonymyre, Balla Balla, Baynton, Bulgarra, Burrup, Cleaverville, Cooya Pooya, Gap Ridge, Gnoorea, Karratha, Karratha Industrial Estate, Maitland, Mardie, Millars Well, Mount Anketell, Mulataga, Nickol, Pegs Creek, Sherlock, Stove Hill', '0015'),
(99, '0032', 'City of Mandurah', '0000'),
(100, '6180', 'Lakelands, Parklands', '0032'),
(101, '6181', 'Stake Hill', '0032'),
(102, '6207', 'Myara, Nambeelup, North Dandalup, Solus, Whittaker', '0032'),
(103, '6208', 'Blythewood, Fairbridge, Meelon, Nirimba, North Yunderup, Oakley, Pinjarra, Point Grey, Ravenswood, South Yunderup, West Pinjarra', '0032'),
(104, '6209', 'Barragup, Furnissdale', '0032'),
(105, '6210', 'Coodanup, Dudley Park, Erskine, Falcon, Greenfields, Halls Head, Madora Bay, Mandurah, Meadow Springs, San Remo, Silver Sands, Wannanup', '0032'),
(106, '6211', 'Bouvard, Clifton, Dawesville, Herron', '0032'),
(107, '6213', 'Banksiadale, Dwellingup, Etmilyn, Holyoake, Inglehope, Marrinup, Teesdale', '0032'),
(108, '6214', 'Birchmont, Coolup', '0032'),
(109, '6150', 'Bateman, Murdoch, Winthrop', '0016'),
(110, '6153', 'Applecross, Ardross, Brentwood, Mount Pleasant', '0016'),
(111, '6154', 'Alfred Cove, Booragoon, Myaree', '0016'),
(112, '6156', 'Attadale, Melville, Willagee', '0016'),
(113, '6157', 'Bicton, Palmyra', '0016'),
(114, '6953', 'Applecross', '0016'),
(115, '6954', 'Booragoon', '0016'),
(116, '6956', 'Melville', '0016'),
(117, '6957', 'Palmyra', '0016'),
(118, '6961', 'Palmyra', '0016'),
(119, '6960', 'Myaree', '0016'),
(120, '6008', 'Daglish, Shenton Park, Subiaco', '0019'),
(121, '6907', 'Nedlands', '0019'),
(122, '6909', 'Nedlands', '0019'),
(123, '6911', 'Nedlands', '0019'),
(124, '6000', 'Perth', '0021'),
(125, '6003', 'Highgate, Northbridge', '0021'),
(126, '6004', 'East Perth', '0021'),
(127, '6005', 'Kings Park, West Perth', '0021'),
(128, '6009', 'Crawley, Dalkeith, Nedlands', '0021'),
(129, '6831', 'Perth', '0021'),
(130, '6832', 'Perth', '0021'),
(131, '6837', 'Perth', '0021'),
(132, '6838', 'Perth', '0021'),
(133, '6839', 'Perth', '0021'),
(134, '6840', 'Perth', '0021'),
(135, '6841', 'Perth', '0021'),
(136, '6842', 'Perth', '0021'),
(137, '6843', 'Perth', '0021'),
(138, '6844', 'Perth', '0021'),
(139, '6845', 'Perth', '0021'),
(140, '6846', 'Perth', '0021'),
(141, '6847', 'Perth', '0021'),
(142, '6848', 'Perth', '0021'),
(143, '6849', 'Perth', '0021'),
(144, '6850', 'Perth', '0021'),
(145, '6865', 'Perth', '0021'),
(146, '6892', 'East Perth', '0021'),
(147, '6168', 'Cooloongup, East Rockingham, Garden Island, Hillman, Peron, Rockingham', '0022'),
(148, '6169', 'Safety Bay, Shoalwater, Waikiki, Warnbro', '0022'),
(149, '6172', 'Baldivis', '0022'),
(150, '6172', 'Port Kennedy', '0022'),
(151, '6173', 'Secret Harbour', '0022'),
(152, '6174', 'Golden Bay', '0022'),
(153, '6175', 'Singleton', '0022'),
(154, '6176', 'Karnup', '0022'),
(155, '6958', 'Rockingham', '0022'),
(156, '6967', 'Rockingham', '0022'),
(157, '6968', 'Rockingham', '0022'),
(158, '6969', 'Rockingham', '0022'),
(159, '6152', 'Como, Karawara, Manning, Salter Point, Waterford', '0024'),
(160, '6951', 'South Perth', '0024'),
(161, '6952', 'Como', '0024'),
(162, '6941', 'Mirrabooka', '0025'),
(163, '6022', 'Hamersley', '0025'),
(164, '6716', 'Fortescue, Hamersley Range, Millstream, Pannawonica', '0025'),
(165, '6016', 'Glendalough, Mount Hawthorn', '0025'),
(166, '6017', 'Herdsman, Osborne Park', '0025'),
(167, '6018', 'Churchlands, Doubleview, Gwelup, Innaloo, Karrinyup, Woodlands', '0025'),
(168, '6019', 'Scarborough, Wembley Downs', '0025'),
(169, '6021', 'Balcatta, Stirling', '0025'),
(170, '6029', 'Trigg', '0025'),
(171, '6052', 'Bedford, Inglewood', '0025'),
(172, '6060', 'Joondanna, Tuart Hill, Yokine', '0025'),
(173, '6913', 'Wembley', '0025'),
(174, '6914', 'Balcatta', '0025'),
(175, '6916', 'Osborne Park', '0025'),
(176, '6917', 'Osborne Park', '0025'),
(177, '6918', 'Innaloo City', '0025'),
(178, '6920', 'North Beach', '0025'),
(179, '6921', 'Karrinyup', '0025'),
(180, '6922', 'Scarborough', '0025'),
(181, '6929', 'Mount Lawley', '0025'),
(182, '6939', 'Tuart Hill', '0025'),
(183, '6059', 'Dianella', '0025'),
(184, '6061', 'Balga, Mirrabooka, Nollamara, Westminster', '0025'),
(185, '6904', 'Subiaco', '0026'),
(186, '6055', 'Caversham, Guildford, Hazelmere, Henley Brook, South Guildford, West Swan', '0027'),
(187, '6056', 'Baskerville, Bellevue, Boya, Greenmount, Helena Valley, Herne Hill, Jane Brook, Koongamia, Middle Swan, Midland, Midvale, Millendon, Red Hill, Stratton, Swan View, Viveash, Woodbridge', '0027'),
(188, '6063', 'Beechboro', '0027'),
(189, '6067', 'Cullacabardee', '0027'),
(190, '6068', 'Whiteman', '0027'),
(191, '6069', 'Aveley, Belhus, Brigadoon, Ellenbrook, The Vines, Upper Swan', '0027'),
(192, '6083', 'Gidgegannup, Morangup', '0027'),
(193, '6084', 'Bullsbrook, Chittering, Lower Chittering', '0027'),
(194, '6066', 'Ballajura', '0027'),
(195, '6090', 'Malaga', '0027'),
(196, '6935', 'Guilford', '0027'),
(197, '0033', 'Rottnest Island', '0000'),
(198, '0034', 'Shire of Dardanup', '0000'),
(199, '0035', 'Shire of Gingin', '0000'),
(200, '0036', 'Shire of Harvey', '0000'),
(201, '0037', 'Shire of Mundaring', '0000'),
(202, '6936', 'Midland', '0027'),
(203, '6944', 'Malaga', '0027'),
(204, '6945', 'Malaga', '0027'),
(205, '6872', 'West Perth', '0029'),
(206, '6006', 'North Perth', '0029'),
(207, '6050', 'Coolbinia, Menora, Mount Lawley', '0029'),
(208, '6902', 'Leederville', '0029'),
(209, '6903', 'Leederville', '0029'),
(210, '6915', 'Mount Hawthorn', '0029'),
(211, '6906', 'North Perth', '0029'),
(212, '6026', 'Kingsley, Woodvale', '0030'),
(213, '6065', 'Ashby, Darch, Hocking, Landsdale, Lexia, Madeley, Melaleuca, Pearsall, Sinagra, Tapping, Wangara, Wanneroo', '0030'),
(214, '6077', 'Gnangara, Jandabup', '0030'),
(215, '6078', 'Mariginiup, Pinjar', '0030'),
(216, '6946', 'Wanneroo', '0030'),
(217, '6947', 'Wangara', '0030'),
(218, '6030', 'Clarkson, Merriwa, Mindarie, Quinns Rocks, Ridgewood, Tamala Park', '0030'),
(219, '6031', 'Banksia Grove, Carramar, Neerabup', '0030'),
(220, '6032', 'Nowergup', '0030'),
(221, '6033', 'Carabooda', '0030'),
(222, '6034', 'Eglinton', '0030'),
(223, '6035', 'Yanchep', '0030'),
(224, '6036', 'Butler, Jindalee', '0030'),
(225, '6037', 'Two Rocks', '0030'),
(226, '6038', 'Alkimos', '0030'),
(227, '6064', 'Alexander Heights, Girrawheen, Koondoola, Marangaroo', '0030'),
(228, '6161', 'Rottnest Island', '0033'),
(229, '6232', 'Eaton, Millbridge', '0034'),
(230, '6041', 'Caraban, Gabbadah, Guilderton, Wilbinga, Woodridge', '0035'),
(231, '6042', 'Seabird', '0035'),
(232, '6043', 'Breton Bay, Ledge Point', '0035'),
(233, '6044', 'Karakin, Lancelin, Nilgen, Wedge Island', '0035'),
(234, '6233', 'Australind, Binningup, Leschenault, Parkfield, Wellesley', '0036'),
(235, '6556', 'Beechina, Chidlow, Gorrie, Malmalling, The Lakes', '0037'),
(236, '6558', 'Wooroloo', '0037'),
(237, '6070', 'Darlington', '0037'),
(238, '6071', 'Glen Forrest, Hovea', '0037'),
(239, '6072', 'Mahogany Creek', '0037'),
(240, '6073', 'Mundaring', '0037'),
(241, '6074', 'Sawyers Valley', '0037'),
(242, '6081', 'Parkerville, Stoneville', '0037'),
(243, '6082', 'Mount Helena', '0037'),
(244, '6271', 'Capel, Capel River, Forrest Beach, Peppermint Grove Beach, Stirling Estate', '0020'),
(245, '6122', 'Byford, Cardup, Darling Downs, Karrakup', '0023'),
(246, '6124', 'Jarrahdale', '0023'),
(247, '6125', 'Hopeland, Mardella, Serpentine', '0023'),
(248, '6126', 'Keysbrook', '0023'),
(249, '6121', 'Oakford, Oldbury', '0023'),
(250, '6123', 'Mundijong, Whitby', '0023'),
(251, '6932', 'Bassendean', '0002'),
(252, '6934', 'Bassendean', '0002'),
(253, '6054', 'Ashfield, Bassendean, Eden Hill, Kiara, Lockridge', '0002'),
(254, '6942', 'Bassendean', '0002'),
(255, '6943', 'Bassendean', '0002'),
(256, '6010', 'Claremont, Karrakatta, Mount Claremont, Swanbourne', '0005'),
(257, '6007', 'Leederville, West Leederville', '0005'),
(258, '6014', 'Floreat, Jolimont, Wembley', '0005'),
(259, '6015', 'City Beach', '0005'),
(260, '6901', 'West Leederville', '0005'),
(261, '6910', 'Claremont', '0007'),
(262, '6011', 'Cottesloe, Peppermint Grove', '0009'),
(263, '6158', 'East Fremantle', '0010'),
(264, '6012', 'Mosman Park', '0017'),
(265, '6912', 'Mosman Park', '0017'),
(266, '6979', 'Victoria Park', '0028'),
(267, '6981', 'Victoria Park East', '0028'),
(268, '6100', 'Burswood, Lathlain, Victoria Park', '0028'),
(269, '6101', 'Carlisle, East Victoria Park', '0028'),
(270, '6151', 'Kensington, South Perth', '0028'),
(271, '6026', 'Joondalup', '0013');

-- --------------------------------------------------------

--
-- Table structure for table `tblsize`
--

CREATE TABLE `tblsize` (
  `idSize` int(10) UNSIGNED NOT NULL,
  `size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dimensions` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsize`
--

INSERT INTO `tblsize` (`idSize`, `size`, `dimensions`) VALUES
(1, '2 cubic metres', '2 Cubic Metre Skip is about the same capacity as 2 standard box trailers'),
(2, '3 cubic metres', '3 Cubic Metre Skip is about the same capacity as 3 standard box trailers'),
(3, '4 cubic metres', '4 Cubic Metre Skip is about the same capacity as 4 standard box trailers'),
(4, '6 cubic metres', '5 Cubic Metre Skip is about the same capacity as 5 standard box trailers'),
(5, '7.5 cubic metres', '7.5 Cubic Metre Skip is about the same capacity as 7.5 standard box trailers'),
(6, '9 cubic metres', '9 Cubic Metre Skip is about the same capacity as 9 standard box trailers'),
(7, '11 cubic metres', '11 Cubic Metre Skip is about the same capacity as 11 standard box trailers'),
(8, '15 cubic metres', '15 Cubic Metre Skip is about the same capacity as 15 standard box trailers'),
(9, '30 cubic metres', '30 Cubic Metre Skip is about the same capacity as 30 standard box trailers');

-- --------------------------------------------------------

--
-- Table structure for table `tblsupplier`
--

CREATE TABLE `tblsupplier` (
  `idSupplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobilePhone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `email2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSaturday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSunday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mainServiceArea` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceContact` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServicePhone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceMobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullAddress` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsupplier`
--

INSERT INTO `tblsupplier` (`idSupplier`, `name`, `contactName`, `phonenumber`, `email`, `mobilePhone`, `comments`, `email2`, `isOpenSaturday`, `isOpenSunday`, `mainServiceArea`, `customerServiceContact`, `customerServicePhone`, `customerServiceMobile`, `fullAddress`) VALUES
(11, 'Ezy Skip Bin', 'Webmaster', '0361 241875', 'tutuk.soms@gmail.com', '089658595043', 'test', 'tutux.hollowbody@gmail', '1', '1', '2', 'Webmaster', '0361 241875', '089658595043', 'Joondalup 6027'),
(12, 'Great Bin', 'Great Bin', '089658595043', 'support@somsweb.com.au', '089658595043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Konoha Anbu Skip Bin', 'Kakashi Hatake', '089658595043', 'tutux.hollowbody@gmail.com', '089658595043', 'Test', NULL, '1', '1', NULL, NULL, NULL, NULL, NULL),
(18, 'Digisnapper', 'Putu Winata', '089658595043', 'info@baliwebdesignservice.id', '089658595043', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Steve\'s Skip Bins', 'Steve Young', '0432049351', 'stevey@jimsskipbins.com.au', '0432049351', 'I want to sign up', NULL, '1', '0', NULL, NULL, NULL, NULL, NULL),
(20, 'Selbies Skip Bins', 'Neil Selbie', '0432049351', 'pipersteve124@bigpond.com', '0432049351', 'I would like to sign up with you please.', NULL, '1', '0', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `idUser` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminApproved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userStatus` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registerDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`idUser`, `idSupplier`, `username`, `password`, `isActive`, `role`, `adminApproved`, `userStatus`, `registerDate`) VALUES
(5, 11, 'webmaster', '62c8ad0a15d9d1ca38d5dee762a16e01', '1', '1', '1', '1', '2018-06-01'),
(6, 12, 'tutuk1959', '62c8ad0a15d9d1ca38d5dee762a16e01', '1', '2', '1', '1', '2018-09-06'),
(11, 17, 'naruto', '62c8ad0a15d9d1ca38d5dee762a16e01', '1', '2', '1', '1', '2018-09-17'),
(12, 18, 'digisnapper', '62c8ad0a15d9d1ca38d5dee762a16e01', '0', '2', '1', '3', '2018-09-24'),
(13, 19, 'Stevey124', 'e6641202b842375f531ffceedbde1a9b', '1', '2', '1', '1', '2018-10-20'),
(14, 20, 'Selbie1', 'a451e05de30cc27778bc0b9f82e586c1', '1', '2', '1', '1', '2018-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `tbluserservicearea`
--

CREATE TABLE `tbluserservicearea` (
  `idUserServiceArea` int(10) NOT NULL,
  `idUser` int(10) NOT NULL,
  `idSupplier` int(10) NOT NULL,
  `idServiceArea` int(10) NOT NULL,
  `serviceAreaParent` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluserservicearea`
--

INSERT INTO `tbluserservicearea` (`idUserServiceArea`, `idUser`, `idSupplier`, `idServiceArea`, `serviceAreaParent`) VALUES
(15, 11, 17, 29, '0003'),
(16, 11, 17, 30, '0003'),
(17, 5, 11, 22, '0002'),
(18, 5, 11, 23, '0002'),
(19, 5, 11, 24, '0002'),
(20, 5, 11, 33, '0001'),
(21, 5, 11, 34, '0001'),
(22, 5, 11, 35, '0001'),
(23, 5, 11, 36, '0001'),
(24, 5, 11, 37, '0001'),
(25, 5, 11, 38, '0003'),
(26, 5, 11, 39, '0003'),
(27, 5, 11, 40, '0003'),
(28, 5, 11, 41, '0003'),
(29, 5, 11, 42, '0003'),
(30, 5, 11, 79, '0013'),
(31, 5, 11, 80, '0013'),
(32, 5, 11, 81, '0013'),
(33, 5, 11, 82, '0013'),
(34, 5, 11, 83, '0013'),
(35, 5, 11, 84, '0013'),
(36, 5, 11, 85, '0013'),
(37, 5, 11, 86, '0013'),
(38, 5, 11, 87, '0013'),
(39, 5, 11, 88, '0013'),
(40, 13, 19, 84, '0013'),
(41, 14, 20, 84, '0013'),
(42, 11, 17, 33, '0001');

-- --------------------------------------------------------

--
-- Table structure for table `user_confirmation`
--

CREATE TABLE `user_confirmation` (
  `idConfirmation` int(11) NOT NULL,
  `idSupplier` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_confirmation`
--

INSERT INTO `user_confirmation` (`idConfirmation`, `idSupplier`, `idUser`, `token`) VALUES
(2, 14, 8, 'Macu1nyczD09sGE0qmx9pCa3rWtbmU'),
(3, 15, 9, 'hY040R9rquK0j9nQQUPaI4wufimTcB'),
(4, 16, 10, 'U7soQjR1z0OFIAE8pbkASPSgHxtd7S');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `csv_data`
--
ALTER TABLE `csv_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  ADD PRIMARY KEY (`idNonDeliveryDays`);

--
-- Indexes for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  ADD PRIMARY KEY (`idBinService`);

--
-- Indexes for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  ADD PRIMARY KEY (`idBinServiceOptions`);

--
-- Indexes for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  ADD PRIMARY KEY (`idBinServiceUpdates`);

--
-- Indexes for table `tblbintype`
--
ALTER TABLE `tblbintype`
  ADD PRIMARY KEY (`idBinType`);

--
-- Indexes for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  ADD PRIMARY KEY (`idBookingPrice`);

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  ADD PRIMARY KEY (`idOrderService`);

--
-- Indexes for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  ADD PRIMARY KEY (`idOrderStatus`);

--
-- Indexes for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  ADD PRIMARY KEY (`idPaymentForm`);

--
-- Indexes for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  ADD PRIMARY KEY (`idPaymentTemp`);

--
-- Indexes for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  ADD PRIMARY KEY (`idArea`);

--
-- Indexes for table `tblsize`
--
ALTER TABLE `tblsize`
  ADD PRIMARY KEY (`idSize`);

--
-- Indexes for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  ADD PRIMARY KEY (`idSupplier`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  ADD PRIMARY KEY (`idUserServiceArea`);

--
-- Indexes for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  ADD PRIMARY KEY (`idConfirmation`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `csv_data`
--
ALTER TABLE `csv_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  MODIFY `idNonDeliveryDays` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  MODIFY `idBinService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  MODIFY `idBinServiceOptions` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  MODIFY `idBinServiceUpdates` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblbintype`
--
ALTER TABLE `tblbintype`
  MODIFY `idBinType` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  MODIFY `idBookingPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `idCustomer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  MODIFY `idOrderService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  MODIFY `idOrderStatus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  MODIFY `idPaymentForm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  MODIFY `idPaymentTemp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  MODIFY `idArea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=272;

--
-- AUTO_INCREMENT for table `tblsize`
--
ALTER TABLE `tblsize`
  MODIFY `idSize` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  MODIFY `idSupplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `idUser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  MODIFY `idUserServiceArea` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  MODIFY `idConfirmation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
